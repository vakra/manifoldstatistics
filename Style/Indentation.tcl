# Enforces consistent indentation
# Note: this checker is not foolproof, will especially generate false positives
#
# Handles the following cases:
#
# 1. Blocks inside curly braces
# 2. Indentation of continued parenthetical statements
# 3. Single-line after a control statement
# 4. Aligned stream tokens ("<<", ">>")
# 5. Continuation of template arguments (inside "< >")

set indentLevel [getParameter "indent-level" 2]

foreach f [getSourceFileNames] {
    set lineNumber 1
    set indentVal 0
    set parenIndent [list ]
    set inTemplate 0
    set inTypedef 0
    set templateIndent [list ]
    set inClassName 0
    set inControl 0
    set prevStreamPos 0
    set prevStatementOpen 0
    set matched ""
    foreach line [getAllLines $f] {
        set lineLength [string length $line]
        set tokenList [getTokens $f $lineNumber 0 $lineNumber $lineLength {}]
        set numTokens [llength $tokenList]

        set parenDepth [llength $parenIndent]
        set templateDepth [llength $templateIndent]

        #report $f $lineNumber "parenIndent = $parenIndent, inClassName = $inClassName, inControl = $inControl, templateIndent = $templateIndent, indentLevel = $indentLevel"

        set firstToken [lindex $tokenList 0 3]
        if {$firstToken == "space"} {
            if {$numTokens >= 2} {
                set firstToken [lindex $tokenList 1 3]
            }
            incr numTokens -1
        }

        if {$numTokens != 0} {
            regexp {^( )*} $line matched

            set expectedIndent $indentVal
            if {$parenDepth != 0} {
                set expectedIndent [lindex $parenIndent end]
                incr expectedIndent
            } elseif {$templateDepth != 0} {
                set expectedIndent [lindex $templateIndent end]
                incr expectedIndent
            } elseif $inClassName {
                if {$firstToken == "leftbrace"} {
                    set expectedIndent 0
                } else {
                    set expectedIndent $indentLevel
                }
            } elseif {$inControl == 1 && $firstToken != "leftbrace"} {
                incr expectedIndent $indentLevel
            } elseif {$firstToken == "rightbrace"} {
                incr expectedIndent -$indentLevel
            } elseif {[lsearch {"private" "protected" "public"} $firstToken] != -1 && !$inClassName} {
                set expectedIndent 0
            } elseif {$firstToken == "class"} {
                set inClassName 1
            } elseif {$firstToken == "typedef"} {
                set inTypedef 1
            } elseif {[lsearch {"shiftleft" "shiftright"} $firstToken] != -1} {
                set expectedIndent $prevStreamPos
            }

            set numSpaces [string length $matched]

            if {$inControl == 1 && $firstToken == "leftbrace"} {
                set prevStatementOpen 0
            }

            # Bit of a hack to test if this line is a continuation of the previous one (not foolproof!)
            incr expectedIndent $indentLevel
            if {$numSpaces != $expectedIndent || $prevStatementOpen == 0} {
                incr expectedIndent -$indentLevel
            }

            if {$numSpaces != $expectedIndent} {
                report $f $lineNumber "Incorrect indentation: should be $expectedIndent"
            }
        }

        set prevStreamPos 0
        foreach token $tokenList {
            set type [lindex $token 3]
            set pos [lindex $token 2]
            if {$type == "rightbrace"} {
                if {$inControl == 2} {
                    set inControl 0
                }
                incr indentVal -$indentLevel
            } elseif {$type == "leftbrace"} {
                if $inClassName {
                    set inClassName 0
                } elseif {$inControl == 1 && $parenDepth == 0} {
                    incr inControl
                }
                incr indentVal $indentLevel
            } elseif {$type == "leftparen"} {
                set parenIndent [concat $parenIndent $pos]
                incr parenDepth
            } elseif {$type == "rightparen"} {
                set parenIndent [lreplace $parenIndent end end]
                incr parenDepth -1
            } elseif {[lsearch {"for" "if" "while" "else"} $type] != -1} {
                set inControl 1
            } elseif {$prevStreamPos == 0 && [lsearch {"shiftleft" "shiftright"} $type] != -1} {
                set prevStreamPos $pos
            } elseif {$type == "less" && ($inTemplate || $inClassName || $inTypedef)} {
                set templateIndent [concat $templateIndent $pos]
                incr templateDepth
            } elseif {$type == "greater" && $templateDepth > 0} {
                set templateIndent [lreplace $templateIndent end end]
                incr templateDepth -1
                if {$templateDepth == 0} {
                    set inTemplate 0
                }
            } elseif {$type == "template"} {
                set inTemplate 1
            }

            # Determine if this line has an open statement continuing to next line
            # (only handle long typedefs right now!)
            if {$type == "semicolon"} {
                set prevStatementOpen 0
                if {$inControl == 1 && $parenDepth == 0} {
                    set inControl 0
                }
                if {$inTypedef == 1} {
                    set inTypedef 0
                }
            } elseif {$firstToken == "typedef" && $type != "space" && $type != "newline"} {
                set prevStatementOpen 1
            }
        }

        incr lineNumber
    }
}
