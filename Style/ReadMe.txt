This document describes how to check the coding style of C++ code in the
Manifold Statistics project. We are using the Vera++ tool, which can be found
here:

https://bitbucket.org/verateam/vera

We've extended Vera++ to also check the indentation of C++ code. Here is how to
use it in this project:

1. After building Vera++, you will want to add the main Vera++ directory to your
PATH variable and also set the VERA_ROOT environment variable.

2. Copy the file "manifold_stats" into the $VERA_ROOT/profiles/ directory. Copy
the file "Indentation.tcl" in the $VERA_ROOT/scripts/rules/ directory.

3. Check the code! Assuming you are in the src directory of this project, you
can run the following:

find . -name "*.*xx" | xargs vera++ -profile manifold_stats -paramfile ./Style/manifold_stats.param
