# Install the ManifoldStatistics Library

This file will take you through the steps to build the library.

## Dependencies:
To install ManifoldStatistics, you will first need to install
the following. Recommended versions are listed, but things may
work with older versions.

[CMake 3.6+](http://www.cmake.org/)

[Boost 1.61+](http://www.boost.org/)

[Armadillo 7.4+](http://arma.sourceforge.net/)

[Expokit](http://www.maths.uq.edu.au/expokit/)

Follow the installation procedures for each of the above. For
Expokit, we recommend building the file expokit.f using the
following procedure:

> gfortran -c -fPIC expokit.f -llapack -lblas -lgfortran -o expokit.o

> ar rcs [library filename] expokit.o


The naming convention for the [library filename] will depend
on your OS, e.g., libexpokit.so for Linux, or libexpokit.dylib
for Mac. If you copy the resulting library to a standard place
(e.g., /usr/local/lib), CMake will hopefully find it for you
automatically when you build ManifoldStatistics.

## Getting the source:
First, clone the source code:

> git clone https://bitbucket.org/vakra/manifoldstatistics.git [local directory]


## Building the library:

Next, create a directory to build the source, and run
ccmake from within it:

> mkdir Build

> cd Build

> ccmake [path to source directory]

Inside CMake, hit 'c' to configure. Then hit 'c' again. At
this point, you may need to tell CMake where it can find the
dependencies (Boost, Armadillo, Expokit). If they are in
standard directories, then CMake may find them for you.
Once everything is in order, type 'g' to generate the makefile.

Now you are ready to build the ManifoldStatistics library.
Simply run make:

> make
