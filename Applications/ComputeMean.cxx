/*==============================================================================
  File: ComputeMean.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include <boost/program_options.hpp>
#include "ManifoldFileReader.hxx"
#include "ManifoldFileWriter.hxx"
#include "MeanAndPGA.hxx"

using namespace std;

namespace po = boost::program_options;

template <class PointType, class TangentType>
int computeMean(ManifoldFileReader & reader, string outfile, string format,
                bool verbose)
{
  typedef MeanAndPGA<PointType, TangentType> StatsType;

  unsigned int i;
  std::vector<PointType> points;
  PointType mean;

  if(!reader.readPointList<PointType, TangentType>(points))
    return EXIT_FAILURE;

  if(verbose)
  {
    cout << "Point list:" << endl;

    for(i = 0; i < points.size(); i++)
      cout << points[i] << endl;
  }


  std::shared_ptr<RiemannianGeometry<PointType, TangentType> > geom;
  geom = reader.getGeometry<PointType, TangentType>();
  StatsType * stats = new StatsType(geom);
  stats->computeMean(points, mean);

  if(verbose)
  {
    cout << "Mean point = " << endl;
    cout << mean << endl;
  }

  std::shared_ptr<MFDContainer> inContainer = reader.getContainer();
  ManifoldFileWriter writer(outfile, inContainer->getManifoldType(),
                            inContainer->getDimParameters(),
                            format.compare("binary") == 0);

  bool success;
  std::vector<PointType> meanList;
  meanList.push_back(mean);
  success = writer.writePointList<PointType, TangentType>(meanList);

  delete stats;

  if(success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}

int main(int argc, char ** argv)
{
  string format;
  string infile;
  string outfile;
  bool verbose = false;

  try {
    po::options_description desc("Usage:\n  ComputeMean [options] <infile>\n\nOptions");
    desc.add_options()
      ("help,h", "prints a help message")
      ("verbose,v", "prints out details")
      ("format,f", po::value<string>(), "output file format (default: \"binary\")")
      ("infile,i", po::value<string>(), "input file name (required)")
      ("outfile,o", po::value<string>(), "output file name (required)")
      ;

    po::positional_options_description p;
    p.add("infile", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      cout << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("verbose"))
      verbose = true;

    if(vm.count("format"))
      format = vm["format"].as<string>();
    else
      format = "binary";

    if(vm.count("infile"))
      infile = vm["infile"].as<string>();
    else
    {
      cerr << "Error: option \"infile\" must be set to desired input file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("outfile"))
      outfile = vm["outfile"].as<string>();
    else
    {
      cerr << "Error: option \"outfile\" must be set to desired output file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }
  }
  catch(exception & e)
  {
    cerr << "Error: " << e.what() << endl;
    return EXIT_FAILURE;
  }
  catch(...)
  {
    cerr << "Unknown exception" << endl;
    return EXIT_FAILURE;
  }

  ManifoldFileReader reader;
  reader.load(infile);
  std::shared_ptr<MFDContainer> inContainer = reader.getContainer();

  // File was not loaded correctly
  if(!inContainer)
    return EXIT_FAILURE;

  if(verbose)
  {
    cout << "File header information:" << endl;
    cout << *inContainer << endl;
  }

  if(inContainer->getRepresentationType() == SCALAR)
    return computeMean<ScalarType, ScalarType>(reader, outfile,
                                               format, verbose);
  else if(inContainer->getRepresentationType() == VECTOR)
    return computeMean<VectorType, VectorType>(reader, outfile,
                                               format, verbose);
  else if(inContainer->getRepresentationType() == MATRIX)
    return computeMean<MatrixType, MatrixType>(reader, outfile,
                                               format, verbose);
  //else if(inContainer->getRepresentationType() == VECTOR_TANGENT_BUNDLE)
  //  computeMean<VectorTBPointType, VectorTBTangentType>(reader, outfile, format, verbose);
  //else if(inContainer->getRepresentationType() == MATRIX_TANGENT_BUNDLE)
  //  computeMean<MatrixTBPointType, MatrixTBTangentType>(reader, outfile, format, verbose);
  else
  {
    cerr << "Error: Unsupported manifold representation type." << endl;
    return EXIT_FAILURE;
  }
}
