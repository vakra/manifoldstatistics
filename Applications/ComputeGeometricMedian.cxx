/*==============================================================================
  File: ComputeMean.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include <boost/program_options.hpp>
#include "ManifoldFileReader.hxx"
#include "ManifoldFileWriter.hxx"
#include "GeometricMedian.hxx"

using namespace std;

namespace po = boost::program_options;

template <class PointType, class TangentType>
int computeGeometricMedian(ManifoldFileReader & reader, string outfile,
                           string format, bool verbose)
{
  unsigned int i;

  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;
  typedef std::shared_ptr<RiemannianGeometry<PointType, TangentType> > GeometryPtr;
  typedef GeometricMedian<PointType, TangentType> MedianStatType;

  PointListType points;
  if(!reader.readPointList<PointType, TangentType>(points))
    return EXIT_FAILURE;

  if(verbose)
  {
    cout << "Point list:" << endl;

    for(i = 0; i < points.size(); i++)
      cout << points[i] << endl;
  }

  GeometryPtr geom = reader.getGeometry<PointType, TangentType>();
  MedianStatType * medianStat = new MedianStatType(geom);

  PointType median;
  medianStat->computeMedian(points, median);

  if(verbose)
  {
    cout << "Median point = " << endl;
    cout << median << endl;
  }

  std::shared_ptr<MFDContainer> inContainer = reader.getContainer();
  ManifoldFileWriter writer(outfile, inContainer->getManifoldType(),
                            inContainer->getDimParameters(),
                            format.compare("binary") == 0);

  bool success;
  std::vector<PointType> medianList;
  medianList.push_back(median);
  success = writer.writePointList<PointType, TangentType>(medianList);

  delete medianStat;

  if(success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}

int main(int argc, char ** argv)
{
  string format;
  string infile;
  string outfile;
  bool verbose = false;

  try {
    po::options_description desc("Usage:\n  ComputeGeometricMedian [options] <infile>\n\nOptions");
    desc.add_options()
      ("help,h", "prints a help message")
      ("verbose,v", "prints out details")
      ("format,f", po::value<string>(), "output file format (default: \"binary\")")
      ("infile,i", po::value<string>(), "input file name (required)")
      ("outfile,o", po::value<string>(), "output file name (required)")
      ;

    po::positional_options_description p;
    p.add("infile", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      cout << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("verbose"))
      verbose = true;

    if(vm.count("format"))
      format = vm["format"].as<string>();
    else
      format = "binary";

    if(vm.count("infile"))
      infile = vm["infile"].as<string>();
    else
    {
      cerr << "Error: option \"infile\" must be set to desired input file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("outfile"))
      outfile = vm["outfile"].as<string>();
    else
    {
      cerr << "Error: option \"outfile\" must be set to desired output file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }
  }
  catch(exception & e)
  {
    cerr << "Error: " << e.what() << endl;
    return EXIT_FAILURE;
  }
  catch(...)
  {
    cerr << "Unknown exception" << endl;
    return EXIT_FAILURE;
  }

  ManifoldFileReader reader;
  reader.load(infile);
  std::shared_ptr<MFDContainer> inContainer = reader.getContainer();

  if(!inContainer)
    return EXIT_FAILURE;

  if(verbose)
  {
    cout << "File header information:" << endl;
    cout << *inContainer << endl;
  }

  if(inContainer->getRepresentationType() == SCALAR)
    return computeGeometricMedian<ScalarType, ScalarType>(reader, outfile,
                                                          format, verbose);
  else if(inContainer->getRepresentationType() == VECTOR)
    return computeGeometricMedian<VectorType, VectorType>(reader, outfile,
                                                          format, verbose);
  else if(inContainer->getRepresentationType() == MATRIX)
    return computeGeometricMedian<MatrixType, MatrixType>(reader, outfile,
                                                          format, verbose);
  else
  {
    cerr << "Error: Unsupported manifold representation type." << endl;
    return EXIT_FAILURE;
  }
}
