/*==============================================================================
  File: ComputeGeodesicRegression.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include <boost/program_options.hpp>
#include "ManifoldFileReader.hxx"
#include "ManifoldFileWriter.hxx"
#include "GeodesicRegression.hxx"
#include <fstream>

namespace po = boost::program_options;
using namespace std;

template <class PointType, class TangentType>
int computeGeodesicRegression(ManifoldFileReader & readerX,
                              ManifoldFileReader & readerY, string outfile,
                              string format, bool verbose)
{
  unsigned int i;

  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef std::shared_ptr<GeometryType> GeometrySharedPtr;

  typedef SasakiGeometry<PointType, TangentType> TMGeometryType;
  typedef typename TMGeometryType::TMPointType TMPointType;
  typedef typename TMGeometryType::TMPointListType TMPointListType;

  typedef GeodesicRegression<PointType, TangentType> RegressionType;

  std::vector<ScalarType> xPoints;
  std::vector<PointType> yPoints;
  if(!readerX.readPointList<ScalarType, ScalarType>(xPoints))
    return EXIT_FAILURE;
  if(!readerY.readPointList<PointType, TangentType>(yPoints))
    return EXIT_FAILURE;

  if(verbose)
  {
    cout << "X point list:" << endl;

    for(i = 0; i < xPoints.size(); i++)
      cout << xPoints[i] << endl;

    cout << endl << "Y point list:" << endl;
    for(i = 0; i < yPoints.size(); i++)
      cout << yPoints[i] << endl;
  }

  GeometrySharedPtr geom = readerY.getGeometry<PointType, TangentType>();
  RegressionType * reg = new RegressionType(geom);

  PointType base = yPoints[0];
  TangentType tangent = geom->getZeroVector();

  reg->computeGeodesicRegression(yPoints, xPoints, base, tangent);

  std::shared_ptr<MFDContainer> inContainer = readerY.getContainer();
  ManifoldFileWriter writer(outfile, inContainer->getManifoldType(),
                            inContainer->getDimParameters(),
                            format.compare("binary") == 0);

  // Store result in a list data structure for writing
  TangentBundlePointType<PointType, TangentType> tbPoint;
  tbPoint.p = base;
  tbPoint.u = tangent;
  std::vector<TangentBundlePointType<PointType, TangentType> > resultList;
  resultList.push_back(tbPoint);

  // Write result to file
  bool success;
  success = writer.writeTangentBundleList(resultList);

  delete reg;

  if(success)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}

int main(int argc, char ** argv)
{
  string format;
  string infileX, infileY;
  string outfile;
  bool verbose = false;

  try {
    po::options_description desc("Usage:\n  ComputeGeodesicRegression [options]\n\nOptions");
    desc.add_options()
      ("help,h", "prints a help message")
      ("verbose,v", "prints out details")
      ("format,f", po::value<string>(), "output file format (default: \"binary\")")
      ("infileX,x", po::value<string>(), "input file name: independent variable (required)")
      ("infileY,y", po::value<string>(), "input file name: dependent variable (required)")
      ("outfile,o", po::value<string>(), "output file name (required)")
      ;

    po::positional_options_description p;
    p.add("infileY", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      cout << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("verbose"))
      verbose = true;

    if(vm.count("format"))
      format = vm["format"].as<string>();
    else
      format = "binary";

    if(vm.count("infileX"))
      infileX = vm["infileX"].as<string>();
    else
    {
      cerr << "Error: option \"infileX\" must be set to desired input file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("infileY"))
      infileY = vm["infileY"].as<string>();
    else
    {
      cerr << "Error: option \"infileY\" must be set to desired input file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("outfile"))
      outfile = vm["outfile"].as<string>();
    else
    {
      cerr << "Error: option \"outfile\" must be set to desired output file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }
  }
  catch(exception & e)
  {
    cerr << "Error: " << e.what() << endl;
    return EXIT_FAILURE;
  }
  catch(...)
  {
    cerr << "Unknown exception" << endl;
    return EXIT_FAILURE;
  }

  ManifoldFileReader readerX, readerY;
  readerX.load(infileX);
  readerY.load(infileY);

  std::shared_ptr<MFDContainer> xContainer = readerX.getContainer();
  std::shared_ptr<MFDContainer> yContainer = readerY.getContainer();

  if(verbose)
  {
    cout << "X file header information:" << endl;
    cout << *xContainer << endl;

    cout << "Y file header information:" << endl;
    cout << *yContainer << endl;
  }

  if(xContainer->getNumElements() != yContainer->getNumElements())
  {
    cout << "Error: X and Y data must have the same number of elements."
         << endl;
    return EXIT_FAILURE;
  }

  if(xContainer->getManifoldType().compare("Euclidean") != 0 ||
     xContainer->getDimParameters()[0] != 1)
  {
    cout << "Error: X data must be real numbers." << endl;
    return EXIT_FAILURE;
  }

  if(yContainer->getRepresentationType() == SCALAR)
    return computeGeodesicRegression<ScalarType, ScalarType>
      (readerX, readerY, outfile, format, verbose);
  else if(yContainer->getRepresentationType() == VECTOR)
    return computeGeodesicRegression<VectorType, VectorType>
      (readerX, readerY, outfile, format, verbose);
  else if(yContainer->getRepresentationType() == MATRIX)
    return computeGeodesicRegression<MatrixType, MatrixType>
      (readerX, readerY, outfile, format, verbose);
  else
  {
    cerr << "Error: Unsupported manifold representation type." << endl;
    return EXIT_FAILURE;
  }
}
