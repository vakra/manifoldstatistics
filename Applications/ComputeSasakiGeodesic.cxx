/*==============================================================================
  File: ComputeSasakiGeodesic.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include <boost/program_options.hpp>
#include "RiemannianGeometryFactory.hxx"
#include "ManifoldFileIO.hxx"
#include "GeodesicRegression.hxx"
#include "MeanAndPGA.hxx"
#include "SasakiGeometry.hxx"
#include <fstream>

namespace po = boost::program_options;
using namespace std;
typedef typename ManifoldFileIO::GeometryPtr GeometryPtr;

int main(int argc, char ** argv)
{
  string format;
  string infileX, infileY;
  string outfile;
  bool verbose = false;
  unsigned int i;

  try {
    po::options_description desc("Usage:\n  ComputeSasakiGeodesic [options] <infile>\n\nOptions");
    desc.add_options()
      ("help,h", "prints a help message")
      ("verbose,v", "prints out details")
      ("format,f", po::value<string>(), "output file format (default: \"binary\")")
      ("infileX,x", po::value<string>(), "input file name: time (required)")
      ("infileY,y", po::value<string>(), "input file name: data (required)")
      ("outfile,o", po::value<string>(), "output file name (required)")
      ;

    po::positional_options_description p;
    p.add("outfile", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      cout << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("verbose"))
      verbose = true;

    if(vm.count("format"))
      format = vm["format"].as<string>();
    else
      format = "binary";

    if(vm.count("infileX"))
      infileX = vm["infileX"].as<string>();
    else
    {
      cerr << "Error: option \"infileX\" must be set to desired input file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }
    
    if(vm.count("infileY"))
      infileY = vm["infileY"].as<string>();
    else
    {
      cerr << "Error: option \"infileY\" must be set to desired input file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("outfile"))
      outfile = vm["outfile"].as<string>();
    else
    {
      cerr << "Error: option \"outfile\" must be set to desired output file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }
  }
  catch(exception & e)
  {
    cerr << "Error: " << e.what() << endl;
    return EXIT_FAILURE;
  }
  catch(...)
  {
    cerr << "Unknown exception" << endl;
    return EXIT_FAILURE;
  }

  GeometryPtr geom; // = container.geom (there needs to be a container for geom)
  PointListType pointList;
  ScalarListType scalarList;
  PointType base;
  TangentType tangent;
  ManifoldFileIO io;
  ifstream xFile;
  double xVal;
  unsigned int numSubj; // = container.num
  
  // TODO: Need to keep track variable number of time-points
  
  if(verbose)
    cout << "Loading points from input file: " << infileY << endl;

  geom = io.load(infileY, pointList); // this needs to be a container!!!
  scalarList.set_size(pointList.size());
  
  xFile.open(infileX.c_str());
  unsigned int index = 0;
  while(xFile >> xVal)
    {
      scalarList[index] = xVal;
      index++;
      if(index > pointList.size())
	{
	  cerr << "Error: Size mismatch in input data from \"infileX\" and \"infileY\" " << endl;
	  return EXIT_FAILURE;
	}
    }
  
  if(verbose)
  {
    cout << "Point list:" << endl;
    for(i = 0; i < pointList.size(); i++)
      cout << pointList[i] << endl;
    
    cout << "Scalar list:" << endl;
    for(i = 0; i < pointList.size(); i++)
      cout << scalarList[i] << endl;
  }


  // for(i = 0; i < numSubj; i++)
  // { 

  // initializing base and tangent
  base = pointList[0];
  tangent = geom->getZeroVector();
  
  GeodesicRegression * regression = new GeodesicRegression(geom);
  regression->computeGeodesicRegression(pointList, scalarList, base, tangent);

  if(verbose)
  {
    cout << "Base point = " << endl;
    cout << base << endl;

    cout << "Tangent = " << endl;
    cout << tangent << endl;
  } // store to sasakiPointList

  // } 

  delete regression;

  // need to store these points and tangents in a list
  // and call the sasaki geometry and compute mean
  // and then output to file.

  // SasakiGeometry * tmGeom = new SasakiGeometry(geom); // convert to shared pointer
  // MeanAndPGA * stats = new MeanAndPGA(tmGeom);

  // stats->computeMean(sasakiPointList, groupGeodesic);

  pointList.resize(2);
  pointList[0] = base; // groupGeodesic.p;
  pointList[1] = tangent; // groupGeodesic.u;

  // TODO: would be nice to output to stdout
  if(format.compare("binary") == 0)
    io.save(outfile, pointList, geom, true);
  else if(format.compare("text") == 0)
    io.save(outfile, pointList, geom, false);
  else
  {
    cerr << "Invalid file format: " << format << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
