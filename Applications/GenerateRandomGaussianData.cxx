/*==============================================================================
  File: GenerateRandomGaussianData.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include <boost/program_options.hpp>
#include "ManifoldFileReader.hxx"
#include "ManifoldFileWriter.hxx"
#include "RandomGenerator.hxx"

namespace po = boost::program_options;
using namespace std;

template <class PointType, class TangentType>
int generateRandomGaussianData(ManifoldFileReader & reader, string outfile,
                               unsigned int number, ScalarType sigma,
                               string format, bool verbose)
{
  typedef RandomGenerator<PointType, TangentType> RandomType;

  unsigned int i;
  std::vector<PointType> mean;
  std::vector<PointType> points(number);
  std::shared_ptr<RiemannianGeometry<PointType, TangentType> > geom;

  if(!reader.readPointList<PointType, TangentType>(mean))
    return EXIT_FAILURE;

  geom = reader.getGeometry<PointType, TangentType>();

  RandomType::seedWithTime();
  for(i = 0; i < number; i++)
    points[i] = RandomType::randomGaussianPoint(geom, mean[0], sigma);

  std::shared_ptr<MFDContainer> inContainer = reader.getContainer();
  ManifoldFileWriter writer(outfile, inContainer->getManifoldType(),
                            inContainer->getDimParameters(),
                            format.compare("binary") == 0);

  if(writer.writePointList<PointType, TangentType>(points))
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}

int main(int argc, char ** argv)
{
  unsigned int number;
  ScalarType sigma;
  string format, meanFile, outfile;
  bool verbose = false;

  try {
    po::options_description desc("Usage:\n  GenerateRandomGaussianData [options] [mean]\n\nOptions");
    desc.add_options()
      ("help,h", "prints a help message")
      ("verbose,v", "prints out details")
      ("format,f", po::value<string>(), "output file format (default: \"binary\")")
      ("number,n", po::value<unsigned int>(), "number of points to generate (required)")
      ("meanFile,m", po::value<string>(), "mean of the Gaussian (required, can be listed last without the -m)")
      ("sigma,s", po::value<ScalarType>(), "sigma of the Gaussian (required)")
      ("outfile,o", po::value<string>(), "output file name (required)")
      ;

    po::positional_options_description p;
    p.add("meanFile", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).positional(p).run(), vm);
    po::notify(vm);

    if(vm.count("help"))
    {
      cout << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("verbose"))
      verbose = true;

    if(vm.count("format"))
      format = vm["format"].as<string>();
    else
      format = "binary";

    if(vm.count("number"))
      number = vm["number"].as<unsigned int>();
    else
    {
      cerr << "Error: option \"number\" must be set to desired number of points to generate." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("meanFile"))
      meanFile = vm["meanFile"].as<string>();
    else
    {
      cerr << "Error: option \"meanFile\" must be set to desired output file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("sigma"))
      sigma = vm["sigma"].as<ScalarType>();
    else
    {
      cerr << "Error: option \"sigma\" must be set to desired sigma parameter for Gaussian." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }

    if(vm.count("outfile"))
      outfile = vm["outfile"].as<string>();
    else
    {
      cerr << "Error: option \"outfile\" must be set to desired output file name." << endl;
      cerr << desc << endl;
      return EXIT_FAILURE;
    }
  }
  catch(exception & e)
  {
    cerr << "Error: " << e.what() << endl;
    return EXIT_FAILURE;
  }
  catch(...)
  {
    cerr << "Unknown exception" << endl;
    return EXIT_FAILURE;
  }

  ManifoldFileReader reader;
  reader.load(meanFile);
  std::shared_ptr<MFDContainer> inContainer = reader.getContainer();

  // File was not loaded correctly
  if(!inContainer)
    return EXIT_FAILURE;

  if(inContainer->getNumElements() != 1)
  {
    cerr << "\"meanFile\" should have exactly one point to be read in as mean."
         << endl;
    return EXIT_FAILURE;
  }

  if(verbose)
  {
    cout << "Mean file header:" << endl;
    cout << *inContainer << endl << endl;

    cout << "Number of points to generate = " << number << endl;
    cout << "Sigma = " << sigma << endl;
    cout << "Output file = " << outfile << endl;
  }

  if(inContainer->getRepresentationType() == VECTOR)
    return generateRandomGaussianData<VectorType, VectorType>(reader, outfile,
                                                              number, sigma,
                                                              format, verbose);
  else if(inContainer->getRepresentationType() == MATRIX)
    return generateRandomGaussianData<MatrixType, MatrixType>(reader, outfile,
                                                              number, sigma,
                                                              format, verbose);
  //else if(inContainer->getRepresentationType() == VECTOR_TANGENT_BUNDLE)
  //  generateRandomGaussianData<VectorTBPointType, VectorTBTangentType>(reader, outfile, number, sigma, format, verbose);
  //else if(inContainer->getRepresentationType() == MATRIX_TANGENT_BUNDLE)
  //  generateRandomGaussianData<MatrixTBPointType, MatrixTBTangentType>(reader, outfile, number, sigma, format, verbose);
}

// TODO: An option to set the seed
