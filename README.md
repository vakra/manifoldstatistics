# ManifoldStatistics Project

This is a C++ library for general computations and statistics on Riemannian
manifolds. Algorithms for computing statistics are implemented generically, and
can be applied to multiple types of Riemannian manifolds. Several manifold types
are implemented, and the library can be extended to include more.

For information on installation, see the
[wiki](https://bitbucket.org/vakra/manifoldstatistics/wiki/Home).