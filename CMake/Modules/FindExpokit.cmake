find_library(EXPOKIT_LIBRARY
  NAMES expokit
  PATHS ""
  )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(EXPOKIT DEFAULT_MSG EXPOKIT_LIBRARY)

mark_as_advanced(EXPOKIT_LIBRARY)
