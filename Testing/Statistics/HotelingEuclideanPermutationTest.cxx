#include <iostream>
#include <fstream>
#include <string>
#include "MeanAndPGA.hxx"
#include "GeodesicRegression.hxx"
#include "RandomGenerator.hxx"
#include "RiemannianGeometryFactory.hxx"

typedef double Real;
typedef EuclideanGeometry<VectorType, VectorType> GeometryType;
typedef GeometryType::PointType ShapeType;
typedef GeometryType::TangentType TangentType;
typedef vector<ShapeType> ShapeListType;
typedef vector<TangentType> TangentListType;

typedef RiemannianGeometryFactory<ShapeType, TangentType> FactoryType;
typedef std::shared_ptr<GeometryType> GeometryPtr;

// Tangent bundle
typedef SasakiGeometry<ShapeType, TangentType> TMGeometryType;
typedef TMGeometryType::TMPointType TMPointType;
typedef TMGeometryType::TMTangentType TMTangentType;
typedef std::shared_ptr<TMGeometryType> TMGeometryPtr;

typedef vector<TMPointType> TMPointListType;

// for random generation
typedef RandomGenerator<ShapeType, TangentType> RandomType;

// To find individual geodesic trends (TMPointType)
typedef GeodesicRegression<ShapeType, TangentType> RegType;

// Need MeanAndPGA class to compute mean shape
typedef MeanAndPGA<ShapeType, TangentType> StatsType;

// Need this for averaging trends
typedef MeanAndPGA<TMPointType, TMTangentType> TMStatsType;

// Subject Struct
template<class ShapeType>
struct SubjectData
{
  unsigned int num;
  ScalarListType ageList;
  vector<ShapeType> shapeList;
};

typedef SubjectData<ShapeType> SubjectDataType;

// permute function
void permute(vector<unsigned int> & x)
{
  unsigned int i, j, sz;
  unsigned int swapTemp;
  double unifRand;

  sz = x.size();
  for(i = 0; i < sz - 1; i++)
  {
    unifRand = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    j = static_cast<unsigned int>(static_cast<double>(sz - i) * unifRand) + i;

    swapTemp = x[i];
    x[i] = x[j];
    x[j] = swapTemp;
  }
}

// individual regression closed form
void computeRegression(const ShapeListType & data, 
		       const ScalarListType & x,
		       ShapeType & base,
		       TangentType & tangent)
{
  MatrixType X(3, 2), Y(3, 512*3), result(2, 512*3);
  X.fill(1.0);
  X(0, 1) = x[0];
  X(1, 1) = x[1];
  X(2, 1) = x[2];

  for(int i = 0; i < x.size(); i++)
    Y.row(i) = strans(data[i]);

  result = inv(strans(X) * X) * strans(X) * Y;
  
  base = strans(result.row(0));
  tangent = strans(result.row(1));
}

// sasaki euclidean mean
void computeTrend(const TMPointListType & pointList,
		  TMPointType & mean,
		  GeometryPtr geom)
{
  int i, sz = pointList.size();
  ShapeType point = geom->getZeroVector();
  TangentType tangent = geom->getZeroVector();

  for(i = 0; i < sz; i++)
  {
    point += pointList[i].p;
    tangent += pointList[i].u;
  }

  mean.p = point / static_cast<double>(sz);
  mean.u = tangent / static_cast<double>(sz);
}

// half statistic
double computeHalfStat(const TMPointListType & pointList,
                       const TMPointType & meanPoint,
                       const TMTangentType & diff,
                       TMGeometryPtr tmGeom)
{
  TMTangentType logPoint;
  unsigned int i, j;

  unsigned int dimension = tmGeom->getExtrinsicDimension();
  unsigned int m = dimension / 2;

  unsigned int n = pointList.size();
  MatrixType covariance = zeros<MatrixType>(n, n);

  MatrixType x;
  x.set_size(m, n);

  VectorType v;
  v.set_size(m);

  for(i = 0; i < pointList.size(); i++)
  {
    logPoint = tmGeom->logMap(meanPoint, pointList[i]);
    //x.col(i) = logPoint.x;
    x.col(i) = logPoint.y;
  }

  MatrixType xx;
  xx = strans(x) * x;
  for(i = 0; i < n; i++)
  {
    for(j = i; j < n; j++)
    {
      covariance(i, j) = xx(i, j);
      covariance(j, i) = xx(i, j);
    }
  }

  covariance *= (1.0 / static_cast<double>(pointList.size() - 1));

  // v = diff.x; // wrt shape
  v = diff.y; // wrt trend

  MatrixType U, V; VectorType s;
  svd(U, s, V, covariance);

  double result = 0.0;
  VectorType vNew;
  x = strans(V) * strans(x);
  vNew.set_size(n);
  vNew = x * v;
  for(i = 1; i < vNew.size(); i++)
  {
    result += vNew[i] * vNew[i] / (s[i] * s[i]);
  }
  return result;
}

// Hoteling t^2 statistic
double computeTestStatistic(const TMPointListType & trendList0,
                            const TMPointListType & trendList1,
			    TMStatsType * tmStats,
			    GeometryPtr geom)
{
  TMGeometryPtr tmGeom = dynamic_pointer_cast<TMGeometryType>(tmStats->getRiemannianGeometry());
  TMPointType mean0, mean1;
  TMTangentType diff0, diff1;

  mean0 = trendList0[0];
  mean1 = trendList1[0];

  tmStats->computeMean(trendList0, mean0);
  tmStats->computeMean(trendList1, mean1);

  computeTrend(trendList0, mean0, geom);
  computeTrend(trendList1, mean1, geom);

  diff0 = tmGeom->logMap(mean0, mean1);
  diff1 = tmGeom->logMap(mean1, mean0);

  // std::cout << "|diff0| = " << tmGeom->norm(mean0, diff0) << std::endl;
  // std::cout << "|diff1| = " << tmGeom->norm(mean1, diff1) << std::endl;

  return computeHalfStat(trendList0, mean0, diff0, tmGeom) +
    computeHalfStat(trendList1, mean1, diff1, tmGeom);
}

// main function
int main(int argc, char ** argv)
{
  if(argc != 3)
  {
    cout << "Usage: HotelingPermutationTest inputFile numPermutations" << endl;
    return EXIT_FAILURE;
  }

  srand(51);

  // Variables we will need for regression
  unsigned int numberOfSubjects, shapeSize;
  unsigned int i, j, subjectNumber, prevSubjectNumber;
  TangentType oldTangent;

  // The Lists
  TMPointListType tmPointList;
  vector<SubjectDataType> subjectDataList;
  vector<unsigned int> groupIDs;
  ScalarListType x;
  ShapeListType data;

  // Reading shape data
  ifstream inFile, shapeFile;
  Real xVal, px, py, pz;
  string filename;
  unsigned int gid;

  inFile.open(argv[1]);
  inFile >> gid >> subjectNumber >> xVal >> filename;
  prevSubjectNumber = subjectNumber;

  unsigned int index = 0, tp;

  while(!inFile.eof())
  {
    SubjectDataType subj;
    subj.ageList = zeros<ScalarListType>(3);
    groupIDs.push_back(gid);
    while(subjectNumber == prevSubjectNumber)
    {
      subj.num = subjectNumber;
      (subj.ageList)[tp] = xVal;
      shapeFile.open(filename.c_str());
      cout << "Reading file: " << filename << endl;

      ShapeType shape;
      shape.set_size(512 * 3);
      while(shapeFile >> px >> py >> pz)
      {
        shape[index] = px;
        shape[index + 1] = py;
        shape[index + 2] = pz;
        index = index + 3;
      }

      // re-initalizing index
      index = 0;
      subj.shapeList.push_back(shape);
      shapeFile.close();

      inFile >> gid >> subjectNumber >> xVal >> filename;
      if(inFile.fail())
        break;

      // incrementing tp
      tp++;
    }

    subjectDataList.push_back(subj);
    // re-intializing
    tp = 0;
    prevSubjectNumber = subjectNumber;
  }

  numberOfSubjects = subjectDataList.size();
  shapeSize = subjectDataList[0].shapeList[0].size();
  tmPointList.resize(numberOfSubjects);

  // Checks
  // std::cout << "Number of subjects read = " <<
  // subjectDataList.size() << std::endl;
  // for(index = 0; index < numberOfSubjects; index++)
  // {
  //   std::cout << "SUBJECT " << subjectDataList[index].num << std::endl;
  //   std::cout << "group = " << groupIDs[index] << std::endl;
  //   std::cout << "Num shapes = " <<
  // subjectDataList[index].shapeList.size() << std::endl;
  //   std::cout << "Shape sizes = " << std::flush;
  //   for(i = 0; i < subjectDataList[index].shapeList.size(); i++)
  //     std::cout << subjectDataList[index].shapeList[i].size() <<
  // " " << std::flush;
  //   std::cout << std::endl;
  //   std::cout << "Ages = " << std::flush;
  //   for(i = 0; i < subjectDataList[index].ageList.size(); i++)
  //     std::cout << subjectDataList[index].ageList[i] << " " << std::flush;
  //   std::cout << std::endl;
  // }

  FactoryType factory;
  GeometryPtr euGeom;
  euGeom = dynamic_pointer_cast<GeometryType>(factory.create("Euclidean", shapeSize));
  RegType * reg = new RegType(euGeom);
  reg->setStepSize(0.01);
  reg->setMaxIterations(1000);

  StatsType * stats = new StatsType(euGeom);

  Real minX = 1.0e03, maxX = 0;
  for(i = 0; i < numberOfSubjects; i++)
  {
    // Rescale x to [0,1]
    x = subjectDataList[i].ageList;

    for(j = 0; j < x.size(); j++)
    {
      if(minX > x[j])
        minX = x[j];
      if(maxX < x[j])
        maxX = x[j];
    }
  }
  cout << "the min & max age is  = " << minX << ", " << maxX << endl;

  // Need to do regression for every subject
  for(i = 0; i < numberOfSubjects; i++)
  {
    //std::cout << "subjectNumber = " << i+1 << std::endl;
    x = subjectDataList[i].ageList;
    // for(j = 0; j < x.size(); j++)
    //   x[j] = (x[j] - minX) / (maxX - minX);

    if(data.size() != subjectDataList[i].shapeList.size())
      data.resize(subjectDataList[i].shapeList.size());

    double val = 0.0;
    // normalizing shapes or not
    for(j = 0; j < data.size(); j++)
      {
	data[j] = subjectDataList[i].shapeList[j];
      }

    // Initial base and tangent vectors
    ShapeType base = data[0];
    TangentType tangent(shapeSize);

    tangent = euGeom->getZeroVector();

    // Compute new meanShape and call it base
    stats->computeMean(data, base);

    Real xMean = 0.0;
    Real xSqrMean = 0.0;
    for(j = 0; j < x.size(); j++)
    {
      tangent += euGeom->logMap(base, data[j]) * x[j];
      xMean += x[j];
      xSqrMean += x[j] * x[j];
    }
    xMean /= static_cast<Real>(x.size());
    xSqrMean /= static_cast<Real>(x.size());
    tangent /= (static_cast<Real>(x.size()) * (xSqrMean - xMean * xMean));

    reg->computeGeodesicRegression(data, x, base, tangent);

    tmPointList[i].p = base;
    tmPointList[i].u = tangent;
  }

  TMGeometryPtr tmGeom;
  tmGeom = dynamic_pointer_cast<TMGeometryType>(factory.create("Euclidean", shapeSize));
  TMStatsType * tmStats = new TMStatsType(tmGeom);

  // Lists of tangent bundle trends for each group
  TMPointListType trendList0, trendList1;
  // Means for each group
  TMPointType mean0, mean1;

  for(i = 0; i < tmPointList.size(); i++)
  {
    if(groupIDs[i] == 0)
      trendList0.push_back(tmPointList[i]);
    else
      trendList1.push_back(tmPointList[i]);
  }

  mean0 = trendList0[0];
  mean1 = trendList1[0];

  // general compute mean
  // tmStats->computeMean(trendList0, mean0);
  // tmStats->computeMean(trendList1, mean1);

  // euclidean versions are closed form
  computeTrend(trendList0, mean0, euGeom);
  computeTrend(trendList1, mean1, euGeom);

  // Output mean trends

  std::ofstream outFile("females/caudateControlShape.txt");
  for(i = 0; i < (mean0.p.size()) / 3; i++)
    outFile << mean0.p[3 * i] << " " << mean0.p[3 * i + 1]
  	    << " " << mean0.p[3 * i + 2] << std::endl;
  outFile.close();

  outFile.open("females/caudateControlTrend.txt");
  for(i = 0; i < (mean0.u.size()) / 3; i++)
    outFile << mean0.u[3 * i] << " " << mean0.u[3 * i + 1]
  	    << " " << mean0.u[3 * i + 2] << std::endl;
  outFile.close();

  outFile.open("females/caudateCaseShape.txt");
  for(i = 0; i < (mean1.p.size()) / 3; i++)
    outFile << mean1.p[3 * i] << " " << mean1.p[3 * i + 1]
  	    << " " << mean1.p[3 * i + 2] << std::endl;
  outFile.close();

  outFile.open("females/caudateCaseTrend.txt");
  for(i = 0; i < (mean1.u.size()) / 3; i++)
    outFile << mean1.u[3 * i] << " " << mean1.u[3 * i + 1]
  	    << " " << mean1.u[3 * i + 2] << std::endl;
  outFile.close();

  // Experimental hypothesis test
  int numPermutes = atoi(argv[2]);
  double origTestStat, testStat;

  origTestStat = computeTestStatistic(trendList0, trendList1, tmStats, euGeom);
  cout << "Original test stat = " << origTestStat << endl;

  unsigned int count0, count1;
  unsigned int numBelow = 0;
  for(j = 0; j < numPermutes; j++)
  {
    permute(groupIDs);
    count0 = 0;
    count1 = 0;
    for(i = 0; i < tmPointList.size(); i++)
    {
      if(groupIDs[i] == 0)
      {
        trendList0[count0] = tmPointList[i];
        count0++;
      }
      else
      {
        trendList1[count1] = tmPointList[i];
        count1++;
      }
    }

    testStat = computeTestStatistic(trendList0, trendList1, tmStats, euGeom);
    if(testStat < origTestStat)
      numBelow++;
    std::cout << j << ": Test stat = " << testStat << "\t"
              << 1 - ((double) numBelow / (double)(j + 1)) << std::endl;
  }

  std::cout << "p value = " << 1 - ((double)(numBelow) / numPermutes) << std::endl;

  delete reg;
  delete stats;
  delete tmStats;

  return EXIT_SUCCESS;
}
