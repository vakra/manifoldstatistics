#include <iostream>
#include "RiemannianGeometryFactory.hxx"
#include "MeanAndPGA.hxx"
#include "RandomGenerator.hxx"

typedef SphereGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;
typedef GeometryType::PointListType PointListType;

typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;
typedef FactoryType::SharedGeometryPtr SharedGeometryPtr;

typedef RandomGenerator<PointType, TangentType> RandomType;
typedef MeanAndPGA<PointType, TangentType> StatsType;

void runTests(unsigned int dim)
{
  unsigned int i;
  unsigned int numPts = 1000;
  PointListType sphereData;

  PointType mean(dim + 1, 1);

  // Set up the geometry and statistics
  FactoryType factory;
  SharedGeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("Sphere", dim));

  StatsType * stats = new StatsType(geom);

  mean.fill(0.0);
  mean(0) = 1.0;

  // Generate random data on the sphere
  for(i = 0; i < numPts; i++)
  {
    PointType p = RandomType::randomGaussianPoint(geom, mean, M_PI / 20);
    sphereData.push_back(p);
  }

  // Compute the mean on the sphere
  stats->computeMean(sphereData, mean);
  cout << "Mean = " << mean << endl;
  cout << "Length = " << geom->norm(mean, mean) << endl;

  delete stats;
}

int main(int argc, char ** argv)
{
  RandomType::seed(100);

  cout << "*** Running 1D Tests ***" << endl;
  runTests(1);

  cout << endl << "*** Running 2D Tests ***" << endl;
  runTests(2);

  cout << endl << "*** Running 3D Tests ***" << endl;
  runTests(3);

  cout << endl << "*** Running 7D Tests ***" << endl;
  runTests(7);

  return EXIT_SUCCESS;
}
