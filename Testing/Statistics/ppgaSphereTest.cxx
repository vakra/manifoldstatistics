#include "ProbabilisticPGA.hxx"
#include "MeanAndPGA.hxx"
#include "RiemannianGeometryFactory.hxx"

//========================================================================
typedef SphereGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;
typedef vector<PointType> PointListType;
typedef std::shared_ptr<GeometryType> GeometryPtr;
typedef RandomGenerator<PointType, TangentType> RandomType;
typedef ProbabilisticPGA<PointType, TangentType> PPGA;

int main(int argc, char** argv)
{
  if (argc != 8)
  {
    cout << "ERROR: No. of arguments should be 8. Run the bash script."
	 << endl;
    cout << "Usage: PATH/TO/Testing/Data/Sphere/run_ppgaSphereTest.sh"
	 << endl << endl;
    return EXIT_FAILURE;
  }

  int seed = atoi(argv[1]);
  unsigned int dim = atoi(argv[2]);
  unsigned int dimReduce = atoi(argv[3]);
  unsigned int numPts = atoi(argv[4]);
  ScalarType trueSigma = atof(argv[5]);
  ScalarType initSigma = atof(argv[6]);
  int EMIter = atoi(argv[7]);

  RiemannianGeometryFactory<PointType, TangentType> factory;
  GeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("Sphere", dim));

  srand(seed);

  unsigned int extrinsicDim = geom->getExtrinsicDimension();

  MatrixType randomMat(extrinsicDim, extrinsicDim);
  PointType base, True_base;

  MatrixType w, True_w, x(dim, 2*numPts);
  MatrixType Lambda(dimReduce, dimReduce), True_Lambda(dim, dim), True_wLambda;
  PointListType data;
  PointType rgdHat, rgd;

  PPGA * ppga = new PPGA(geom, dimReduce, initSigma);

  // generate random ground-truth
  randomMat.fill(0.0);
  ppga->randOrthoMat(randomMat); // orthonormalising
  True_base = randomMat.col(0);
  True_w = randomMat.submat(0, 1, dim, dim);

  True_Lambda.fill(0.0);
  True_Lambda(0,0) = 0.4;
  True_Lambda(1,1) = 0.2;
  True_Lambda(2,2) = 0.0;

  // Generate random data on n sphere
  x.randn(); // get sample with sigma 1.0
  unsigned int counter = 0;
  unsigned int acc = 0;
  True_wLambda = True_w * True_Lambda;

  while (counter < numPts)
  {
    if (norm(True_wLambda * x.col(acc), "fro") < M_PI/2.0)
    {
      rgdHat = geom->expMap(True_base, True_wLambda * x.col(acc));

      rgd = RandomType::randomGaussianPoint(geom, rgdHat, trueSigma);

      data.push_back(rgd);

      counter++;
    }
    acc++;
  }

  // generate initialization
  randomMat.fill(0.0);
  ppga->randOrthoMat(randomMat); // orthonormalising
  base = randomMat.col(0);
  w = randomMat.submat(0, 1, dim, dimReduce);

  Lambda.fill(0.0);
  Lambda(0, 0) = 0.2;
  Lambda(1, 1) = 0.1;

  //*****************************RUN TEST*****************************//

  MatrixType current_q(dimReduce, numPts);  //current x_i vectors
  current_q.fill(0.0);
  ppga->EM(base, current_q, w, Lambda, data, EMIter);

  cout << "**************************************" << endl;
  cout << "True answer is, " << endl;
  cout << "True base = " << strans(True_base) << endl;
  cout << "True w = " << endl;
  True_w.print();
  cout << "True Lambda = " << endl;
  True_Lambda.print();

  cout << "**************************************" << endl;
  cout << "Using Ppga, " << endl;

  cout << "The order of estimated parameters need not be in "
       << "the same order as the true parameters." << endl;

  cout << "Estimated base = "<< strans(base) << endl;
  cout << "Estimated w = " << endl;
  w.print();
  cout << "Estimated Lambda = " << endl;
  Lambda.print();

  return EXIT_SUCCESS;
}
