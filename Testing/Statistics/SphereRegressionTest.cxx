#include <iostream>
#include "GeodesicRegression.hxx"
#include "RandomGenerator.hxx"
#include "RiemannianGeometryFactory.hxx"

typedef SphereGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;
typedef GeometryType::PointListType PointListType;

typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;
typedef FactoryType::SharedGeometryPtr SharedGeometryPtr;

typedef RandomGenerator<PointType, TangentType> RandomType;
typedef GeodesicRegression<PointType, TangentType> RegressionType;

void runTests(PointType & p, TangentType & v, double sigma,
              unsigned int n, unsigned int dim,
              PointType & startP, TangentType & startV)
{
  FactoryType factory;
  SharedGeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("Sphere", dim));
  RegressionType * regression = new RegressionType(geom);
  regression->setStepSize(0.05);
  regression->setMaxIterations(1000);

  PointListType pointList;
  ScalarListType scalarList(n);
  PointType mean, newPoint, finalP;
  TangentType finalV;

  unsigned int i;
  for(i = 0; i < n ; i++)
  {
    scalarList[i] = RandomType::realUniform(0, 1);
    mean = geom->expMap(p, scalarList[i] * v);
    newPoint = RandomType::randomGaussianPoint(geom, mean, sigma);
    pointList.push_back(newPoint);
  }

  finalP = startP;
  finalV = startV;
  regression->computeGeodesicRegression(pointList, scalarList,
                                        finalP, finalV);

  cout << "True p = " << strans(p) << endl;
  cout << "Initial p = " << strans(startP) << endl;
  cout << "Estimated p = " << strans(finalP) << endl;

  cout << "True v = " << strans(v) << endl;
  cout << "Initial v = " << strans(startV) << endl;
  cout << "Estimated v = " << strans(finalV) << endl;

  cout << "True v norm = " << geom->norm(p, v) << endl;
  cout << "Estimated v norm = "
       <<  geom->norm(finalP, finalV) << endl;

  delete regression;
}

int main(int argc, char ** argv)
{
  RandomType::seed(50);
  double dim = 2;

  unsigned int exDim = dim + 1;

  PointType p(exDim, 1);
  TangentType v(exDim, 1);

  PointType initP(exDim, 1);
  TangentType initV(exDim, 1);

  p.fill(0.0);
  p(0) = 1.0;

  v.fill(0.0);
  v(1) = M_PI * 0.25;

  initP[0] = cos(M_PI * 0.25);
  initP[1] = sin(M_PI * 0.25);
  initP[2] = 0.0;
  initV[0] = -sin(M_PI * 0.25);
  initV[1] = cos(M_PI * 0.25);
  initV[2] = 0.0;

  cout << "*** Running 2D Tests on the Sphere ***" << endl;
  runTests(p, v, 0.1, 20, dim, initP, initV);

  return EXIT_SUCCESS;
}
