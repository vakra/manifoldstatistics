#include <iostream>
#include "GeodesicRegression.hxx"
#include "RandomGenerator.hxx"
#include "RiemannianGeometryFactory.hxx"
#include <fstream>

typedef KendallShapeSpace2DGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;
typedef GeometryType::PointListType PointListType;

typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;
typedef std::shared_ptr<GeometryType> SharedGeometryPtr;

typedef RandomGenerator<PointType, TangentType> RandomType;
typedef GeodesicRegression<PointType, TangentType> RegressionType;

void runTests(SharedGeometryPtr geom, PointListType & data, ScalarListType & t,
	      DimensionList & dims, PointType & estBase, TangentType & estTgt)
{
  RegressionType * regression = new RegressionType(geom);
  regression->setStepSize(0.05);
  regression->setMaxIterations(1000);

  regression->computeGeodesicRegression(data, t, estBase, estTgt);

  delete regression;
}

int main(int argc, char ** argv)
{
  // Variables we will need
  unsigned int i, j;
  double sigma = M_PI / 80;

  // The Lists
  unsigned int numData = 100;
  ScalarListType x(numData);
  PointListType y;
  y.resize(numData);

  DimensionList dims;
  dims.resize(2);
  dims[0] = 2;
  dims[1] = 5;

  PointType tempPoint = arma::randu<PointType>(dims[0], dims[1]);

  // setting up the geometry via the factory
  FactoryType factory;
  SharedGeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("KendallShapeSpace", dims));

  // this pointer is to access the preshape function
  PointType trueBase(dims[0], dims[1]);
  trueBase = geom->preshape(tempPoint);

  TangentType tempTgt = arma::randu<PointType>(dims[0], dims[1]);

  TangentType trueTgt = geom->getZeroVector();
  geom->projectTangent(trueTgt, trueBase, tempTgt);

  for(i = 0; i < numData; i++)
  {
    x[i] = RandomType::realUniform(0.0, 1.0);
    y[i] = geom->expMap(trueBase, x[i] * trueTgt);
    y[i] = RandomType::randomGaussianPoint(geom, y[i], sigma);
  }

  // Checks
  cout << "Num data points = " << y.size() << endl;

  // True regression parameters
  cout << "********** Regression parameters should be close to *********" << endl;
  cout << "Base = " << endl;
  for(int i = 0; i < dims[1]; i++)
    cout << trueBase(0, i) << " " << trueBase(1, i) << endl;
  cout << endl;
  cout << "Tangent = " << endl;
  for(int i = 0; i < dims[1]; i++)
    cout << trueTgt(0, i) << " " << trueTgt(1, i) << endl;
  cout << endl;

  // Estimating regression parameters
  PointType estBase = y[0];
  TangentType estTgt = geom->getZeroVector();

  // initializing
  ScalarType xMean = 0.0;
  ScalarType xSqrMean = 0.0;
  for(int j = 0; j < x.size(); j++)
    {
      estTgt += geom->logMap(estBase, y[j]) * x[j];
      xMean += x[j];
      xSqrMean += x[j] * x[j];
    }
  xMean /= static_cast<ScalarType>(x.size());
  xSqrMean /= static_cast<ScalarType>(x.size());
  estTgt /= (static_cast<ScalarType>(x.size()) * (xSqrMean - xMean * xMean));

  // running regression tests
  runTests(geom, y, x, dims, estBase, estTgt);

  // Output results
  cout << "**********Estimated regression parameters*********" << endl;
  cout << "Estimated Base = " << endl;
  for(int i = 0; i < dims[1]; i++)
    cout << estBase(0, i) << " " << estBase(1, i) << endl;
  cout << endl;

  cout << "Estimated Tangent = " << endl;
  for(int i = 0; i < dims[1]; i++)
    cout << estTgt(0, i) << " " << estTgt(1, i) << endl;
  cout << endl;
}
