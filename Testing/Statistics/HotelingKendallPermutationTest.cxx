#include <iostream>
#include <fstream>
#include <string>
#include "MeanAndPGA.hxx"
#include "GeodesicRegression.hxx"
#include "RandomGenerator.hxx"
#include "RiemannianGeometryFactory.hxx"

// for p-threads
#include <pthread.h>
#include <stdio.h>
#include <assert.h>
#define NUM_THREADS 19

typedef double Real;
typedef KendallShapeSpaceGeometry GeometryType;
typedef GeometryType::PointType ShapeType;
typedef GeometryType::TangentType TangentType;
typedef vector<ShapeType> ShapeListType;
typedef vector<TangentType> TangentListType;

typedef RiemannianGeometryFactory<ShapeType, TangentType> FactoryType;
typedef std::shared_ptr<GeometryType> GeometryPtr;

// Tangent bundle
typedef SasakiGeometry<ShapeType, TangentType> TMGeometryType;
typedef TMGeometryType::TMPointType TMPointType;
typedef TMGeometryType::TMTangentType TMTangentType;
typedef std::shared_ptr<TMGeometryType> TMGeometryPtr;

typedef vector<TMPointType> TMPointListType;

// for random generation
typedef RandomGenerator<ShapeType, TangentType> RandomType;

// To find individual geodesic trends (TMPointType)
typedef GeodesicRegression<ShapeType, TangentType> RegType;

// Need MeanAndPGA class to compute mean shape
typedef MeanAndPGA<ShapeType, TangentType> StatsType;

// Need this for averaging trends
typedef MeanAndPGA<TMPointType, TMTangentType> TMStatsType;

// Subject Struct
template<class ShapeType>
struct SubjectData
{
  unsigned int num;
  ScalarListType ageList;
  ShapeListType shapeList;
};

typedef SubjectData<ShapeType> SubjectDataType;

// using threads
// structure for the p-thread
struct threadData {
  unsigned int threadID;
  ShapeType * base;
  TangentType * tangent;
  ShapeListType * shapeList;
  ScalarListType xVal;
};

struct threadData threadDataArray[NUM_THREADS];

// sampling on a thread
void * computeGeodesic(void *argument)
{
  struct threadData * subjData;
  subjData = (struct threadData *)(argument);
  //cout << subjData->threadID << " : print start" << endl;
  //cout << subjData->threadID << " : subjDataShape = " << subjData->base->n_rows << endl;
  //cout << subjData->threadID << " : subjDataTangent = " << subjData->tangent->n_cols << endl;
  //cout << subjData->threadID << " : subjDataShapeList = " << subjData->shapeList->size() << endl;
  //cout << subjData->threadID << " : xVal = " << (subjData->xVal)[0] << endl;
  //cout << subjData->threadID << " : print end" << endl;

  // threadID
  unsigned int tid = subjData->threadID;

  // theta
  ShapeType * subjShape = (ShapeType *)subjData->base;
  TangentType * subjTgt = (TangentType *)subjData->tangent;

  // shape data
  ShapeListType * subjShapeList = subjData->shapeList;

  // time
  ScalarListType subjX = subjData->xVal;

  DimensionList dims;
  dims.resize(2);
  dims[0] = 3;
  dims[1] = 512;

  // setting the pointers
  FactoryType factory;
  GeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("KendallShapeSpace", dims));
  RegType * reg = new RegType(geom);
  reg->setStepSize(0.5);
  reg->setMaxIterations(1000);

  StatsType * stats = new StatsType(geom);

  // Compute new meanShape and call it base
  stats->computeMean(*subjShapeList, *subjShape);

  Real xMean = 0.0;
  Real xSqrMean = 0.0;
  //cout << "subjX  size = " << subjX.size() << endl;
  //cout << "subjX = " << subjX[0] << endl;
  //cout << "subjShapeList for " << tid << "is = " << endl << (*subjShapeList)[0].col(0) << endl;

  for(int j = 0; j < subjX.size(); j++)
   {
     *subjTgt += geom->logMap(*subjShape, (*subjShapeList)[j]) * (subjX)[j];
     xMean += (subjX)[j];
     xSqrMean += (subjX)[j] * (subjX)[j];
   }

   xMean /= static_cast<Real>(subjX.size());
   xSqrMean /= static_cast<Real>(subjX.size());
   *subjTgt /= (static_cast<Real>(subjX.size()) * (xSqrMean - xMean * xMean));
   //cout << "subjTheta.u[0] = " << endl << subjTgt->col(0) << endl;

   // calling the regression function
   reg->computeGeodesicRegression(*subjShapeList, subjX, *subjShape, *subjTgt);

   return NULL;
}



// permute function
void permute(vector<unsigned int> & x)
{
  unsigned int i, j, sz;
  unsigned int swapTemp;
  double unifRand;

  sz = x.size();
  for(i = 0; i < sz - 1; i++)
  {
    unifRand = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    j = static_cast<unsigned int>(static_cast<double>(sz - i) * unifRand) + i;

    swapTemp = x[i];
    x[i] = x[j];
    x[j] = swapTemp;
  }
}

// half statistic
double computeHalfStat(const TMPointListType & pointList,
                       const TMPointType & meanPoint,
                       const TMTangentType & diff,
                       TMGeometryPtr tmGeom)
{
  TMTangentType logPoint;
  unsigned int i, j;

  unsigned int dimension = tmGeom->getExtrinsicDimension();
  unsigned int m = dimension / 2;

  unsigned int n = pointList.size();
  MatrixType covariance = zeros<MatrixType>(n, n);

  MatrixType x;
  x.set_size(m, n);

  VectorType v;
  v.set_size(m);

  for(i = 0; i < pointList.size(); i++)
  {
    logPoint = tmGeom->logMap(meanPoint, pointList[i]);
    //x.col(i) = logPoint.x;
    x.col(i) = logPoint.y;
  }

  MatrixType xx;
  xx = strans(x) * x;
  for(i = 0; i < n; i++)
  {
    for(j = i; j < n; j++)
    {
      covariance(i, j) = xx(i, j);
      covariance(j, i) = xx(i, j);
    }
  }

  covariance *= (1.0 / static_cast<double>(pointList.size() - 1));

  // v = diff.x; // wrt shape
  v = diff.y; // wrt trend

  MatrixType U, V; VectorType s;
  svd(U, s, V, covariance);

  double result = 0.0;
  VectorType vNew;
  x = strans(V) * strans(x);
  vNew.set_size(n);
  vNew = x * v;
  for(i = 1; i < vNew.size(); i++)
  {
    result += vNew[i] * vNew[i] / (s[i] * s[i]);
  }
  return result;
}

// Hoteling t^2 statistic
double computeTestStatistic(const TMPointListType & trendList0,
                            const TMPointListType & trendList1,
			    TMStatsType * tmStats,
			    GeometryPtr geom)
{
  TMGeometryPtr tmGeom = dynamic_pointer_cast<TMGeometryType>(tmStats->getRiemannianGeometry());
  TMPointType mean0, mean1;
  TMTangentType diff0, diff1;

  mean0 = trendList0[0];
  mean1 = trendList1[0];

  tmStats->computeMean(trendList0, mean0);
  tmStats->computeMean(trendList1, mean1);

  diff0 = tmGeom->logMap(mean0, mean1);
  diff1 = tmGeom->logMap(mean1, mean0);

  std::cout << "|diff0| = " << tmGeom->norm(mean0, diff0) << std::endl;
  std::cout << "|diff1| = " << tmGeom->norm(mean1, diff1) << std::endl;

  return computeHalfStat(trendList0, mean0, diff0, tmGeom) +
    computeHalfStat(trendList1, mean1, diff1, tmGeom);
}

// main function
int main(int argc, char ** argv)
{
  if(argc != 3)
  {
    cout << "Usage: HotelingPermutationTest inputFile numPermutations" << endl;
    return EXIT_FAILURE;
  }

  DimensionList dims;
  dims.resize(2);
  dims[0] = 3;
  dims[1] = 512;

  // this pointer is to access the preshape and setBasis functions
  GeometryType * kGeom = new GeometryType(dims);

  srand(51);

  // Variables we will need for regression
  unsigned int numberOfSubjects;
  unsigned int i, j, subjectNumber, prevSubjectNumber;
  TangentType oldTangent;

  // The Lists
  TMPointListType tmPointList;
  vector<SubjectDataType> subjectDataList;
  vector<unsigned int> groupIDs;
  ScalarListType x;
  ShapeListType data;

  // Reading shape data
  ifstream inFile, shapeFile;
  Real xVal, px, py, pz;
  string filename;
  unsigned int gid;

  inFile.open(argv[1]);
  inFile >> gid >> subjectNumber >> xVal >> filename;
  prevSubjectNumber = subjectNumber;

  unsigned int index = 0, tp;

  while(!inFile.eof())
  {
    SubjectDataType subj;
    subj.ageList = zeros<ScalarListType>(3);
    groupIDs.push_back(gid);
    while(subjectNumber == prevSubjectNumber)
    {
      subj.num = subjectNumber;
      (subj.ageList)[tp] = xVal;
      shapeFile.open(filename.c_str());
      cout << "Reading file: " << filename << endl;

      ShapeType shape;
      shape.set_size(dims[0], dims[1]);
      while(shapeFile >> px >> py >> pz)
      {
        shape(0, index) = px;
        shape(1, index) = py;
        shape(2, index) = pz;
        index = index + 1;
      }

      // re-initalizing index
      index = 0;

      subj.shapeList.push_back(shape);
      shapeFile.close();

      inFile >> gid >> subjectNumber >> xVal >> filename;
      if(inFile.fail())
        break;

      // incrementing tp
      tp++;
    }

    subjectDataList.push_back(subj);
    // re-intializing
    tp = 0;
    prevSubjectNumber = subjectNumber;
  }

  numberOfSubjects = subjectDataList.size();
  tmPointList.resize(numberOfSubjects);

  // Checks
  std::cout << "Number of subjects read = " <<
  subjectDataList.size() << std::endl;
  for(index = 0; index < numberOfSubjects; index++)
  {
    std::cout << "SUBJECT " << subjectDataList[index].num << std::endl;
    std::cout << "group = " << groupIDs[index] << std::endl;
    std::cout << "Num shapes = " <<
      subjectDataList[index].shapeList.size() << std::endl;
    std::cout << "Shape sizes = " << std::flush;
    for(i = 0; i < subjectDataList[index].shapeList.size(); i++)
      std::cout << subjectDataList[index].shapeList[i].n_cols
		<< " " << std::flush;
    std::cout << std::endl;
    std::cout << "Ages = " << std::flush;
    for(i = 0; i < subjectDataList[index].ageList.size(); i++)
      std::cout << subjectDataList[index].ageList[i] << " " << std::flush;
    std::cout << std::endl;
  }

  FactoryType factory;
  GeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("KendallShapeSpace", dims));
  RegType * reg = new RegType(geom);
  reg->setStepSize(0.5);
  reg->setMaxIterations(1000);

  StatsType * stats = new StatsType(geom);

  Real minX = 1.0e03, maxX = 0;
  for(i = 0; i < numberOfSubjects; i++)
  {
    // Rescale x to [0,1]
    x = subjectDataList[i].ageList;

    for(j = 0; j < x.size(); j++)
    {
      if(minX > x[j])
        minX = x[j];
      if(maxX < x[j])
        maxX = x[j];
    }
  }
  cout << "the min & max age is  = " << minX << ", " << maxX << endl;

  ofstream check("males/klog.txt");

  // *************** creating the PThreadArray **************
  pthread_t threads[NUM_THREADS];
  pthread_attr_t attr;
  unsigned int rc;
  void * status;

  ShapeListType dummyList[NUM_THREADS];
  ShapeType shapes[NUM_THREADS];
  TangentType tangents[NUM_THREADS];

  for(i = 0; i < NUM_THREADS; i++)
  {
    dummyList[i].resize(3);
    shapes[i].resize(1);
    tangents[i].resize(1);
  }

  /* Initialize and set thread detached attribute */
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  // Need to do regression for every subject
  for(i = 0; i < numberOfSubjects; i++)
  {
    x = subjectDataList[i].ageList;

    // time on a relative scale from 0 to 1
    for(j = 0; j < x.size(); j++)
      x[j] = (x[j] - minX) / (maxX - minX);

    if(data.size() != subjectDataList[i].shapeList.size())
      data.resize(subjectDataList[i].shapeList.size());

    double val = 0.0;

    // normalizing shapes to unit-norm
    for(j = 0; j < data.size(); j++)
      data[j] = kGeom->preshape(subjectDataList[i].shapeList[j]);

    // populating the subject data
    subjectDataList[i].ageList = x;
    subjectDataList[i].shapeList = data;

    shapes[i] = data[0];
    tangents[i] = zeros<TangentType>(3, 512);
    dummyList[i] = data;

    // initializing / setting the right sizes
    threadDataArray[i].threadID = i;
    threadDataArray[i].base = &shapes[i];
    threadDataArray[i].tangent = &tangents[i];
    threadDataArray[i].shapeList = &dummyList[i];
    threadDataArray[i].xVal = x;

    rc = pthread_create(&threads[i], &attr, computeGeodesic, (&threadDataArray[i]));
    cout << "created thread " << i << endl;
    if(rc) {
      cout << "ERROR; return code from pthread_join() is " << rc << endl;
      exit(-1);
    }
  }

  /* Free attribute and wait for the other threads */
  pthread_attr_destroy(&attr);

  // Joining Threads - wait for all threads to complete
  for (i = 0; i < NUM_THREADS; ++i)
  {
    // joining threads
    rc = pthread_join(threads[i], &status);
    if(rc) {
      cout << "ERROR; return code from pthread_join() is " << rc << endl;
      exit(-1);
    }
    cout << "Main: completed join with thread " << i
	 << " having a status of " << (long)(status) << endl;;

    ShapeType base = *(threadDataArray[i].base);
    TangentType tangent = *(threadDataArray[i].tangent);
    ShapeType target = kGeom->expMap(base, tangent);
    for(int k = 0; k < dims[1]; k++)
      check << base(0, k) << " " <<  base(1, k) << " " << base(2, k) << " "
	    << tangent(0, k) << " " << " " << tangent(1, k) << " "
	    << tangent(2, k) << " " << target(0, k) << " " << target(1, k) << " "
	    << target(2, k) << endl;
    check << endl;

    tmPointList[i].p = base;
    tmPointList[i].u = tangent;
  }

  cout << "PThreads complete. Regression done for all individuals! " << endl;

  // TMGeometryPtr tmGeom;
  // tmGeom = (TMGeometryPtr)factory.create("KendallShapeSpace", dims, true);
  // TMStatsType * tmStats = new TMStatsType(tmGeom);

  // // Lists of tangent bundle trends for each group
  // TMPointListType trendList0, trendList1;
  // // Means for each group
  // TMPointType mean0, mean1;

  // for(i = 0; i < tmPointList.size(); i++)
  // {
  //   if(groupIDs[i] == 0)
  //     trendList0.push_back(tmPointList[i]);
  //   else
  //     trendList1.push_back(tmPointList[i]);
  // }

  // mean0 = trendList0[0];
  // mean1 = trendList1[0];

  // // general compute mean
  // tmStats->computeMean(trendList0, mean0);
  // tmStats->computeMean(trendList1, mean1);

  // // Output mean trends

  // std::ofstream outFile("males/putamenControlKShape.txt");
  // for(i = 0; i < dims[1]; i++)
  //   outFile << mean0.p(0, i) << " " << mean0.p(1, i)
  // 	    << " " << mean0.p(2, i) << std::endl;
  // outFile.close();

  // outFile.open("males/putamenControlKTrend.txt");
  // for(i = 0; i < dims[1]; i++)
  //   outFile << mean0.u(0, i) << " " << mean0.u(1, i)
  // 	    << " " << mean0.u(2, i) << std::endl;
  // outFile.close();

  // outFile.open("males/putamenCaseKShape.txt");
  // for(i = 0; i < dims[1]; i++)
  //   outFile << mean1.p(0, i) << " " << mean1.p(1, i)
  // 	    << " " << mean1.p(2, i) << std::endl;
  // outFile.close();

  // outFile.open("males/putamenCaseKTrend.txt");
  // for(i = 0; i < dims[1]; i++)
  //   outFile << mean1.u(0, i) << " " << mean1.u(1, i)
  // 	    << " " << mean1.u(2, i) << std::endl;
  // outFile.close();

  // // Experimental hypothesis test
  // int numPermutes = atoi(argv[2]);
  // double origTestStat, testStat;

  // origTestStat = computeTestStatistic(trendList0, trendList1, tmStats, geom);
  // cout << "Original test stat = " << origTestStat << endl;

  // unsigned int count0, count1;
  // unsigned int numBelow = 0;
  // for(j = 0; j < numPermutes; j++)
  // {
  //   permute(groupIDs);
  //   count0 = 0;
  //   count1 = 0;
  //   for(i = 0; i < tmPointList.size(); i++)
  //   {
  //     if(groupIDs[i] == 0)
  //     {
  //       trendList0[count0] = tmPointList[i];
  //       count0++;
  //     }
  //     else
  //     {
  //       trendList1[count1] = tmPointList[i];
  //       count1++;
  //     }
  //   }

  //   testStat = computeTestStatistic(trendList0, trendList1, tmStats, geom);
  //   if(testStat < origTestStat)
  //     numBelow++;
  //   std::cout << j << ": Test stat = " << testStat << "\t"
  //             << 1 - ((double) numBelow / (double)(j + 1)) << std::endl;
  // }

  // std::cout << "p value = " << 1 - ((double)(numBelow) / numPermutes) << std::endl;
  // cout << "done! " << endl;

  delete kGeom;
  delete reg;
  delete stats;
  // delete tmStats;

  return EXIT_SUCCESS;
}
