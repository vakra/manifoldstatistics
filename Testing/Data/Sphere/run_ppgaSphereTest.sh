#!/bin/bash
execPath="Testing/Statistics"
seed=100; # set seed to generate random data
dim=3;
dimReduce=2;
numPts=200;
trueSigma=0.1; # ground truth
initSigma=0.4; # initialization
Emiter=100;

${execPath}/ppgaSphereTest ${seed} ${dim} ${dimReduce} ${numPts} ${trueSigma} ${initSigma} ${Emiter}
