/*=============================================================================
  File: PointListFileIOTest.cxx

  Short test program that puts the manifold file IO classes through their paces.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#include <iostream>
#include "ManifoldFileReader.hxx"
#include "ManifoldFileWriter.hxx"
#include "SphereGeometry.hxx"

typedef RiemannianGeometry<VectorType, VectorType> GeometryType;
typedef std::shared_ptr<GeometryType> GeometryPtr;

typedef VectorType PointType;
typedef std::vector<PointType> PointListType;

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    cout << "Usage: PointListFileIOTest inputFile" << endl;
    cout << "Choose a file containing point data from the Testing/Data/ folder."
         << endl;
    return EXIT_FAILURE;
  }

  unsigned int i;

  ManifoldFileReader reader;

  PointListType points, points2;

  reader.load(argv[1]);
  GeometryPtr geom = reader.getGeometry<VectorType, VectorType>();

  if(geom)
    cout << "Successfully loaded manifold file." << endl
         << "Header information:" << endl << *(reader.getContainer()) << endl;
  else
  {
    cout << "ERROR reading file header" << endl;
    return EXIT_FAILURE;
  }

  if(reader.readPointList<VectorType, VectorType>(points))
    cout << "Read point list containing " << points.size() << " points." << endl;
  else
  {
    cout << "ERROR reading point list" << endl;
    return EXIT_FAILURE;
  }

  // saving to file
  ManifoldFileWriter writer("TestBinaryOutput.mfd",
                            reader.getContainer()->getManifoldType(),
                            geom->getDimParameters(), true);
  if(writer.writePointList<VectorType, VectorType>(points))
    cout << "Successfully wrote data out as binary." << endl;
  else
    cout << "ERROR writing data as binary." << endl;

  writer.setFileName("TestTextOutput.mfd");
  writer.setIsBinary(false);
  if(writer.writePointList<VectorType, VectorType>(points))
    cout << "Successfully wrote data out as text." << endl;
  else
    cout << "ERROR writing data as text." << endl;

  reader.load("TestBinaryOutput.mfd");
  if(reader.readPointList<VectorType, VectorType>(points2))
    cout << "Successfully loaded written manifold file." << endl;
  else
  {
    cout << "ERROR loading written manifold file" << endl;
    return EXIT_FAILURE;
  }

  cout << "Binary read of "<< points2.size() << " points." << endl;

  for(i = 0; i < points.size(); i++)
  {
    if(norm(points[i] - points2[i], "fro") > 1.0e-10)
      cout << "ERROR: binary save/load result different" << endl;

    cout << strans(points2[i]) << flush;
  }
  cout << endl;

  return EXIT_SUCCESS;
}
