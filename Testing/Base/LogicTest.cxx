// Logic Test
//
// Ensures that primitive classes produce correct results.

#include <iostream>
#include "SasakiGeometry.hxx"

#define SCALAR_VALUE 10.0

typedef MatrixType PointType;
typedef MatrixType TangentType;

int main(int argc, char ** argv)
{
  cout << endl << "=== Running Consistency & Correctness Checks ===" << endl;
  TangentType p1, p2, p3, p4, p5, p6;
  TangentBundleTangentType<TangentType> t1, t2, t3, t4, t5;

  // scalar
  ScalarType value = SCALAR_VALUE;
  p1 = value;
  cout << "Integer check (should be " << SCALAR_VALUE << "): " << p1 << endl << endl;

  // arma matrix
  MatrixType mat = randu<MatrixType>(3, 3);
  p1 = mat;
  cout << "Matrix check: " << endl << p1 << endl;

  // scalar multiply
  p2 = 2 * p1;
  cout << "2 * matrix: " << endl << p2 << endl;

  // scalar multiply
  p3 = p1 * 3;
  cout << "Matrix * 3: " << endl << p3 << endl;

  // scalar divide
  p4 = p1 / 2;
  cout << "Matrix / 2: " << endl << p4 << endl;

  // invert
  p5 = -p1;
  cout << "-matrix: " << endl << p5 << endl;

  // matrix multiply
  p6 = p1 * p1;
  cout << "Matrix * matrix: " << endl << p6 << endl;

  // sasaki
  t1.x = p1;
  t1.y = p2;
  cout << "Bundle check: " << endl << t1.x << endl << t1.y << endl;

  // scalar multiply
  t3 = t1 * 3;
  cout << "Bundle * 3: " << endl << t3.x << endl << t3.y << endl;

  // scalar divide
  t4 = t1 / 2;
  cout << "Bundle / 2: " << endl << t4.x << endl << t4.y << endl;

  // invert
  t5 = -t1;
  cout << "-bundle: " << endl << t5.x << endl << t5.y << endl;

  cout << "Done." << endl;

  return 0;
}

