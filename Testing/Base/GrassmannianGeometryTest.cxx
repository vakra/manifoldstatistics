#include "RiemannianGeometryFactory.hxx"

typedef GrassmannianGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;

typedef RiemannianGeometryFactory<MatrixType, MatrixType> FactoryType;
typedef shared_ptr<GeometryType> GeometryPtr;

void runTests(unsigned int dim, unsigned int num)
{
  FactoryType factory;
  GeometryPtr geom;

  DimensionList dims;
  dims.resize(2);

  dims[0] = dim;
  dims[1] = num;

  geom = dynamic_pointer_cast<GeometryType>(factory.create("Grassmannian", dims));

  PointType base = randu<mat>(dim, num);
  base.print("base = ");

  PointType newBase = geom->preshape(base);
  newBase.print("newBase = ");
  TangentType v(dim, num), vProj(dim, num);
  TangentType w(dim, num), wProj(dim, num);

  v = randu<mat>(dim, num);
  w = randu<mat>(dim, num);
  v.print("v = ");
  w.print("w = ");

  // projecting onto the tangent space
  geom->projectTangent(vProj, newBase, v);
  geom->projectTangent(wProj, newBase, w);

  vProj.print("vProj = ");
  wProj.print("wProj = ");

  // Check if projected vectors are actually tangents
  MatrixType check;
  check = strans(newBase) * vProj;
  check.print("is vProj tangent? ");
  check = strans(newBase) * wProj;
  check.print("is wProj tangent? ");

  // innerProd
  cout << "<vProj, wProj> = "
       << geom->innerProduct(newBase, vProj, wProj) << endl;

  // ExpMap
  PointType target = geom->expMap(newBase, vProj);
  target.print("target = ");

  // LogMap
  TangentType vLog = geom->logMap(newBase, target);
  vLog.print("vLog = ");

  // Is target orthogonal?
  MatrixType id;
  id = strans(target) * target;
  id.print("id? = ");

  // TangentToList
  ScalarListType list;
  geom->tangentToList(vProj, list);
  cout << "list = " << endl << list << endl;

  // ListToTangent
  TangentType temp;
  temp = zeros<mat>(dim, num);
  geom->listToTangent(list, temp);
  temp = temp - vProj;
  double val = geom->norm(newBase, temp);
  cout << "testing TangentToListToTangent" << endl;
  cout << "norm(temp - vProj) = " << val << endl;

  // Parallel Translation
  TangentType vPara = geom->parallelTranslate(newBase, vProj, vProj);
  vPara.print("vPara = ");

  TangentType wPara = geom->parallelTranslate(newBase, vProj, wProj);
  wPara.print("wPara = ");

  // Check whether vPara and wPara are tangents to target
  TangentType vParaProjected, wParaProjected;

  geom->projectTangent(vParaProjected, target, vPara);
  geom->projectTangent(wParaProjected, target, wPara);

  vParaProjected.print("vParaProjected = ");
  wParaProjected.print("wParaProjected = ");

  // innerProduct checks after parallel translation
  val = geom->norm(newBase, vProj);
  cout << "norm(vProj) =  " << val << endl;
  val = geom->norm(target, vPara);
  cout << "norm(vPara) =  " << val << endl;
  val = geom->norm(newBase, wProj);
  cout << "norm(wProj) =  " << val << endl;
  val = geom->norm(target, wPara);
  cout << "norm(wPara) =  " << val << endl;
  val = geom->innerProduct(newBase, vProj, wProj);
  cout << "<vProj, wProj> = " << val << endl;
  val = geom->innerProduct(target, vPara, wPara);
  cout << "<vPara, wPara> = " << val << endl;
}

int main(int argc, char ** argv)
{
  cout << "Armadillo version: " << arma_version::as_string() << endl;

  std::cout << "*** Running GrassmannianGeometry Tests ***" << std::endl;
  unsigned int dim = 3, num = 2;
  runTests(dim, num);
  return EXIT_SUCCESS;
}
