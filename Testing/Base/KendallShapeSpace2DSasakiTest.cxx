#include <iostream>
#include "RiemannianGeometryFactory.hxx"
#include "RandomGenerator.hxx"
#include "MeanAndPGA.hxx"

typedef KendallShapeSpace2DGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;
typedef std::shared_ptr<GeometryType> GeometryPtr;

typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;

typedef RandomGenerator<PointType, TangentType> RandomType;
typedef MeanAndPGA<PointType, TangentType> StatsType;

typedef SasakiGeometry<PointType, TangentType> TMGeometryType;
typedef TMGeometryType::TMPointType TMPointType;
typedef TMGeometryType::TMTangentType TMTangentType;
typedef RandomGenerator<TMPointType, TMTangentType> TMRandomType;

typedef std::shared_ptr<TMGeometryType> TMGeometryPtr;

void runTests(unsigned int dim, unsigned int num)
{
  // Variables we will need
  unsigned int i, j;
  double sigma = M_PI / 80;

  DimensionList dims;
  dims.resize(2);
  dims[0] = dim;
  dims[1] = num;

  // setting up the geometry via the factory
  FactoryType factory;
  GeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("KendallShapeSpace", dims));

  // SasakiGeometry
  TMGeometryPtr tmGeom;
  tmGeom.reset(new TMGeometryType(geom));

  TMPointType a, b, c, d;
  TMTangentType v, w, t, vTrans, wTrans;

  PointType p1(dim, num), p2(dim, num);
  TangentType u1(dim, num), u2(dim, num);

  a.p.fill(0.0);
  a.u.fill(0.0);
  b.p.fill(0.0);
  b.u.fill(0.0);

  p1(0, 0) = 0.18;
  p1(0, 2) = 3.2;
  p1(0, 3) = 1.7;
  p1(1, 0) = 0.8;
  p1(1, 1) = 0.2;
  p1(1, 3) = 0.7;

  u1(0, 1) = 0.2;
  u1(0, 4) = 0.3;
  u1(1, 2) = 0.2;
  u1(1, 4) = 0.1;

  p2 = RandomType::randomGaussianPoint(geom, p1, sigma);
  u2 = RandomType::randomGaussianPoint(geom, u1, sigma);

  a.p = geom->preshape(p1);
  b.p = geom->preshape(p2);

  geom->projectTangent(a.u, a.p, u1);
  geom->projectTangent(b.u, b.p, u2);

  cout << "rank(a.p) = " << arma::rank(a.p) << endl;
  cout << "rank(b.p) = " << arma::rank(b.p) << endl;

  cout << "a = " << endl << a.p << endl << a.u << endl;
  cout << "b = " << endl << b.p << endl << b.u << endl;

  t = tmGeom->logMap(a, b);
  tmGeom->projectTangent(v, a, t);
  cout << "v = log(a, b) = " << endl << v.x << endl << v.y << endl;

  // getting a random point on Tangent bundle
  d = TMRandomType::randomGaussianPoint(tmGeom, a, sigma);

  geom->projectTangent(u1, d.p, d.u);
  d.u = u1;
  cout << "d = randomGaussianPoint(a, sigma) = " << endl << d.p << endl << d.u << endl;

  t = tmGeom->logMap(a, d);
  tmGeom->projectTangent(w, a, t);
  cout << "w = log(a, d) = " << endl << w.x << endl << w.y << endl;

  cout << " ***** Is c = exp(a, v) = b? ***** " << endl;
  cout << "Dist(a, b) = " << tmGeom->distance(a, b) << endl;

  c = tmGeom->expMap(a, v);
  cout << "Dist(c, b) = " << tmGeom->distance(c, b) << endl;
  cout << "Dist(a, c) = " << tmGeom->distance(a, c) << endl;

  vTrans = v;
  wTrans = w;

  vTrans = tmGeom->parallelTranslate(a, v, v);
  wTrans = tmGeom->parallelTranslate(a, v, w);

  cout << " ***** Testing inner products on the tangent bundle of 2D shape space ***** " << endl;
  cout << "<a, v> = " << (geom->innerProduct(a.p, a.p, v.x) +
   			  geom->innerProduct(a.p, a.p, v.y))
       << endl;
  cout << "<b, vTrans> = " << (geom->innerProduct(b.p, b.p, vTrans.x) +
			       geom->innerProduct(b.p, b.p, vTrans.y))
       << endl;

  double val = tmGeom->innerProduct(a, v, w);
  cout << "<v, w> = " << val << endl;
  val = tmGeom->innerProduct(b, vTrans, wTrans);
  cout << "<vTrans, wTrans> = " << val << endl;
}

int main(int argc, char ** argv)
{
  cout << "*** Running 2D Kendall Shape Sasaki Tests ***" << endl;
  srand(51);
  runTests(2, 5);
  return EXIT_SUCCESS;
}
