#include "SpecialOrthogonalGroupGeometry.hxx"
#include "SpecialLinearGroupGeometry.hxx"

typedef SpecialOrthogonalGroupGeometry SOGeometryType;
typedef SOGeometryType::PointType SOPointType;
typedef SOGeometryType::TangentType SOTangentType;

typedef SpecialLinearGroupGeometry SLGeometryType;
typedef SLGeometryType::PointType SLPointType;
typedef SLGeometryType::TangentType SLTangentType;

void myprint(string head, mat m)
{
  cout << head << endl;

  streamsize p = cout.precision(10);
  streamsize s = cout.width(16);
  cout.setf(ios::fixed);

  m.raw_print(cout);
  cout << endl;

  cout.width(s);
  cout.precision(p);
  cout.unsetf(ios::fixed);
}

void runTests(unsigned int n)
{
  SOGeometryType * geom = new SOGeometryType(n);

  SOPointType g(n, n), h(n, n);

  SOPointType id(n, n);
  id.eye(n, n);

  SOTangentType u = randu<mat>(n, n);
  SOTangentType v = randu<mat>(n, n);
  SOTangentType w = randu<mat>(n, n);
  SOTangentType z = randu<mat>(n, n);

  u = u - trans(u);
  v = v - trans(v);
  w = w - trans(w);
  z = z - trans(z);

  // Test exp and log maps
  myprint("u =", u);
  myprint("v =", v);
  myprint("w =", w);
  myprint("z =", z);

  g = geom->expMapAtId(u);

  myprint("g = expMapAtId(u) =", g);
  myprint("logMapAtId(g) =", geom->logMapAtId(g));

  h = geom->expMap(g, g * v);
  myprint("g * v =", g * v);
  myprint("h = expMap(g, g * v) =", h);
  myprint("logMap(g, h) =", geom->logMap(g, h));

  // Test parallel translation at id
  SOTangentType paraU = geom->parallelTranslateAtId(u, u);
  SOTangentType paraV = geom->parallelTranslateAtId(u, v);
  SOTangentType paraW = geom->parallelTranslateAtId(u, w);
  myprint("parallelTranslateAtId(u, u) =", paraU);
  myprint("parallelTranslateAtId(u, v) =", paraV);
  myprint("parallelTranslateAtId(u, w) =", paraW);

  // Inner product of parallel translation test
  cout << "These inner products should be equal (in pairs)." << endl;
  cout << "<u, u>         = " << geom->normSquaredAtId(u) << endl;
  cout << "<paraU, paraU> = " << geom->normSquaredAtId(paraU) << endl;
  cout << "<u, v>         = " << geom->innerProductAtId(u, v) << endl;
  cout << "<paraU, paraV> = " << geom->innerProductAtId(paraU, paraV) << endl;
  cout << "<v, w>         = " << geom->innerProductAtId(v, w) << endl;
  cout << "<paraV, paraW> = " << geom->innerProductAtId(paraV, paraW) << endl;

  // Now parallel translation at another point
  h = geom->expMap(g, g * u);
  paraU = geom->parallelTranslate(g, g * u, g * u);
  paraV = geom->parallelTranslate(g, g * u, g * v);
  paraW = geom->parallelTranslate(g, g * u, g * w);
  myprint("h^-1 * parallelTranslate(g, g * u, g * u) =",
          geom->inverse(h) * paraU);
  myprint("h^-1 * parallelTranslate(g, g * u, g * v) =",
          geom->inverse(h) * paraV);
  myprint("h^-1 * parallelTranslate(g, g * u, g * w) =",
          geom->inverse(h) * paraW);

  // Inner product of parallel translation test
  cout << "These inner products should be equal (in pairs)." << endl;
  cout << "<u, u>         = " << geom->normSquared(g, g * u) << endl;
  cout << "<paraU, paraU> = " << geom->normSquared(h, paraU) << endl;
  cout << "<u, v>         = " << geom->innerProduct(g, g * u, g * v) << endl;
  cout << "<paraU, paraV> = " << geom->innerProduct(h, paraU, paraV) << endl;
  cout << "<v, w>         = " << geom->innerProduct(g, g * v, g * w) << endl;
  cout << "<paraV, paraW> = " << geom->innerProduct(h, paraV, paraW) << endl
       << endl;

  // Test sectional curvature
  cout << "sectionalCurvatureAtId(u, v) = "
       << geom->sectionalCurvatureAtId(u, v) << endl;
  cout << "sectionalCurvature(g * u, g * v) = "
       << geom->sectionalCurvature(g, g * u, g * v) << endl;
  cout << "sectionalCurvature(g * v, g * v) = "
       << geom->sectionalCurvature(g, g * v, g * v) << endl << endl;


  // Test Jacobi fields
  SOTangentType j0 = v;
  SOTangentType dj0 = w;

  SOTangentType j1 = j0;
  SOTangentType dj1 = dj0;

  myprint("g =", g);
  myprint("u =", u);

  myprint("j(0) =", j0);
  myprint("dj(0) =", dj0);
  geom->jacobiFieldAtId(u, j1, dj1);
  myprint("j(1) =", j1);

  // Finite difference approximation of Jacobi field
  ScalarType epsilon = 1.0e-6;
  SOPointType gEps = geom->expMapAtId(epsilon * j0);
  SOTangentType uEps;
  uEps = gEps * geom->parallelTranslateAtId(epsilon * j0, u + epsilon * dj0);
  h = geom->expMap(gEps, uEps);

  SOPointType j1FD = geom->inverse(g) * geom->logMap(g, h) / epsilon;

  myprint("finite difference j(1) =", j1FD);

  // Compare to SL(n) version of Jacobi fields
  SLGeometryType * geom2 = new SLGeometryType(n);

  SLPointType slj1 = j0;
  SLPointType sldj1 = dj0;
  geom2->jacobiFieldAtId(u, slj1, sldj1);

  myprint("SL(n) version of j(1) =", slj1);

  myprint("dj(1) =", dj1);
  myprint("SL(n) version of dj(1) =", sldj1);

  // Backward Jacobi fields (should get back to initial conditions)
  g = geom->expMapAtId(u);
  SOTangentType revU = geom->translate(g, -geom->parallelTranslateAtId(u, u));
  SOTangentType revJ0 = geom->translate(g, j1);
  SOTangentType revDJ0 = geom->translate(g, -dj1);
  geom->jacobiField(g, revU, revJ0, revDJ0);

  myprint("j(0) =", j0);
  myprint("reverse j(0) =", revJ0);
  myprint("-dj(0) =", -dj0);
  myprint("reverse dj(0) =", revDJ0);

  // Test adjoint Jacobi fields
  j1 = j0;
  dj1 = dj0;
  geom->jacobiFieldAtId(u, j1, dj1);
  SOTangentType z1 = geom->parallelTranslateAtId(u, z);
  revJ0 = geom->translate(g, z1);
  revDJ0 = geom->getZeroVector();
  geom->adjointJacobiField(g, revU, revJ0, revDJ0);
  myprint("adjoint J0 =", revJ0);
  myprint("adjoint dJ0 =", revDJ0);

  cout << "<j1, z1> = " << geom->innerProductAtId(j1, z1) << endl;
  cout << "<adjJ0, j0> + <adjDJ0, dj0> = "
       << geom->innerProductAtId(revJ0, j0) +
          geom->innerProductAtId(revDJ0, dj0)
       << endl;

  delete geom;
  delete geom2;
}

int main(int argc, char ** argv)
{
  std::srand(50);
  cout.precision(10);

  cout << "**********************************************" << endl
       << "     Running SO(2) Tests" << endl
       << "**********************************************" << endl;
  runTests(2);

  cout << "**********************************************" << endl
       << "     Running SO(3) Tests" << endl
       << "**********************************************" << endl;
  runTests(3);

  cout << "**********************************************" << endl
       << "     Running SO(6) Tests" << endl
       << "**********************************************" << endl;
  runTests(6);

  cout << "**********************************************" << endl
       << "     Running SO(7) Tests" << endl
       << "**********************************************" << endl;
  runTests(7);

  return EXIT_SUCCESS;
}
