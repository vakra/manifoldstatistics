#include <iostream>
#include "RiemannianGeometryFactory.hxx"
#include "RandomGenerator.hxx"

typedef KendallShapeSpaceGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;

typedef RiemannianGeometryFactory<MatrixType, MatrixType> FactoryType;
typedef std::shared_ptr<GeometryType> GeometryPtr;

typedef RandomGenerator<PointType, TangentType> RandomType;

void runTests(unsigned int dim, unsigned int num)
{
  FactoryType factory;
  GeometryPtr geom;

  DimensionList dims;
  dims.resize(2);

  dims[0] = dim;
  dims[1] = num;

  geom = dynamic_pointer_cast<GeometryType>(factory.create("KendallShapeSpace", dims));

  unsigned int exDim = geom->getExtrinsicDimension();

  PointType p = randu<PointType>(dim, num);
  p = geom->preshape(p);
  cout << "p = " << endl << p << endl;

  PointType q(dim, num);
  q = RandomType::randomGaussianPoint(geom, p, 3);
  cout << "q = " << endl << q << endl;

  TangentType t(dim, num);
  t = geom->logMap(p, q);
  cout << "t = log(p, q) = " << endl << t << endl;

  PointType expT = geom->expMap(p, t);
  cout << "exp(t) = " << endl << expT << endl;
  cout << "Is exp(t) = q? -- " << "d(exp(t), q) = "
       << geom->distance(expT, q) << endl;

  cout << "d(p, q) = " << geom->distance(p, q) << endl;
  cout << "d(p, exp(t)) = " << geom->distance(p, expT) << endl;
  cout << endl;

  // Testing the horizontal projection
  cout << "***** Testing the horizontal projection ****" << endl;
  TangentType vTemp = (0.5) * randu<TangentType>(dim, num);
  cout << "vTemp = " << endl << vTemp << endl;
  cout << "Check if vTemp is a tangent at q: Is q * t(vTemp) sym? "
       << endl << q * strans(vTemp) << endl;

  TangentType v = vTemp;
  geom->projectTangent(v, q, vTemp);
  cout << "After projecting vTemp onto v = " << endl << v << endl;
  cout << "Check if v is a tangent at q: Is q * t(v) sym? "
       << endl << q * strans(v) << endl;

  // Exp
  PointType qv = geom->expMap(q, v);
  cout << "qv = exp(q, v) = " << endl << qv << endl;

  TangentType u = geom->logMap(q, qv);
  cout << "u = log(q, qv) = " << endl << u << endl;

  cout << "norm(v) = " << geom->norm(q, v) << endl;
  cout << "norm(u) = " << geom->norm(q, u) << endl;

  PointType qu = geom->expMap(q, u);
  cout << "exp(u) = qu = " << endl << qu << endl;
  cout << "Is qv = qu? " << endl << "d(qv, qu) = "
       << geom->distance(qv, qu) << endl;

  // New tangent vectors w, x, y
  TangentType wTemp = randu<TangentType>(dim, num);
  TangentType w = wTemp;
  geom->projectTangent(w, q, wTemp);

  TangentType xTemp = randu<TangentType>(dim, num);
  TangentType x = xTemp;
  geom->projectTangent(x, q, xTemp);

  TangentType yTemp = randu<TangentType>(dim, num);
  TangentType y = yTemp;
  geom->projectTangent(y, q, yTemp);
  cout << endl;

  // Parallel Translation
  cout << " ***** Testing parallel translation ***** " << endl;

  cout << endl << "Parallel translating v along v: " << endl;
  TangentType vv = geom->parallelTranslate(q, v, v);
  cout << "vv = parTrans(q, v, v) = " << endl << vv << endl;
  cout << "Check if vv is a tangent at qv: qv * t(vv) sym? "
       << endl << qv * strans(vv) << endl;

  cout << "<v, v> = " << geom->innerProduct(q, v, v) << endl;
  cout << "<vv, vv> = " << geom->innerProduct(qv, vv, vv) << endl;

  TangentType ww = geom->parallelTranslate(q, w, w);

  cout << endl << "Parallel translating w along w: " << endl;
  cout << "ww = parTrans(q, w, w) = " << endl << ww << endl;

  PointType qw(dim, num);
  qw = geom->expMap(q, w);

  cout << "<w, w> = " << geom->innerProduct(q, w, w) << endl;
  cout << "<ww, ww> = " << geom->innerProduct(qw, ww, ww) << endl;

  // parallel translating one along another
  TangentType wv = geom->parallelTranslate(q, v, w);
  cout << endl << "Parallel translating w along v: " << endl;
  cout << "wv = parTrans(q, v, w) = " << endl << wv << endl;
  cout << "Check if wv is a tangent at qv: qv * t(wv) sym? "
       << endl << qv * strans(wv) << endl;

  cout << "<v, w> = " << geom->innerProduct(q, v, w) << endl;
  cout << "<vv, wv> = " << geom->innerProduct(qv, vv, wv) << endl;

  TangentType vw = geom->parallelTranslate(q, w, v);
  cout << endl << "Parallel translating v along w: " << endl;
  cout << "vw = parTrans(q, w, v) = " << endl << vw << endl;
  cout << "<w, v> = " << geom->innerProduct(q, w, v) << endl;
  cout << "<ww, vw> = " << geom->innerProduct(qw, ww, vw) << endl;
  cout << endl;

  // Test Jacobi fields
  cout << " ***** Testing Jacobi fields ***** " << endl;
  TangentType j0 = w;
  TangentType dj0 = x;

  TangentType j1 = j0;
  TangentType dj1 = dj0;

  cout << "j(0) = " << endl << j0 << endl;
  cout << "dj(0) = " << endl << dj0 << endl;

  geom->jacobiField(q, v, j1, dj1);
  cout << "j(1) = " << endl << j1 << endl;
  cout << "dj(1) = " << endl << dj1 << endl;

  // Backward Jacobi fields (should get back to initial conditions)
  TangentType revV = geom->logMap(qv, q);
  TangentType revJ0 = j1;
  TangentType revDJ0 = -dj1;
  geom->jacobiField(qv, revV, revJ0, revDJ0);

  cout << " **** Comparing backward jacobi fields with inits **** " << endl;
  cout << "j(0) = " << endl << j0 << endl;
  cout << "rev j(0) = " << endl << revJ0 << endl;
  cout << "-dj(0) = " << endl << -dj0 << endl;
  cout << "rev dj(0) = " << endl << revDJ0 << endl;

  // Test adjoint Jacobi fields
  cout << " ***** Testing adjoint Jacobi fields ***** " << endl;
  j1 = j0;
  dj1 = geom->getZeroVector();
  geom->jacobiField(q, v, j1, dj1);
  TangentType y1 = geom->parallelTranslate(q, v, y);
  revJ0 = y1;
  revDJ0 = geom->getZeroVector();
  geom->adjointJacobiField(qv, revV, revJ0, revDJ0);
  cout << "adjoint J0 = " << endl << revJ0 << endl;

  cout << "<j1, y1> = " << geom->innerProduct(qv, j1, y1) << endl;
  cout << "<adjJ0, j0> = " << geom->innerProduct(q, revJ0, j0) << endl;

  // Curvature
  cout << "****** Testing curvature ********** "<< endl;
  TangentType curv = geom->curvatureTensor(q, v, w, x);
  cout << "<curv, y> = " << geom->innerProduct(q, curv, y) << endl;

  double sec = geom->sectionalCurvature(q, v, w);
  cout << "K(v, w) = " << sec << endl;
}

int main(int argc, char ** argv)
{
  cout << "*** Running 3D Kendall Shape Tests ***" << endl;
  srand(51);
  runTests(3, 6);
  return EXIT_SUCCESS;
}
