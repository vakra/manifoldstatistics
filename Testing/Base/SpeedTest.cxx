// Speed Test
//
// Measures performance.

#include <iostream>
#include "SasakiGeometry.hxx"

#define ITERATIONS 1500000

typedef MatrixType PointType;
typedef MatrixType TangentType;

int main(int argc, char ** argv)
{
  cout << endl << "=== Vector Operations Timing Test ===" << endl;

  MatrixType mat = randu<MatrixType>(3, 1);
  TangentType t1(mat);
  mat = randu<MatrixType>(2, 2);
  TangentType t2(mat);
  mat = randu<MatrixType>(5, 7);
  TangentType t3(mat);

  // cout << t1 << endl << t2 << endl << t3 << endl << "Begin timing." << endl;

  time_t start, end;
  time(& start);
  for(int iter = 0; iter < ITERATIONS; iter++)
  {
    t1 += (3 * (t1 - -t1)) / 2;
    t2 += (3 * (t2 - -t2)) / 2;
    t3 += (3 * (t3 - -t3)) / 2;
    t1 -= (3 * (t1 - -t1));
    t2 -= (3 * (t2 - -t2));
    t3 -= (3 * (t3 - -t3));
  }
  time(& end);

  cout << "Done." << endl;

  // cout << t1 << endl << t2 << endl << t3 << endl << "Begin timing." << endl;
  
  cout << "Seconds to complete all computations: " << difftime(end, start) << endl;

  return 0;
}
