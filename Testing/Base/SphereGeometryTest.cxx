/*==============================================================================
  File: SphereGeometryTest.cxx
  Desc: Testing methods in SphereGeometry
==============================================================================*/

#include <iostream>
#include "RiemannianGeometryFactory.hxx"
#include "RandomGenerator.hxx"

typedef SphereGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;

typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;
typedef FactoryType::SharedGeometryPtr SharedGeometryPtr;

typedef RandomGenerator<PointType, TangentType> RandomType;

void runTests(unsigned int dim)
{
  FactoryType factory;
  SharedGeometryPtr geom;
  geom = factory.create("Sphere", dim);

  // for random generator
  DimensionList dims;
  dims.push_back(dim);
  GeometryType * sGeom = new GeometryType(dims);

  unsigned int exDim = geom->getExtrinsicDimension();

  PointType p1(exDim, 1), p2(exDim, 1);
  PointType p3(exDim, 1);
  TangentType t(exDim, 1), w(exDim, 1);

  // Jacobi inits
  TangentType j(exDim, 1), jPrime(exDim, 1);

  p1.fill(0.0);
  p1(0) = 1.0;
  p2 = p1;
  cout << "Equal points:" << endl;
  cout << "Log Map = " << geom->logMap(p1, p2) << endl;
  cout << "Dist = " << geom->distance(p1, p2) << endl;

  p2(0) = 0.0;
  p2(1) = 1.0;
  cout << "pi/2 points:" << endl;
  cout << "Log Map = " << geom->logMap(p1, p2) << endl;
  cout << "Dist = " << geom->distance(p1, p2) << endl;

  p2(0) = -1.0;
  p2(1) = 0.0;
  cout << "Poles:" << endl;
  cout << "Log Map = " << geom->logMap(p1, p2) << endl;
  cout << "Dist = " << geom->distance(p1, p2) << endl;

  t.fill(0.0);
  cout << "Exp map (0) = " << geom->expMap(p1, t) << endl;
  t(1) = M_PI * 0.5;
  cout << "Exp map (pi/2) = " << geom->expMap(p1, t) << endl;
  t(1) = M_PI;
  cout << "Exp map (pi) = " << geom->expMap(p1, t) << endl;
  t(1) = M_PI / 6.0;
  cout << "Exp map (pi/6) = " << geom->expMap(p1, t) << endl;

  // Parallel Translation
  p3.fill(0.0);
  w.fill(0.0);
  j.fill(0.0);
  jPrime.fill(0.0);

  p3(0) = 1.0;  // the point (1 0 0) for instance
  t(1) = 1.0;   // the tangent direction (0 1 0)
  w(2) = 1.0;   // parallel translate (0 1 0)

  cout << "pt(p3, t, w) = "
       <<  geom->parallelTranslate(p3, t, w) << endl;

  // Jacobi fields
  // TODO: this needs to test that inner product of adjoint is working

  // Test 1 : initial value problem
  j(0) = 1.0;   // the tangent (1 0 0)
  jPrime(1) = 1.0;  // the tangent (0 1 0)

  geom->adjointJacobiField(p3, t, j, jPrime);
  cout << "The Jacobi output tangent  = " << j << endl;

  // Test 2 : generate random points and gradient descend
  unsigned int counter = 0;

  PointType base = RandomType::randomGaussianPoint(geom, p1, 0.2);
  PointType target = RandomType::randomGaussianPoint(geom, p1, 0.2);
  PointType somePoint = RandomType::randomGaussianPoint(geom, p1, 0.2);

  cout << "Point 1 is = " << base << endl;
  cout << "Point 2 is = " << target << endl;
  cout << "Point 3 is = " << somePoint << endl;

  TangentType bt(exDim, 1), st(exDim, 1);
  TangentType sb(exDim, 1), bs(exDim, 1);

  // to verify the correctness of tangent direction from base to target

  // TODO: testing gradient descent is not necessary here (that is done in
  // geodesic regresion). More importantly we need to test that Jacobi fields
  // and their adjoints give appropriate derivatives of Exp (compare to finite
  // differences)

  bt = geom->logMap(base, target); // tangent from base to target
  bs = geom->logMap(base, somePoint); // tangent from base to somePoint
  st = geom->logMap(somePoint, target); // the discrepancy

  cout << " the actual tangent is = " << bt << endl;
  cout << " Our mistaken direction = " << bs << endl;
  cout << " The discrepancy = " << st << endl;

  double stNorm = geom->norm(somePoint, st);
  double epsilon = 0.05;

  cout << " norm(InitDiscrepancy) = " << stNorm << endl;
  while(stNorm > 1.0e-06 && counter < 1000)
  {
    j.fill(0.0);
    sb = geom->logMap(somePoint, base); // reverse geodesic
    geom->adjointJacobiField(somePoint, sb, j, st);
    TangentType grad = st;
    bs = bs + epsilon * grad;
    somePoint = geom->expMap(base, bs);

    st = geom->logMap(somePoint, target);
    stNorm = geom->norm(somePoint, st);
    cout << "norm(discrepancy) = " << stNorm << endl;
    counter++;
  }

  cout << endl << " Actual tangent = " << bt << endl;
  cout << " Estimated tangent = " << bs << endl;

  // Testing curvature
  TangentType spCurv = geom->curvatureTensor(p1, t, w, t);
  cout << "curv = " << spCurv << endl;
}

int main(int argc, char ** argv)
{
  RandomType::seed(10);
  cout << "*** Running 2D Tests ***" << endl;
  runTests(2);

  cout << "*** Running 3D Tests ***" << endl;
  runTests(3);

  cout << "*** Running 7D Tests ***" << endl;
  runTests(7);

  return EXIT_SUCCESS;
}
