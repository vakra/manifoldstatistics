/* ================================================================
File: SphereSasakiTest.cxx
Description: Testing for Sasaki geodesics in SphereGeometry
================================================================ */

#include <iostream>
#include "RiemannianGeometryFactory.hxx"

typedef SphereGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;
typedef std::shared_ptr<GeometryType> GeometryPtr;

typedef SasakiGeometry<VectorType, VectorType> TMGeometryType;
typedef TMGeometryType::TMPointType TMPointType;
typedef TMGeometryType::TMTangentType TMTangentType;

typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;

typedef std::shared_ptr<TMGeometryType> TMGeometryPtr;

int main(int argc, char ** argv)
{
  unsigned int dim = 2;

  FactoryType factory;
  GeometryPtr sphereGeom;
  sphereGeom = dynamic_pointer_cast<GeometryType>(factory.create("Sphere", dim));
  TMGeometryPtr geom;
  geom.reset(new TMGeometryType(sphereGeom));

  DimensionList dims;
  dims.push_back(dim);

  unsigned int exDim = geom->getExtrinsicDimension();

  TMPointType a, b;
  TMTangentType vTemp, v, wTemp, w, t, vTrans, wTrans;
  a.p.set_size(exDim);
  a.u.set_size(exDim);
  b.p.set_size(exDim);
  b.u.set_size(exDim);

  vTemp.x.set_size(exDim);
  vTemp.y.set_size(exDim);
  wTemp.x.set_size(exDim);
  wTemp.y.set_size(exDim);

  t.x.set_size(exDim);
  t.y.set_size(exDim);

  a.p.fill(0.0); a.p[0] = 1.0;
  a.u.fill(0.0); a.u[1] = M_PI / 4;
  b.p.fill(0.0); b.p[1] = 1.0;
  b.u.fill(0.0); b.u[2] = 1.0;

  vTemp.x.fill(0.0); vTemp.y.fill(0.0);
  wTemp.x.fill(0.0); wTemp.y.fill(0.0);
  t.x.fill(0.0); t.y.fill(0.0);
  vTemp.x[1] = 0.6;
  vTemp.y[0] = 0.8;
  wTemp.x[1] = 1.0;

  geom->projectTangent(v, a, vTemp);
  geom->projectTangent(w, a, wTemp);

  vTrans = v;
  wTrans = w;

  cout << "TS^2 Intrinsic dim = " << geom->getDimension() << endl;
  cout << "TS^2 Extrinsic dim = " << geom->getExtrinsicDimension() << endl;

  cout << "a = " << endl << a.p << a.u << endl;
  cout << "v = " << endl << v.x << v.y << endl;

  b = geom->expMap(a, v);
  cout << "b = Exp(a, v) = " << endl << b.p << b.u << endl;
  t = geom->logMap(a, b);
  cout << "t = Log(a, b) = " << endl << t.x << t.y << endl;

  vTrans = geom->parallelTranslate(a, v, v);
  wTrans = geom->parallelTranslate(a, v, w);

  cout << " ***** Testing inner products on TS^2 ***** " << endl;
  cout << "<a, v> = " << (sphereGeom->innerProduct(a.p, a.p, v.x) +
  			  sphereGeom->innerProduct(a.p, a.p, v.y))
       << endl;
  cout << "<b, vTrans> = " << (sphereGeom->innerProduct(b.p, b.p, vTrans.x) +
  			       sphereGeom->innerProduct(b.p, b.p, vTrans.y))
       << endl;

  double val = geom->innerProduct(a, v, w);
  cout << "<v, w> = " << val << endl;
  val = geom->innerProduct(b, vTrans, wTrans);
  cout << "<vTrans, wTrans> = " << val << endl;

  return EXIT_SUCCESS;
}
