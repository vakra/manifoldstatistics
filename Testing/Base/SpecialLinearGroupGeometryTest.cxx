#include "RiemannianGeometryFactory.hxx"

typedef SpecialLinearGroupGeometry GeometryType;
typedef GeometryType::PointType PointType;
typedef GeometryType::TangentType TangentType;

typedef RiemannianGeometryFactory<MatrixType, MatrixType> FactoryType;
typedef shared_ptr<GeometryType> GeometryPtr;

void myprint(string head, mat m)
{
  cout << head << endl;

  streamsize p = cout.precision(10);
  streamsize s = cout.width(16);
  cout.setf(ios::fixed);

  m.raw_print(cout);
  cout << endl;

  cout.width(s);
  cout.precision(p);
  cout.unsetf(ios::fixed);
}

int main(int argc, char ** argv)
{
  std::srand(50);
  cout.precision(10);
  unsigned int n = 2;

  FactoryType factory;
  GeometryPtr geom;
  geom = dynamic_pointer_cast<GeometryType>(factory.create("SpecialLinearGroup", n));

  // pointer to access the extra functions in the geometry
  GeometryType * slGeom = new GeometryType(n);
  PointType p = eye(n,n);

  // testing the factory pointer
  PointType zero = geom->getZeroVector();
  myprint("testing factory pointer - zero = ", zero);

  TangentType e1(n, n);
  TangentType e2(n, n);
  TangentType e3(n, n);
  TangentType v(n, n), w(n, n), z(n, n);

  ScalarType a = 0.5 * sqrt(2);

  // Orthonormal basis of Lie algebra
  e1(0,0) = a;
  e1(1,0) = 0.0;
  e1(0,1) = 0.0;
  e1(1,1) = -a;

  e2(0,0) = 0.0;
  e2(1,0) = a;
  e2(0,1) = a;
  e2(1,1) = 0.0;

  e3(0,0) = 0.0;
  e3(1,0) = a;
  e3(0,1) = -a;
  e3(1,1) = 0.0;

  myprint("p = ", p);
  myprint("e1 = ", e1);
  myprint("e2 = ", e2);
  myprint("e3 = ", e3);

  cout << "norm(e1) = " << geom->norm(p, e1) << endl;
  cout << "norm(e2) = " << geom->norm(p, e2) << endl;
  cout << "norm(e3) = " << geom->norm(p, e3) << endl << endl;

  cout << "<e1, e2> = " << slGeom->innerProductAtId(e1, e2) << endl;
  cout << "<e1, e3> = " << slGeom->innerProductAtId(e1, e3) << endl;
  cout << "<e2, e3> = " << slGeom->innerProductAtId(e2, e3) << endl << endl;

  myprint("expMapAtId(e1) = ", slGeom->expMapAtId(e1));
  myprint("expMapAtId(e2) = ", slGeom->expMapAtId(e2));
  myprint("expMapAtId(e3) = ", slGeom->expMapAtId(e3));

  // Testing tangent projection
  PointType p2(2, 2);
  p2(0,0) = 1.0;
  p2(0,1) = 0.5;
  p2(1,0) = 2.3;
  p2(1,1) = 0.4;

  myprint("Testing tangent projection: p2 = ", p2);
  geom->projectTangent(p2, p, p2);
  myprint("Projected p2 = ", p2);

  TangentType transV = slGeom->parallelTranslateTangentAtId(e1);
  myprint("Parallel translation of e1 along e1 = ", transV);
  transV = slGeom->parallelTranslateTangentAtId(e2);
  myprint("Parallel translation of e2 along e2 = ", transV);
  transV = slGeom->parallelTranslateTangentAtId(e3);
  myprint("Parallel translation of e3 along e3 = ", transV);

  transV = slGeom->parallelTranslateAtId(e1, e2);
  myprint("Parallel translation of e2 along e1 = ", transV);
  transV = slGeom->parallelTranslateAtId(e1, e3);
  myprint("Parallel translation of e3 along e1 = ", transV);
  transV = slGeom->parallelTranslateAtId(e2, e1);
  myprint("Parallel translation of e1 along e2 = ", transV);
  transV = slGeom->parallelTranslateAtId(e2, e3);
  myprint("Parallel translation of e3 along e2 = ", transV);
  transV = slGeom->parallelTranslateAtId(e3, e1);
  myprint("Parallel translation of e1 along e3 = ", transV);
  transV = slGeom->parallelTranslateAtId(e3, e2);
  myprint("Parallel translation of e2 along e3 = ", transV);

  ScalarListType x = randn<vec>(3);
  v = x[0] * e1 + x[1] * e2 + x[2] * e3;
  x = randn<vec>(3);
  w = x[0] * e1 + x[1] * e2 + x[2] * e3;
  x = randn<vec>(3);
  z = x[0] * e1 + x[1] * e2 + x[2] * e3;

  myprint("v =", v);
  myprint("w =", w);
  myprint("z =", z);

  TangentType transW = slGeom->parallelTranslateAtId(v, w);
  TangentType transZ = slGeom->parallelTranslateAtId(v, z);

  myprint("Parallel translation of w along v =", transW);
  myprint("Parallel translation of z along v =", transZ);

  cout << "<w, z> =           " << slGeom->innerProductAtId(w, z) << endl;
  cout << "<transW, transZ> = " << slGeom->innerProductAtId(transW, transZ)
       << endl << endl;

  transV = slGeom->parallelTranslateTangentAtId(v);
  transW = slGeom->parallelTranslateAtId(-transV, transW);
  transZ = slGeom->parallelTranslateAtId(-transV, transZ);
  myprint("Parallel translation of w back to id =", transW);
  myprint("Parallel translation of z back to id =", transZ);

  cout << "<w, z> =           " << slGeom->innerProductAtId(w, z) << endl;
  cout << "<transW, transZ> = " << slGeom->innerProductAtId(transW, transZ)
       << endl << endl;

  // Testing Jacobi fields
  TangentType jacW = w;
  TangentType djacW = zero;
  slGeom->jacobiFieldAtId(v, jacW, djacW);
  myprint("Jacobi field along v, with (j0, dj0) = (w,0):", jacW);

  TangentType adjjacZ = z;
  TangentType adjjacDZ = zero;
  slGeom->adjointJacobiFieldAtId(-transV, adjjacZ, adjjacDZ);
  myprint("Adjoint Jacobi field along -transV, with (j0, dj0) = (z, 0):",
          adjjacZ);

  cout << "Checking that Jacobi and adjoint Jacobi give same inner product"
       << endl;
  cout << "<jacobi(w), z> = " << slGeom->innerProductAtId(jacW, z)
       << endl;
  cout << "<adjjac(z), w> = " << slGeom->innerProductAtId(w, adjjacZ) << endl
       << endl;

  delete slGeom;

  return EXIT_SUCCESS;
}
