#include <iostream>
#include "RiemannianGeometryFactory.hxx"
#include "MeanAndPGA.hxx"

template<class T> T uniformRand(unsigned int dim) {}
template<> ScalarType uniformRand<ScalarType>(unsigned int dim)
{
  return (static_cast<ScalarType>(rand()) /
          (static_cast<ScalarType>(RAND_MAX) + 1.0));
}

template<> VectorType uniformRand<VectorType>(unsigned int dim)
{
  VectorType p;
  p.randu(dim);
  return p;
}

template<class DataType>
void runTests(unsigned int dimension)
{
  typedef DataType PointType;
  typedef DataType TangentType;
  typedef EuclideanGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;

  typedef RiemannianGeometryFactory<PointType, TangentType> FactoryType;
  typedef std::shared_ptr<RiemannianGeometry<PointType, TangentType> > GeometryPtr;

  typedef MeanAndPGA<PointType, TangentType> StatsType;

  DimensionList dims;
  dims.push_back(dimension);

  FactoryType factory;
  GeometryPtr geom;
  geom = (GeometryPtr)factory.create("Euclidean", dimension);

  StatsType * stats = new StatsType(geom);

  PointType p1, p2;
  TangentType v, w;

  // Initial tangent vectors for The jacobi equation
  TangentType j, jPrime;

  p1 = uniformRand<DataType>(dimension);
  p2 = p1;
  cout << "Equal points:" << endl;
  cout << "Log Map = " << geom->logMap(p1, p2) << endl;
  cout << "Dist = " << geom->distance(p1, p2) << endl;

  p2 = uniformRand<DataType>(dimension);
  cout << "Different points:" << endl;
  cout << "p1 = " << p1 << endl;
  cout << "p2 = " << p2 << endl;
  cout << "Log(p1, p2) = " << geom->logMap(p1, p2) << endl;
  cout << "Dist(p1, p2) = " << geom->distance(p1, p2) << endl;
  cout << "Exp(Log(p1, p2)) = "
       << geom->expMap(p1, geom->logMap(p1, p2)) << endl;

  v = geom->getZeroVector();
  cout << "Exp(p1, 0) = " << geom->expMap(p1, v) << endl;
  v = uniformRand<DataType>(dimension);
  cout << "v = " << v << endl;
  cout << "Exp(p1, v) = " << geom->expMap(p1, v) << endl;

  // Parallel Translation
  w = uniformRand<DataType>(dimension);
  cout << "Parallel translation:" << endl;
  cout << "v = " << v << endl;
  cout << "w = " << w << endl;
  cout << "Translation of w along v = "
       << geom->parallelTranslate(p1, v, w) << endl;

  // Jacobi fields
  // TODO: this needs to test that inner product of adjoint is correct
  j = uniformRand<DataType>(dimension);
  jPrime = uniformRand<DataType>(dimension);
  cout << "Jacobi field along v:" << endl;
  cout << "j(0) = " << j << endl;
  cout << "j'(0) = " << jPrime << endl;
  geom->jacobiField(p1, v, j, jPrime);
  cout << "j(1) = " << j << endl;
  geom->adjointJacobiField(p1, v, j, jPrime);
  cout << "adjoint j(0) = " << j << endl;

  // Compute mean
  PointType p3, p4, p5, p6;
  p3 = uniformRand<DataType>(dimension);
  p4 = uniformRand<DataType>(dimension);
  p5 = uniformRand<DataType>(dimension);
  p6 = uniformRand<DataType>(dimension);

  PointListType list;
  list.push_back(p1);
  list.push_back(p2);
  list.push_back(p3);
  list.push_back(p4);
  list.push_back(p5);
  list.push_back(p6);

  PointType mean = p1;
  stats->computeMean(list, mean);
  cout << "mean = " << mean << endl;

  PointType avg = (p1 + p2 + p3 + p4 + p5 + p6) / 6;
  cout << "avg = " << avg << endl;

  delete stats;
}

int main(int argc, char ** argv)
{
  srand(10);
  cout << "*** Running 1D Tests ***" << endl;
  runTests<ScalarType>(1);

  cout << "*** Running 2D Tests ***" << endl;
  runTests<VectorType>(2);

  cout << endl << "*** Running 3D Tests ***" << endl;
  runTests<VectorType>(3);

  cout << endl << "*** Running 7D Tests ***" << endl;
  runTests<VectorType>(7);

  return EXIT_SUCCESS;
}
