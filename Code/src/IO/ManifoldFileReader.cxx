/*==============================================================================
  File: ManifoldFileReader.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include "ManifoldFileReader.hxx"

#include <limits>

#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

// Implements binary & textual load functionality for Riemannian Geometries.
void ManifoldFileReader::load(const std::string & _fileName)
{
  string buffer;
  DimensionList dimParameters;
  VakraLog log(string("load.log"));

  fileName = _fileName;
  container.reset(new MFDContainer());

  inputFile.open(fileName.c_str());

  if(inputFile.fail())
  {
    cerr << "Error: Could not open file for loading: " << fileName << endl;
    container.reset();
    return;
  }

  // First line should be magic number "GFBR", followed by version number
  getline(inputFile, buffer);
  if(buffer.length() < 4 || buffer.substr(0, 4).compare("GFBR") != 0)
  {
    cerr << "Error: Unrecognized file magic number" << endl;
    container.reset();
    return;
  }
  // Check for version number (currently only 1)
  if(buffer.length() < 5 || buffer[4] != '1')
  {
    cerr << "Error: Unrecognized file version number" << endl;
    container.reset();
    return;
  }

  filePos = inputFile.tellg();

  // Read header.
  while(inputFile.good() &&
        (filePos = inputFile.tellg()) > -1 &&
        getline(inputFile, buffer))
  {
    boost::char_separator<char> separator("= ");
    inTokenizer tokens(buffer, separator);
    inIterator it = tokens.begin();

    if(it->compare("manifold") == 0)
    {
      ++it;
      container->setManifoldType(*it);
      log.log("Reading manifold type: " + *it);
    }
    else if(it->compare("primitive") == 0)
    {
      ++it;
      log.log("Reading primitive type: " + *it);
      if(it->compare("point") == 0)
        container->setPrimitiveType(POINT);
      else
      {
        container->setPrimitiveType(UNKNOWN_PRIMITIVE);
        log.log("Unknown primitive type.");
      }
    }
    else if(it->compare("dimParameters") == 0)
    {
      // Retrieve list of dimension parameters.
      for(++it; it != tokens.end(); ++it)
      {
        dimParameters.push_back(boost::lexical_cast<int>(*it));
        log.log("Read dimension parameter: " + *it);
      }
      container->setDimParameters(dimParameters);
    }
    else if(it->compare("format") == 0)
    {
      ++it;
      // Set format for read.
      if(it->compare("binary") == 0)
        container->setIsBinary(true);
      else
        container->setIsBinary(false);
      log.log("Format for data: " + *it);
    }
    else if(it->compare("numElements") == 0)
    {
      ++it;
      unsigned int numElements = boost::lexical_cast<unsigned int>(*it);
      container->setNumElements(numElements);
      log.log("Number of elements in dataset: " +
              boost::lexical_cast<string>(numElements));
    }
    else if(it->compare("data:") == 0)
    {
      log.log("Reached data block.");
      break;
    }
    else
    {
      cerr << "Error: Unrecognised token in header: " << *it << endl;
      container.reset();
      return;
    }
  } // End while: done reading header.

  filePos = inputFile.tellg();
  inputFile.close();
}
