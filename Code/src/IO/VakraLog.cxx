/*==============================================================================
  File: VakraLog.cxx

  Implements a general-purpose logging facility for use in the manifold
  statitics research project.  Tries to provide reasonable default behaviour
  to minimise code footprint in client modules.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#include "VakraLog.hxx"

const char * VakraLog::DEFAULT_TITLE = "Manifold Library";
const char * VakraLog::DEFAULT_FILE = "default_log.txt";

VakraLog::VakraLog(bool autoflush) :
  mBufferIsEmpty(true),
  mFileNeedsClosing(false),
  mTitle(DEFAULT_TITLE),
  mDestination(clog),
  mAutoflush(autoflush)
{
  if (!mDestination.good())
  {
    cerr << "LOGGING ERROR (1): UNABLE TO OPEN STREAM FOR WRITING.";
  }
}

VakraLog::VakraLog(ostream & destination, const string title, bool autoflush) :
  mBufferIsEmpty(true),
  mFileNeedsClosing(false),
  mTitle(title),
  mDestination(destination),
  mAutoflush(autoflush)
{
  if (!mDestination.good())
  {
    cerr << "LOGGING ERROR (2): UNABLE TO OPEN STREAM FOR WRITING.";
  }
}

VakraLog::VakraLog(const string destinationName, const string title, bool autoflush) :
  mBufferIsEmpty(true),
  mFileNeedsClosing(true),
  mTitle(title),
  mDestination(mFile),
  mAutoflush(autoflush)
{
  mFile.open(destinationName.size() > 0 ? destinationName.c_str() : DEFAULT_FILE,
             ios::app | ios::ate);
  if (!mFile.is_open() || !mDestination.good())
  {
    cerr << "LOGGING ERROR (3): UNABLE TO OPEN LOG FILE " << destinationName << " FOR WRITING.";
  }
}

VakraLog::~VakraLog(void)
{
  mDestination.flush();
  if (mFileNeedsClosing)
  {
    mFile.close();
  }
}

void VakraLog::log(char * message)
{
  if (message != NULL)
  { log(string(message)); }
}

void VakraLog::log(const char * message)
{
  log((char *)message);
}

void VakraLog::log(string message)
{
  mDestination << "[" << mTitle << "] Note: " << message << "\n";
  if (mAutoflush) { mDestination.flush(); }
  mBufferIsEmpty = false;
}

void VakraLog::warn(char * message)
{
  if (message != NULL)
  { warn(string(message)); }
}

void VakraLog::warn(const char * message)
{
  warn((char *)message);
}

void VakraLog::warn(string message)
{
  mDestination << "[" << mTitle << "] Warning: " << message << "\n";
  if (mAutoflush) { mDestination.flush(); }
  mBufferIsEmpty = false;
}

void VakraLog::err(char * message)
{
  if (message != NULL)
  { err(string(message)); }
}

void VakraLog::err(const char * message)
{
  err((char *)message);
}

void VakraLog::err(string message)
{
  mDestination << "[" << mTitle << "] ERROR: " << message << "\n";
  if (mAutoflush) { mDestination.flush(); }
  mBufferIsEmpty = true;
}

void VakraLog::flush(void)
{
  mDestination.flush();
  mBufferIsEmpty = true;
}

bool VakraLog::bufferIsEmpty(void)
{
  return mBufferIsEmpty;
}

