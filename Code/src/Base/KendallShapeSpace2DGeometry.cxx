/*==============================================================================
  File: KendallShapeSpace2DGeometry.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include "KendallShapeSpace2DGeometry.hxx"

ScalarType KendallShapeSpace2DGeometry::
innerProduct(const PointType & p,
             const TangentType & v,
             const TangentType & w)
{
  ScalarType result = 0.0;
  unsigned int i, j;

  unsigned int num = dimParameters[1];

  for(j = 0; j < num; j++)
    for(i = 0; i < 2; i++)
      result += v(i, j) * w(i, j);
  return result;
}

KendallShapeSpace2DGeometry::
PointType KendallShapeSpace2DGeometry::
expMap(const PointType & p, const TangentType & t)
{
  ScalarType theta = norm(p, t);

  if(theta < 1.0e-12)
    return p;

  PointType q = cos(theta) * p + (sin(theta) / theta) * t;
  return (PointType)(q / norm(q, q));
}

KendallShapeSpace2DGeometry::
TangentType KendallShapeSpace2DGeometry::
logMap(const PointType & p, const PointType & q)
{
  const MatrixType m = p * strans(q);

  MatrixType U, V; VectorType s;
  svd_econ(U, s, V, m, 'b');

  MatrixType rotation = U * strans(V);
  PointType qRot = rotation * q;

  ScalarType cosTheta = innerProduct(p, p, qRot);
  TangentType t = qRot - p * cosTheta;
  ScalarType length = norm(p, t);

  if(length < 1.0e-12 || cosTheta >= 1.0 || cosTheta <= -1.0)
    return SuperClass::zeroVector;

  return (TangentType)(t * (acos(cosTheta) / length));
}

KendallShapeSpace2DGeometry::
TangentType KendallShapeSpace2DGeometry::
parallelTranslate(const PointType & p, const TangentType & v,
                  const TangentType & w)
{
  TangentType parallelW;
  ScalarType vNorm = norm(p, v);
  ScalarType pNorm = norm(p, p);

  if(vNorm < 1.0e-12 || pNorm < 1.0e-12)
    return w;

  MatrixType skew = arma::zeros<MatrixType>(2, 2);
  skew(0, 1) = -1;
  skew(1, 0) = 1;

  TangentType unitV = v / vNorm;
  TangentType unitJV = skew * unitV ;
  TangentType unitJP = skew * (p / pNorm);

  // If v and w are horizontal, the real inner product will work
  ScalarType wDotUnitV = innerProduct(p, unitV, w);
  ScalarType wDotUnitJV = innerProduct(p, unitJV, w);

  // Component of w orthogonal to v and jv
  TangentType orth = w - unitV * wDotUnitV - unitJV * wDotUnitJV;

  // Compute parallel translated v
  TangentType parallelUnitV = p * (-sin(vNorm) / pNorm) + unitV * cos(vNorm);

  // Compute parallel translated jv
  TangentType parallelUnitJV = unitJV * cos(vNorm) - unitJP * sin(vNorm);

  // Add parallel translated v to orth, and get parallel translated w
  parallelW = parallelUnitV * wDotUnitV + parallelUnitJV * wDotUnitJV + orth;

  return parallelW;
}

KendallShapeSpace2DGeometry::
TangentType KendallShapeSpace2DGeometry::
parallelTranslateAtoB(const PointType & p,
                      const PointType & q,
                      const TangentType & w)
{
  TangentType v = logMap(p, q);
  return parallelTranslate(p, v, w);
}

void KendallShapeSpace2DGeometry::
jacobiField(const PointType & p, const TangentType & v,
            TangentType & j, TangentType & dj)
{
  TangentType jTang, djTang, jOrth, djOrth, ddjOrth;
  TangentType jOrth1, jOrth4, djOrth1, djOrth4;

  ScalarType vNorm = norm(p, v);
  if(vNorm < 1.0e-12)
  {
    j = j + dj;
    // dj = dj;
    return;
  }

  ScalarType innerProdVJ = innerProduct(p, v, j);
  ScalarType innerProdVJPrime = innerProduct(p, v, dj);

  ScalarType scaleFactorJ = innerProdVJ / (vNorm * vNorm);
  ScalarType scaleFactorJPrime = innerProdVJPrime / (vNorm * vNorm);

  jTang = v * scaleFactorJ;
  djTang = v * scaleFactorJPrime;

  jOrth = j - jTang;
  djOrth = dj - djTang;

  MatrixType skew = arma::zeros<MatrixType>(2, 2);
  skew(0, 1) = -1;
  skew(1, 0) = 1;

  TangentType w = skew * (v / vNorm);

  // Curvature 4 component
  jOrth4 = w * innerProduct(p, w, jOrth);
  djOrth4 = w * innerProduct(p, w, djOrth);

  // Curvature 1 component
  jOrth1 = jOrth - jOrth4;
  djOrth1 = djOrth - djOrth4;

  jOrth = jOrth1 * cos(vNorm) + jOrth4 * cos(2.0 * vNorm);
  djOrth = djOrth1 * (sin(vNorm) / vNorm) +
    djOrth4 * (0.5 * sin(2.0 * vNorm) / vNorm);

  j = parallelTranslate(p, v, jTang + djTang + jOrth + djOrth);

  djOrth = jOrth1 * (-vNorm * sin(vNorm)) +
    jOrth4 * (-2.0 * vNorm * sin(2.0 * vNorm));
  ddjOrth = djOrth1 * cos(vNorm) + djOrth4 * cos(2.0 * vNorm);

  dj = parallelTranslate(p, v, djTang + djOrth + ddjOrth);
}

void KendallShapeSpace2DGeometry::
adjointJacobiField(const PointType & p, const TangentType & v,
                   TangentType & j, TangentType & dj)
{
  TangentType jTang, djTang, jOrth, djOrth, ddjOrth;
  TangentType jOrth1, jOrth4, djOrth1, djOrth4;

  ScalarType vNorm = norm(p, v);
  if(vNorm < 1.0e-12)
  {
    // j = j;
    dj = j + dj;
    return;
  }

  ScalarType innerProdVJ = innerProduct(p, v, j);
  ScalarType innerProdVJPrime = innerProduct(p, v, dj);

  ScalarType scaleFactorJ = innerProdVJ / (vNorm * vNorm);
  ScalarType scaleFactorJPrime = innerProdVJPrime / (vNorm * vNorm);

  jTang = v * scaleFactorJ;
  djTang = v * scaleFactorJPrime;

  jOrth = j - jTang;
  djOrth = dj - djTang;

  MatrixType skew = arma::zeros<MatrixType>(2, 2);
  skew(0, 1) = -1;
  skew(1, 0) = 1;

  TangentType w = skew * (v / vNorm);

  // Curvature 4 component
  jOrth4 = w * innerProduct(p, w, jOrth);
  djOrth4 = w * innerProduct(p, w, djOrth);

  // Curvature 1 component
  jOrth1 = jOrth - jOrth4;
  djOrth1 = djOrth - djOrth4;

  jOrth = jOrth1 * cos(vNorm) + jOrth4 * cos(2.0 * vNorm);
  djOrth = djOrth1 * (-vNorm * sin(vNorm)) +
    djOrth4 * (-2.0 * vNorm * sin(2.0 * vNorm));

  j =  parallelTranslate(p, v, jOrth + djOrth + jTang);

  djOrth = jOrth1 * (sin(vNorm) / vNorm) +
    jOrth4 * (0.5 * sin(2.0 * vNorm) / vNorm);
  ddjOrth = djOrth1 * cos(vNorm) + djOrth4 * cos(2.0 * vNorm);

  dj = parallelTranslate(p, v, jTang + djTang + djOrth + ddjOrth);
}

KendallShapeSpace2DGeometry::
TangentType KendallShapeSpace2DGeometry::
curvatureTensor(const PointType & p,  const TangentType & x,
                const TangentType & y, const TangentType & z)
{
  unsigned int num = dimParameters[1];

  TangentType Jx(2, num), Jy(2, num), Jz(2, num);

  MatrixType skew = arma::zeros<MatrixType>(2, 2);
  skew(0, 1) = -1;
  skew(1, 0) = 1;

  Jx = skew * x;
  Jy = skew * y;
  Jz = skew * z;

  TangentType sphereCurv(2, num), kCurv(2, num);
  TangentType zxy(2, num), xyz(2, num), yzx(2, num);

  zxy = Jy * innerProduct(p, z, Jx);
  xyz = Jz * innerProduct(p, x, Jy);
  yzx = Jx * innerProduct(p, y, Jz);

  kCurv = zxy + yzx - (xyz * 2.0);

  ScalarType zDotX = innerProduct(p, z, x);
  ScalarType zDotY = innerProduct(p, z, y);

  sphereCurv = y * zDotX - x * zDotY;

  return sphereCurv + kCurv;
}

ScalarType KendallShapeSpace2DGeometry::
sectionalCurvature(const PointType & p, const TangentType & x,
                   const TangentType & y)
{
  unsigned int num = dimParameters[1];

  TangentType curv = arma::zeros<TangentType>(2, num);
  curv = curvatureTensor(p, x, y, x);
  double sec = innerProduct(p, curv, y);
  double xx = normSquared(p, x);
  double yy = normSquared(p, y);
  double xy = innerProduct(p, x, y);

  return (sec / ((xx * yy) - pow(xy, 2)));
}

void KendallShapeSpace2DGeometry::
projectTangent(TangentType & hv, const PointType & p, const TangentType & v)
{
  unsigned int i;
  unsigned int num = dimParameters[1];
  VectorType meanOfV = arma::zeros<VectorType>(2);
  meanOfV = arma::mean(v, 1);

  hv = v;
  // Remove translation component
  for(i = 0; i < num; i++)
    hv.col(i) = hv.col(i) - meanOfV;

  // Project onto tangent space of sphere
  hv = hv - p * innerProduct(p, p, hv);
  // Horizontal projection
  MatrixType skew = arma::zeros<MatrixType>(2, 2);
  skew(0, 1) = -1;
  skew(1, 0) = 1;

  TangentType vert = skew * p;
  hv = hv - vert * innerProduct(p, hv, vert);
}

void KendallShapeSpace2DGeometry::
tangentToList(const TangentType & v, ScalarListType & list)
{
  unsigned int j;
  unsigned int numPoints = dimParameters[1];
  list.set_size(2 * numPoints);

  for(j = 0; j < numPoints; j++)
  {
    list(j * 2) = v(0, j);
    list(j * 2 + 1) = v(1, j);
  }
}

void KendallShapeSpace2DGeometry::
listToTangent(const ScalarListType & list, TangentType & v)
{
  unsigned int j;
  unsigned int numPoints = dimParameters[1];

  v.set_size(2, numPoints);
  for(j = 0; j < numPoints; j++)
  {
    v(0, j) = list(j * 2);
    v(1, j) = list(j * 2 + 1);
  }
}

void KendallShapeSpace2DGeometry::
pointToList(const PointType & p, ScalarListType & list)
{
  unsigned int j;
  unsigned int numPoints = dimParameters[1];
  list.set_size(2 * numPoints);

  for(j = 0; j < numPoints; j++)
  {
    list(j * 2) = p(0, j);
    list(j * 2 + 1) = p(1, j);
  }
}

void KendallShapeSpace2DGeometry::
listToPoint(const ScalarListType & list, PointType & p)
{
  unsigned int j;
  unsigned int numPoints = dimParameters[1];

  p.set_size(2, numPoints);
  for(j = 0; j < numPoints; j++)
  {
    p(0, j) = list(j * 2);
    p(1, j) = list(j * 2 + 1);
  }
}

KendallShapeSpace2DGeometry::
PointType KendallShapeSpace2DGeometry::
preshape(const PointType & p)
{
  unsigned int i;
  unsigned int numPoints = dimParameters[1];

  // Removing translation and scale
  PointType translateP = p;
  VectorType meanP = mean(p, 1);

  for(i = 0; i < numPoints; i++)
    translateP.col(i) = p.col(i) - meanP;

  ScalarType scale = norm(translateP, translateP);

  if(scale < 1.0e-12)
    return SuperClass::zeroVector;

  return (translateP / scale);
}
