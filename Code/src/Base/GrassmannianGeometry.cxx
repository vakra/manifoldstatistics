/*==============================================================================
  File: GrassmannianGeometry.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include "GrassmannianGeometry.hxx"

ScalarType GrassmannianGeometry::
innerProduct(const PointType & base, const TangentType & v,
             const TangentType & w)
{
  ScalarType result = 0.0;

  result = trace(strans(v) * w);
  return result;
}

GrassmannianGeometry::PointType
GrassmannianGeometry::
expMap(const PointType & base, const TangentType & tangent)
{
  ScalarType t = this->norm(base, tangent);

  if(t < 1.0e-12)
    return base;

  // Compact SVD
  MatrixType U, V; VectorType s;
  svd_econ(U, s, V, tangent, 'b');

  PointType target;
  target = (base * V * diagmat(cos(s)) + U * diagmat(sin(s))) * strans(V);

  return target;
}

GrassmannianGeometry::TangentType
GrassmannianGeometry::
logMap(const PointType & base, const PointType & target)
{
  unsigned int dim = dimParameters[0];
  MatrixType I; I.eye(dim, dim);
  MatrixType temp1 = I - base * strans(base);
  MatrixType temp2 = inv(strans(base) * target);
  MatrixType X = temp1 * target * temp2;

  MatrixType U, V; VectorType s;
  svd_econ(U, s, V, X, 'b');

  VectorType theta = atan(s);

  TangentType v = U * diagmat(theta) * strans(V);
  return v;
}

GrassmannianGeometry::TangentType
GrassmannianGeometry::
parallelTranslate(const PointType & base, const TangentType & v,
                  const TangentType & w)
{
  ScalarType vNorm = this->norm(base, v);
  unsigned int dim = dimParameters[0];
  if(vNorm < 1.0e-12)
    return w;

  // Compact SVD
  MatrixType I, U, V; VectorType s;
  I.eye(dim, dim);
  svd_econ(U, s, V, v, 'b');
  TangentType wPara;
  wPara = ((base * V * diagmat(-sin(s)) + U * diagmat(cos(s))) *
           strans(U) + (I - U * strans(U))) * w;
  return wPara;
}

GrassmannianGeometry::TangentType
GrassmannianGeometry::
parallelTranslateAtoB(const PointType & a, const PointType & b,
                      const TangentType & w)
{
  TangentType v = logMap(a, b);
  return parallelTranslate(a, v, w);
}

GrassmannianGeometry::PointType
GrassmannianGeometry::
preshape(PointType & base)
{
  unsigned int i, j;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  PointType temp = base, target = base;
  VectorType sum = arma::zeros<VectorType>(dim), a(dim), b(dim);
  double aNorm;
  temp.col(0) = base.col(0);
  target.col(0) = temp.col(0) / arma::norm(temp.col(0), "fro");

  for(i = 1; i < num; i++)
  {
    b = base.col(i);
    for(j = 0; j < i; j++)
    {
      a = temp.col(j);
      aNorm = arma::norm(a, "fro");
      sum = sum + a * ((strans(b) * a) / (aNorm * aNorm));
    }
    temp.col(i) = base.col(i) - sum;
    target.col(i) = temp.col(i) / arma::norm(temp.col(i), "fro");
    sum = arma::zeros<VectorType>(dim); // re-initializing
  }

  return target;
}

// Not implemented
ScalarType GrassmannianGeometry::
sectionalCurvature(const PointType & base, const TangentType & v,
                   const TangentType & w)
{
  return 1;
}

// Not implemented
void GrassmannianGeometry::
jacobiField(const PointType & base, const TangentType & v,
            TangentType & j, TangentType & jPrime)
{
  return;
}

// Not implemented
void GrassmannianGeometry::
adjointJacobiField(const PointType & base, const TangentType & v,
                   TangentType & j, TangentType & jPrime)
{
  return;
}

// Not implemented
GrassmannianGeometry::TangentType
GrassmannianGeometry::
curvatureTensor(const PointType & base,
		const TangentType & u,
		const TangentType & v,
		const TangentType & w)
{
  return SuperClass::zeroVector;
}

void GrassmannianGeometry::
projectTangent(TangentType & projectedTangent,
               const PointType & base,
               const TangentType & tangent)
{
  unsigned int dim = dimParameters[0];
  MatrixType I; I.eye(dim, dim);
  MatrixType temp = I - base * strans(base);
  projectedTangent = temp * tangent;
}

void GrassmannianGeometry::
tangentToList(const TangentType & v, ScalarListType & list)
{
  unsigned int i, j;
  unsigned int count = 0;

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  list.set_size(dim * num);

  for(j = 0; j < num; j++)
    {
      for(i = 0; i < dim; i++)
	{
	  list[count] = v(i, j);
	  count += 1;
	}
    }

  return;
}

void GrassmannianGeometry::
listToTangent(const ScalarListType & list, TangentType & v)
{
  unsigned int i, j;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  for(j = 0; j < num; j++)
    for(i = 0; i < dim; i++)
      v(i, j) = list[j * dim + i];
  return;
}

void GrassmannianGeometry::
pointToList(const PointType & p, ScalarListType & list)
{
  unsigned int i, j;
  unsigned int count = 0;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  list.set_size(dim * num);

  for(j = 0; j < num; j++)
    {
      for(i = 0; i < dim; i++)
	{
	  list[count] = p(i, j);
	  count += 1;
	}
    }

  return;
}

void GrassmannianGeometry::
listToPoint(const ScalarListType & list, PointType & p)
{
  unsigned int i, j;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  for(j = 0; j < num; j++)
    for(i = 0; i < dim; i++)
      p(i, j) = list[j * dim + i];
  return;
}
