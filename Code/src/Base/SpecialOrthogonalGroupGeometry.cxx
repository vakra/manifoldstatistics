/*==============================================================================
  File: SpecialOrthogonalGroupGeometry.cxx

  This class implements the special orthogonal group, SO(n), i.e., the space of
  nxn rotation matrices. The Lie algebra is the space of skew-symmetric
  matrices.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#include "SpecialOrthogonalGroupGeometry.hxx"

void SpecialOrthogonalGroupGeometry::
jacobiFieldAtId(const TangentType & v, TangentType & j, TangentType & dj)
{
  unsigned int i, k;
  unsigned int n = dimParameters[0];
  ScalarType vNormSquared = normSquaredAtId(v);
  if(vNormSquared < 1e-8)
    return;

  ScalarType vNorm = sqrt(vNormSquared);

  // First decompose tangential and orthogonal components
  TangentType jTang, djTang;
  TangentType jOrth, djOrth;

  jTang = (innerProductAtId(v, j) / vNormSquared) * v;
  djTang = (innerProductAtId(v, dj) / vNormSquared) * v;

  jOrth = j - jTang;
  djOrth = dj - djTang;

  // Next, find the components with varying positive and 0 sectional curvatures
  // Varying sectional curvatures only occurs if n > 3
  if(n > 3)
  {
    // Eigendecomposition trick to give 2x2 block diagonal skew-symmetric matrix
    vec eVals;
    mat U(n, n), Utemp;
    mat symV = v * trans(v);

    eig_sym(eVals, Utemp, v * trans(v));

    // Reorder so that eigen values are high to low
    for(i = 0; i < n; i++)
    {
      for(k = 0; k < n; k++)
      {
        U(i, k) = Utemp(i, n - 1 - k);
      }
    }

    TangentType vBD = trans(U) * v * U;

    // For sectional curvature computations, make this unit length
    vBD = vBD / normAtId(vBD);

    // w, dw will be the j, dj with same similarity transform applied
    TangentType w = trans(U) * jOrth * U;
    TangentType dw = trans(U) * djOrth * U;

    ScalarType a, b, c, t;

    // First fill these in in similarity transformed space
    j.zeros();
    dj.zeros();

    // Traverse 2x2 blocks on diagonal
    for(k = 0; k < n - 1; k += 2)
    {
      j(k, k + 1) = w(k, k + 1) + dw(k, k + 1);
      j(k + 1, k) = -j(k, k + 1);

      dj(k, k + 1) = dw(k, k + 1);
      dj(k + 1, k) = -dj(k, k + 1);
    }

    // Traverse 2x2 blocks in upper-triangular half
    for(k = 2; k < n - 1; k += 2)
    {
      for(i = 0; i < k; i += 2)
      {
        // Four orthonormal basis vectors for the 2x2 block
        // Case 1: |1 0|
        //         |0 1|
        // Sqrt of sectional curvature times geodesic length
        t = 0.5 * fabs(vBD(i, i + 1) - vBD(k, k + 1)) * vNorm;

        a = 0.5 * (w(i, k) + w(i + 1, k + 1));
        b = 0.5 * (dw(i, k) + dw(i + 1, k + 1));

        j(i, k) = scalarJacobiFunction(t, a, b);
        j(i + 1, k + 1) = j(i, k);

        dj(i, k) = dScalarJacobiFunction(t, a, b);
        dj(i + 1, k + 1) = dj(i, k);

        // Case 2: |0  1|
        //         |-1 0|
        // same sectional curvature as Case 1
        a = 0.5 * (w(i, k + 1) - w(i + 1, k));
        b = 0.5 * (dw(i, k + 1) - dw(i + 1, k));

        j(i, k + 1) = scalarJacobiFunction(t, a, b);
        j(i + 1, k) = -j(i, k + 1);

        dj(i, k + 1) = dScalarJacobiFunction(t, a, b);
        dj(i + 1, k) = -dj(i, k + 1);

        // Case 3: |1  0|
        //         |0 -1|
        // Sqrt of sectional curvature times geodesic length
        t = 0.5 * fabs(vBD(i, i + 1) + vBD(k, k + 1)) * vNorm;

        a = 0.5 * (w(i, k) - w(i + 1, k + 1));
        b = 0.5 * (dw(i, k) - dw(i + 1, k + 1));

        c = scalarJacobiFunction(t, a, b);
        j(i, k) = j(i, k) + c;
        j(i + 1, k + 1) = j(i + 1, k + 1) - c;
        j(k, i) = -j(i, k);
        j(k + 1, i + 1) = -j(i + 1, k + 1);

        c = dScalarJacobiFunction(t, a, b);
        dj(i, k) = dj(i, k) + c;
        dj(i + 1, k + 1) = dj(i + 1, k + 1) - c;
        dj(k, i) = -dj(i, k);
        dj(k + 1, i + 1) = -dj(i + 1, k + 1);

        // Case 4: |0 1|
        //         |1 0|
        // same sectional curvature as Case 3
        a = 0.5 * (w(i, k + 1) + w(i + 1, k));
        b = 0.5 * (dw(i, k + 1) + dw(i + 1, k));

        c = scalarJacobiFunction(t, a, b);
        j(i, k + 1) = j(i, k + 1) + c;
        j(i + 1, k) = j(i + 1, k) + c;
        j(k + 1, i) = -j(i, k + 1);
        j(k, i + 1) = -j(i + 1, k);

        c = dScalarJacobiFunction(t, a, b);
        dj(i, k + 1) = dj(i, k + 1) + c;
        dj(i + 1, k) = dj(i + 1, k) + c;
        dj(k + 1, i) = -dj(i, k + 1);
        dj(k, i + 1) = -dj(i + 1, k);
      }
    }

    // If n is odd, we have to traverse the last column
    if(n % 2 == 1)
    {
      for(i = 0; i < n - 1; i++)
      {
        // Sqrt sectional curvature times length of geodesic
        if(i % 2 == 0)
          t = 0.5 * fabs(vBD(i, i + 1)) * vNorm;
        else
          t = 0.5 * fabs(vBD(i, i - 1)) * vNorm;

        a = w(i, n - 1);
        b = dw(i, n - 1);

        j(i, n - 1) = scalarJacobiFunction(t, a, b);
        j(n - 1, i) = -j(i, n - 1);
        dj(i, n - 1) = dScalarJacobiFunction(t, a, b);
        dj(n - 1, i) = -dj(i, n - 1);
      }
    }

    j = jTang + djTang + U * j * trans(U);
    dj = djTang + U * dj * trans(U);
  }
  // SO(3) is special case with constant sectional curvature = 0.25
  // This works for SO(2) also because there is no orthogonal component
  else
  {
    j = jTang + djTang + cos(0.5 * vNorm) * jOrth +
      (2.0 / vNorm) * sin(0.5 * vNorm) * djOrth;
    dj = djTang - (0.5 * vNorm) * sin(0.5 * vNorm) * jOrth +
      cos(0.5 * vNorm) * djOrth;
  }

  j = parallelTranslateAtId(v, j);
  dj = parallelTranslateAtId(v, dj);
}

void SpecialOrthogonalGroupGeometry::
adjointJacobiFieldAtId(const TangentType & v, TangentType & j, TangentType & dj)
{
  unsigned int i, k;
  unsigned int n = dimParameters[0];
  ScalarType vNormSquared = normSquaredAtId(v);
  if(vNormSquared < 1e-8)
    return;

  ScalarType vNorm = sqrt(vNormSquared);

  // First decompose tangential and orthogonal components
  TangentType jTang, djTang;
  TangentType jOrth, djOrth;

  jTang = (innerProductAtId(v, j) / vNormSquared) * v;
  djTang = (innerProductAtId(v, dj) / vNormSquared) * v;

  jOrth = j - jTang;
  djOrth = dj - djTang;

  // Next, find the components with varying positive and 0 sectional curvatures
  // Varying sectional curvatures only occurs if n > 3
  if(n > 3)
  {
    // Eigendecomposition trick to give 2x2 block diagonal skew-symmetric matrix
    vec eVals;
    mat U(n, n), Utemp;

    mat symV = v * trans(v);
    eig_sym(eVals, Utemp, symV);

    // Reorder so that eigen values are high to low
    for(i = 0; i < n; i++)
    {
      for(k = 0; k < n; k++)
      {
        U(i, k) = Utemp(i, n - 1 - k);
      }
    }

    TangentType vBD = trans(U) * v * U;

    // For sectional curvature computations, make this unit length
    vBD = vBD / normAtId(vBD);

    // w, dw will be the j, dj with same similarity transform applied
    TangentType w = trans(U) * jOrth * U;
    TangentType dw = trans(U) * djOrth * U;

    ScalarType a, b, c, t;

    // First fill these in in similarity transformed space
    j.zeros();
    dj.zeros();

    // Traverse 2x2 blocks on diagonal
    for(k = 0; k < n - 1; k += 2)
    {
      j(k, k + 1) = w(k, k + 1);
      j(k + 1, k) = -j(k, k + 1);

      dj(k, k + 1) = w(k, k + 1) + dw(k, k + 1);
      dj(k + 1, k) = -dj(k, k + 1);
    }

    // Traverse 2x2 blocks in upper-triangular half
    for(k = 2; k < n - 1; k += 2)
    {
      for(i = 0; i < k; i += 2)
      {
        // Four orthonormal basis vectors for the 2x2 block
        // Case 1: |1 0|
        //         |0 1|
        // Sqrt of sectional curvature times geodesic length
        t = 0.5 * fabs(vBD(i, i + 1) - vBD(k, k + 1)) * vNorm;

        a = 0.5 * (w(i, k) + w(i + 1, k + 1));
        b = 0.5 * (dw(i, k) + dw(i + 1, k + 1));

        j(i, k) = scalarAdjointJacobiFunction(t, a, b);
        j(i + 1, k + 1) = j(i, k);

        dj(i, k) = dScalarAdjointJacobiFunction(t, a, b);
        dj(i + 1, k + 1) = dj(i, k);

        // Case 2: |0  1|
        //         |-1 0|
        // same sectional curvature as Case 1
        a = 0.5 * (w(i, k + 1) - w(i + 1, k));
        b = 0.5 * (dw(i, k + 1) - dw(i + 1, k));

        j(i, k + 1) = scalarAdjointJacobiFunction(t, a, b);
        j(i + 1, k) = -j(i, k + 1);

        dj(i, k + 1) = dScalarAdjointJacobiFunction(t, a, b);
        dj(i + 1, k) = -dj(i, k + 1);

        // Case 3: |1  0|
        //         |0 -1|
        // Sqrt of sectional curvature times geodesic length
        t = 0.5 * fabs(vBD(i, i + 1) + vBD(k, k + 1)) * vNorm;

        a = 0.5 * (w(i, k) - w(i + 1, k + 1));
        b = 0.5 * (dw(i, k) - dw(i + 1, k + 1));

        c = scalarAdjointJacobiFunction(t, a, b);
        j(i, k) = j(i, k) + c;
        j(i + 1, k + 1) = j(i + 1, k + 1) - c;
        j(k, i) = -j(i, k);
        j(k + 1, i + 1) = -j(i + 1, k + 1);

        c = dScalarAdjointJacobiFunction(t, a, b);
        dj(i, k) = dj(i, k) + c;
        dj(i + 1, k + 1) = dj(i + 1, k + 1) - c;
        dj(k, i) = -dj(i, k);
        dj(k + 1, i + 1) = -dj(i + 1, k + 1);

        // Case 4: |0 1|
        //         |1 0|
        // same sectional curvature as Case 3
        a = 0.5 * (w(i, k + 1) + w(i + 1, k));
        b = 0.5 * (dw(i, k + 1) + dw(i + 1, k));

        c = scalarAdjointJacobiFunction(t, a, b);
        j(i, k + 1) = j(i, k + 1) + c;
        j(i + 1, k) = j(i + 1, k) + c;
        j(k + 1, i) = -j(i, k + 1);
        j(k, i + 1) = -j(i + 1, k);

        c = dScalarAdjointJacobiFunction(t, a, b);
        dj(i, k + 1) = dj(i, k + 1) + c;
        dj(i + 1, k) = dj(i + 1, k) + c;
        dj(k + 1, i) = -dj(i, k + 1);
        dj(k, i + 1) = -dj(i + 1, k);
      }
    }

    // If n is odd, we have to traverse the last column
    if(n % 2 == 1)
    {
      for(i = 0; i < n - 1; i++)
      {
        // Sqrt sectional curvature times length of geodesic
        if(i % 2 == 0)
          t = 0.5 * fabs(vBD(i, i + 1)) * vNorm;
        else
          t = 0.5 * fabs(vBD(i, i - 1)) * vNorm;

        a = w(i, n - 1);
        b = dw(i, n - 1);

        j(i, n - 1) = scalarAdjointJacobiFunction(t, a, b);
        j(n - 1, i) = -j(i, n - 1);
        dj(i, n - 1) = dScalarAdjointJacobiFunction(t, a, b);
        dj(n - 1, i) = -dj(i, n - 1);
      }
    }

    j = jTang + U * j * trans(U);
    dj = jTang + djTang + U * dj * trans(U);
  }
  // SO(3) is special case with constant sectional curvature = 0.25
  // This works for SO(2) also because there is no orthogonal component
  else
  {
    j = jTang + cos(0.5 * vNorm) * jOrth -
      (0.5 * vNorm) * sin(0.5 * vNorm) * djOrth;
    dj = jTang + djTang + (2.0 / vNorm) * sin(0.5 * vNorm) * jOrth +
      cos(0.5 * vNorm) * djOrth;
  }

  j = parallelTranslateAtId(v, j);
  dj = parallelTranslateAtId(v, dj);
}
