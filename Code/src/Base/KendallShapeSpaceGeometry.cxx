/*==============================================================================
  File: KendallShapeSpaceGeometry.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include "KendallShapeSpaceGeometry.hxx"

ScalarType KendallShapeSpaceGeometry::
innerProduct(const PointType & p, const TangentType & v, const TangentType & w)
{
  ScalarType result = 0.0;
  unsigned int i, j;

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  for(j = 0; j < num; j++)
    for(i = 0; i < dim; i++)
      result += v(i, j) * w(i, j);
  return result;
}

KendallShapeSpaceGeometry::PointType
KendallShapeSpaceGeometry::
expMap(const PointType & base, const TangentType & t)
{
  ScalarType theta = sqrt(this->normSquared(base, t));

  if(theta < 1.0e-12)
    return base;

  PointType q = cos(theta) * base + (sin(theta) / theta) * t;
  return (PointType)(q / sqrt(this->normSquared(q, q)));
}

KendallShapeSpaceGeometry::
TangentType KendallShapeSpaceGeometry::
logMap(const PointType & base, const PointType & p)
{
  const MatrixType m = base * strans(p);

  MatrixType U, V; VectorType s;
  svd_econ(U, s, V, m, 'b');

  MatrixType rotation = U * strans(V);
  PointType qRot = rotation * p;

  ScalarType cosTheta = innerProduct(base, base, qRot);
  TangentType t = qRot - base * cosTheta;
  ScalarType length = sqrt(this->normSquared(base, t));

  if(length < 1.0e-12 || cosTheta >= 1.0 || cosTheta <= -1.0)
    return SuperClass::zeroVector;

  return (TangentType)(t * (acos(cosTheta) / length));
}

KendallShapeSpaceGeometry::TangentType
KendallShapeSpaceGeometry::
parallelTranslate(const PointType & p, const TangentType & v,
                  const TangentType & w)
{
  ScalarType vNorm = sqrt(this->normSquared(p, v));

  if(vNorm < 1.0e-12)
    return w;

  PointType pNew = p;
  TangentType vNew = v, wNew = w;

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  unsigned int numSteps = 5;
  ScalarType epsilon = 1 / static_cast<ScalarType>(numSteps), t = 0.0;
  TangentType left = arma::zeros<TangentType>(dim, num);

  unsigned int d = (dim * (dim - 1)) / 2;
  VectorType vec = arma::zeros<VectorType>(d);

  ScalarType diffNorm = sqrt(this->normSquared(p, v - w));
  if(diffNorm < 1.0e-12)
  {
    pNew = v * (sin(vNorm) / vNorm) + p * cos(vNorm);
    vNew = p * (-sin(vNorm) * vNorm) + v * cos(vNorm);

    // Project vNew onto pNew to get wNew
    projectTangent(wNew, pNew, vNew);
    return (TangentType)(wNew);
  }
  // Solving the horizontality equation vw' - wv' = S*p*p' - p*p'*S'
  // as a linear system, where S is an unknown skew-symmetric matrix
  else
  {
    for(int step = 0; step < numSteps; step++)
    {
      left = vNew * strans(wNew) - wNew * strans(vNew);
      int ind = 0;

      // setting up an AX = B system
      for(int i = 0; i < dim; i++)
      {
        for(int j = i+1; j < dim; j++)
        {
          vec[ind] = pow(-1.0f, i+j) * left(i, j);
          ind++;
        }
      }

      MatrixType matr = pNew * strans(pNew);
      MatrixType A = arma::zeros<MatrixType>(d, d);

      if(dim == 2)
      {
        A(0, 0) = 1 / (matr(0, 0) + matr(1, 1));
      }
      else if(dim == 3)
      {
        // for the dim = 3 case
        A(0, 0) = matr(0, 0) + matr(1, 1);
        A(1, 1) = matr(0, 0) + matr(2, 2);
        A(2, 2) = matr(1, 1) + matr(2, 2);

        A(0, 1) = - matr(1, 2);
        A(0, 2) = - matr(0, 2);
        A(1, 2) = - matr(0, 1);

        for(int i = 0; i < dim; i++)
          for(int j = i+1; j < dim; j++)
            A(j, i) = A(i, j);

        // X = inv(A) * B
        A = A.i();
      }

      VectorType sym = A * vec;

      // Skew-symmetric matrix S
      MatrixType S = arma::zeros<MatrixType>(dim, dim);

      ind = 0;
      for(int i = 0; i < dim; i++)
      {
        for(int j = i+1; j < dim; j++)
        {
          S(i, j) = pow(-1.0f, i+j) * sym[ind];
          S(j, i) = -S(i, j);
          ind++;
        }
      }

      // updating vNew, pNew and wNew
      TangentType changeInW = S * pNew;
      TangentType unprojectedW = wNew + epsilon * changeInW;

      // moving to new point on geodesic and parallel translating v
      t = static_cast<ScalarType>(step + 1) * epsilon * vNorm;
      pNew = (v / vNorm) * sin(t) + p * cos(t);
      vNew = p * (-sin(t) * vNorm) + v * cos(t);

      // projecting wNew at pNew.
      projectTangent(wNew, pNew, unprojectedW);
    }
    return (TangentType)(wNew);
  }
}

KendallShapeSpaceGeometry::TangentType
KendallShapeSpaceGeometry::
parallelTranslateAtoB(const PointType & a, const PointType & b,
                      const TangentType & w)
{
  TangentType v = logMap(a, b);
  return parallelTranslate(a, v, w);
}

void KendallShapeSpaceGeometry::
jacobiField(const PointType & p,
	    const TangentType & v,
            TangentType & j,
	    TangentType & dj)
{
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  unsigned int numSteps = 5;
  ScalarType epsilon = 1 / static_cast<ScalarType>(numSteps);
  TangentType temp = arma::zeros<TangentType>(dim, num);
  TangentType jDoublePrimeCurrent = dj;

  // Initializing
  PointType current = p;
  PointType q = p;
  TangentType vCurrent = v, jCurrent = j, djCurrent = dj;
  TangentType vAtQ = v, djAtQ = dj, jAtQ = j;
  ScalarType t = 0.0;
  for(int step = 0; step < numSteps; step++)
  {
    t = (step + 1) * epsilon;
    q = expMap(p, t * v);
    jDoublePrimeCurrent = - curvatureTensor(current, vCurrent, jCurrent, vCurrent);
    // updating JPrime
    temp = djCurrent + epsilon * jDoublePrimeCurrent;
    djAtQ = parallelTranslateAtoB(current, q, temp);
    // updating J
    temp = parallelTranslateAtoB(current, q, jCurrent);
    jAtQ = temp + epsilon * djAtQ;
    // updating v
    vAtQ = parallelTranslateAtoB(current, q, vCurrent);

    // Re-intializing
    current = q;
    jCurrent = jAtQ;
    djCurrent = djAtQ;
    vCurrent = vAtQ;
  }

  // re-setting values
  j = jAtQ;
  dj = djAtQ;
}

void KendallShapeSpaceGeometry::
adjointJacobiField(const PointType & p, const TangentType & v,
                   TangentType & j, TangentType & dj)
{
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  unsigned int numSteps = 5;
  ScalarType epsilon = 1 / static_cast<ScalarType>(numSteps);
  TangentType temp = arma::zeros<TangentType>(dim, num);
  TangentType jDoublePrimeCurrent = dj;

  // Initializing
  PointType current = p;
  PointType q = p;
  TangentType vCurrent = v, jCurrent = j, djCurrent = dj;
  TangentType vAtQ = v, djAtQ = dj, jAtQ = j;
  ScalarType t = 0.0;
  for(int step = 0; step < numSteps; step++)
  {
    t = (step + 1) * epsilon;
    q = expMap(p, t * v);
    jDoublePrimeCurrent = -curvatureTensor(current, vCurrent,
					   jCurrent, vCurrent);
    // updating JPrime
    temp = djCurrent + epsilon * jDoublePrimeCurrent;
    djAtQ = parallelTranslateAtoB(current, q, temp);
    // updating J
    temp = parallelTranslateAtoB(current, q, jCurrent);
    jAtQ = temp + epsilon * djAtQ;
    // updating v
    vAtQ = parallelTranslateAtoB(current, q, vCurrent);

    // Re-intializing
    current = q;
    jCurrent = jAtQ;
    djCurrent = djAtQ;
    vCurrent = vAtQ;
  }

  // re-setting values
  j = jAtQ;
  dj = djAtQ;
}

void KendallShapeSpaceGeometry::
verticalBasis(const PointType & p, TangentBasisType & vertBasis,
              IndexListType & vertIndex)
{
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  MatrixType U, V; VectorType s;
  IndexType index(2);

  svd(U, s, V, p);

  // Vertical basis
  MatrixType skewU = arma::zeros<MatrixType>(dim, dim);
  MatrixType eta = arma::zeros<MatrixType>(dim, dim);

  TangentType vertBasisVec = arma::zeros<TangentType>(dim, num);
  MatrixType lambda = arma::zeros<MatrixType>(dim, num);

  for(int i = 0; i < dim; i++)
    lambda(i, i) = s[i];

  for(int i = 0; i < dim; i++)
  {
    for(int j = i+1; j < dim; j++)
    {
      eta = arma::zeros<MatrixType>(dim, dim);

      skewU(i, j) = pow(-1.0f, i+j) / sqrt(2);
      skewU(j, i) = -skewU(i, j);
      eta = U * skewU;

      // vertical basis
      vertBasisVec = (eta * lambda * strans(V));
      vertBasis.push_back(vertBasisVec);
      index[0] = i;
      index[1] = j;
      vertIndex.push_back(index);

      // Re-initializing
      skewU(i, j) = 0.0;
      skewU(j, i) = 0.0;
    }
  }
}

void KendallShapeSpaceGeometry::
horizontalBasis(const PointType & p, TangentBasisType & diagBasis,
                IndexListType & diagIndex, TangentBasisType & offDiagBasis,
                IndexListType & offDiagIndex)
{
  MatrixType U, V; VectorType s;
  IndexType index = arma::zeros<IndexType>(2);

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  svd(U, s, V, p);
  MatrixType di = arma::zeros<MatrixType>(dim, num);
  TangentType diagBasisVec = arma::zeros<TangentType>(dim, num);
  VectorType u = arma::zeros<VectorType>(dim-1);
  double denom = 0.0;
  for(int i = 0; i < dim - 1; i++)
    u[i] = s[i+1]/(1 + s[0]);

  // restricting attention to dim = 2 or dim = 3
  if(dim == 2)
  {
    denom = pow(1 + u[0] * u[0], 2);
    // first u basis vector
    di(0, 0) = (-4 * u[0])/ denom;
    di(1, 1) = 2 * (1 - u[0] * u[0]) / denom;

    diagBasisVec = U * di * strans(V);
    diagBasisVec /= sqrt(this->normSquared(p, diagBasisVec));

    diagBasis.push_back(diagBasisVec);
    index[0] = 0;
    index[1] = 0;
    diagIndex.push_back(index);
  }
  else if(dim == 3)
  {
    denom = pow(1 + u[0] * u[0] + u[1] * u[1], 2);

    // first u basis vector
    di(0, 0) = (-4 * u[0])/ denom;
    di(1, 1) = 2 * (1 - u[0] * u[0] + u[1] * u[1])/ denom;
    di(2, 2) = (-4 * u[0] * u[1])/ denom;

    diagBasisVec = U * di * strans(V);
    diagBasisVec /= sqrt(this->normSquared(p, diagBasisVec));

    diagBasis.push_back(diagBasisVec);
    index[0] = 0;
    index[1] = 0;
    diagIndex.push_back(index);

    // second u basis vector
    di(0, 0) = (-4 * u[1]) / denom;
    di(1, 1) = (-4 * u[0] * u[1]) / denom;
    di(2, 2) = 2 * (1 + u[0] * u[0] - u[1] * u[1]) / denom;

    diagBasisVec = U * di * strans(V);
    diagBasisVec /= sqrt(this->normSquared(p, diagBasisVec));

    diagBasis.push_back(diagBasisVec);
    index[0] = 1;
    index[1] = 1;
    diagIndex.push_back(index);
  }

  // Horizontal basis for elements above diagonal
  MatrixType skewV = arma::zeros<MatrixType>(num, num);
  MatrixType ksi = arma::zeros<MatrixType>(num, num);

  TangentType vertBasisVec = arma::zeros<TangentType>(dim, num);
  TangentType offDiagBasisVec = arma::zeros<TangentType>(dim, num);

  TangentBasisType vertBasis;
  IndexListType vertIndex(2);
  double c =  0.0;
  unsigned int count = 0;

  MatrixType lambda = arma::zeros<MatrixType>(dim, num);
  for(int i = 0; i < dim; i++)
    lambda(i, i) = s[i];

  // getting the vertical basis
  verticalBasis(p, vertBasis, vertIndex);

  for(int i = 0; i < dim; i++)
  {
    for(int j = i+1; j < dim; j++)
    {
      ksi = arma::zeros<MatrixType>(num, num);
      skewV(i, j) = pow(-1.0f, i+j) / sqrt(2);
      skewV(j, i) = -skewV(i, j);
      ksi = skewV * strans(V);

      // Re-initializing
      skewV(i, j) = 0.0;
      skewV(j, i) = 0.0;

      // making ksi horizontal by adding the vertical basis vec
      vertBasisVec = vertBasis[count];

      c = 2 * lambda(i, i) * lambda(j, j);
      c /= (pow(lambda(i, i), 2) + pow(lambda(j, j), 2));

      offDiagBasisVec =  (U * lambda * ksi) - c * vertBasisVec;
      offDiagBasisVec /= sqrt(this->normSquared(p, offDiagBasisVec));

      offDiagBasis.push_back(offDiagBasisVec);
      index[0] = i;
      index[1] = j;
      offDiagIndex.push_back(index);
      count += 1;
    }
  }
}

void KendallShapeSpaceGeometry::
setBasis(PointType p, BasisType * basisDataNew)
{
  TangentBasisType diagBasis, offDiagBasis, vertBasis, kBasis;
  IndexListType diagIndex, offDiagIndex, vertIndex;

  horizontalBasis(p, diagBasis, diagIndex, offDiagBasis, offDiagIndex);
  verticalBasis(p, vertBasis, vertIndex);

  basisDataNew->uBasis = diagBasis;
  basisDataNew->uIndex = diagIndex;
  basisDataNew->ksiBasis = offDiagBasis;
  basisDataNew->ksiIndex = offDiagIndex;
  basisDataNew->vBasis = vertBasis;
  basisDataNew->vIndex = vertIndex;
  basisDataNew->p = p;
}

KendallShapeSpaceGeometry::TangentType
KendallShapeSpaceGeometry::
BasisLieBracket(const PointType & p, VectorType s,
                IndexType & index1, IndexType & index2,
		BasisType * basis)
{
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  unsigned int r1 = index1[0];
  unsigned int c1 = index1[1];
  unsigned int r2 = index2[0];
  unsigned int c2 = index2[1];
  double kNum = 0.0, kDenom = 0.0, k = 0.0;
  TangentType result = arma::zeros<TangentType>(dim, num);

  VectorType u = arma::zeros<VectorType>(dim-1);
  for(int i = 0; i < dim-1; i++)
    u[i] = s[i+1]/(1+s[0]);

  if(r1 == c1) // first basis vector is one of the u's
  {
    if(r2 == 0)
    {
      if(r1 == c2-1)
      {
        kNum = 2 * (s[0]*s[0] - s[c2]*s[c2]) * (s[0] + s[0]*s[0] + s[c2]*s[c2]);
        kDenom = pow(s[0]*s[0] + s[c2]*s[c2], 2);
        //cout << "k = " << kNum / kDenom << endl;
        result = (kNum / kDenom) * basis->vBasis[c2-1];
        return (TangentType)(result);
      }
      else
      {
        kNum = 2 * s[r1+1] * s[c2] * (s[0]*s[0] - s[c2]*s[c2]);
        kDenom = pow(s[0]*s[0] + s[c2]*s[c2], 2);

        //cout << "k = " << kNum / kDenom << endl;
        result = (kNum / kDenom) * basis->vBasis[c2-1];
        return (TangentType)(result);
      }
    }
    else
    {
      if(r1 == c2-1)
      {
        kNum = 2 * u[r2] * (u[r2]*u[r2] - u[c2]*u[c2]);
        kDenom = pow(u[r2]*u[r2] + u[c2]*u[c2], 2);
        //cout << "k = " << kNum / kDenom << endl;

        result = (kNum / kDenom) * basis->vBasis[c2];
        return (TangentType)(result);
      }
      else if(r1 == r2 - 1)
      {
        kNum = 2 * u[c2] * (u[c2]*u[c2] - u[r2]*u[r2]);
        kDenom = pow(u[r2]*u[r2] + u[c2]*u[c2], 2);
        //cout << "k = " << kNum / kDenom << endl;
        //cout << "u[r2] " << u(r2) << endl;
        result = (kNum / kDenom) * basis->vBasis[c2];
        return (TangentType)(result);
      }
      else
      {
        result = arma::zeros<TangentType>(dim, num);
        return (TangentType)(result);
      }
    }
  }
  else // both vectors are from the off-diagonal
  {
    if(r1 == r2 && c1 != c2)
    {
      kNum = 4 * s[r1] * s[c1] * s[r2] * s[c2];
      kDenom = (s[r1]*s[r1] + s[c1]*s[c1]);
      kDenom *= (s[r1]*s[r1] + s[c2]*s[c2]);
      k = kNum / kDenom;

      kNum = 2 * s[c1] * s[c2];
      kDenom = (s[c1]*s[c1] + s[c2]*s[c2]);
      k -= (kNum / kDenom);
      //cout << "k = " << k << endl;
      result = k * basis->vBasis[std::max(c1, c2)];
      return (TangentType)(result);
    }
    else
    {
      result = arma::zeros<TangentType>(dim, num);
      return (TangentType)(result);
    }
  }
}

KendallShapeSpaceGeometry::TangentType
KendallShapeSpaceGeometry::
LieBracketVertical(const PointType & p, const TangentType & v,
		   const TangentType & w, BasisType * basis)
{
  TangentBasisType diagBasis, offDiagBasis, vertBasis, kBasis;
  IndexListType diagIndex, offDiagIndex, vertIndex;
  MatrixType U, V; VectorType s;
  svd(U, s, V, p);
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  // call the setBasis function at p if using this function directly.
  diagBasis = basis->uBasis;
  offDiagBasis = basis->ksiBasis;
  vertBasis = basis->vBasis;
  diagIndex = basis->uIndex;
  offDiagIndex = basis->ksiIndex;
  vertIndex = basis->vIndex;

  // basis which will contribute to the Lie bracket are u's and some of ksi's
  for(int i = 0; i < vertBasis.size(); i++)
    kBasis.push_back(offDiagBasis[i]);

  double vu = 0.0, wk = 0.0, vk = 0.0, wu = 0.0;
  double vki = 0.0, wkj = 0.0, vkj = 0.0, wki = 0.0;
  double val = 0.0;

  TangentType lie = arma::zeros<TangentType>(dim, num);
  for(int i = 0; i < diagBasis.size(); i++)
  {
    vu = innerProduct(p, v, diagBasis[i]);
    wu = innerProduct(p, w, diagBasis[i]);
    for(int j = 0; j < kBasis.size(); j++)
    {
      wk = innerProduct(p, w, kBasis[j]);
      vk = innerProduct(p, v, kBasis[j]);
      val = vu * wk - vk * wu;
      lie += val * (BasisLieBracket(p, s, diagIndex[i],
				    vertIndex[j], basis));
    }
  }

  if(dim == 2)
    return lie;
  else
  {
    for(int i = 0; i < kBasis.size(); i++)
    {
      vki = innerProduct(p, v, kBasis[i]);
      wki = innerProduct(p, w, kBasis[i]);

      for(int j = i+1; j < kBasis.size(); j++)
      {
        vkj = innerProduct(p, v, kBasis[j]);
        wkj = innerProduct(p, w, kBasis[j]);

        val = vki * wkj - vkj * wki;
        lie += BasisLieBracket(p, s, vertIndex[i],
			       vertIndex[j], basis) * val;
      }
    }
    return (TangentType)(lie);
  }
}

KendallShapeSpaceGeometry::TangentType
KendallShapeSpaceGeometry::
curvatureTensor(const PointType & p,  const TangentType & x,
                const TangentType & y, const TangentType & z)
{
  BasisType * basisData = new BasisType;
  setBasis(p, basisData);

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  TangentType curv = arma::zeros<TangentType>(dim, num);
  double innerProd = 0.0;

  // actual number of horizontal vectors contributing to the Lie bracket
  unsigned int trueCount = (dim - 1) + (0.5 * dim * (dim - 1));

  double zy = 0.0, zx = 0.0; // known innerProds
  zx = innerProduct(p, z, x);
  zy = innerProduct(p, z, y);

  TangentType lieXY = arma::zeros<TangentType>(dim, num); // known lieBrackets
  TangentType lieYZ = arma::zeros<TangentType>(dim, num);
  TangentType lieZX = arma::zeros<TangentType>(dim, num);

  lieXY = LieBracketVertical(p, x, y, basisData);
  //cout << "vert[x, y] = " << endl << lieXY << endl;
  lieYZ = LieBracketVertical(p, y, z, basisData);
  //cout << "vert[y, z] = " << endl << lieYZ << endl;
  lieZX = LieBracketVertical(p, z, x, basisData);
  //cout << "vert[z, x] = " << endl << lieZX << endl;

  double yb = 0.0, xb = 0.0;
  TangentType lieXB = arma::zeros<TangentType>(dim, num); // basis dependent
  TangentType lieYB = arma::zeros<TangentType>(dim, num);
  TangentType lieZB = arma::zeros<TangentType>(dim, num);

  TangentBasisType diagBasis, offDiagBasis;
  IndexListType diagIndex(2), offDiagIndex(2);

  diagBasis = basisData->uBasis;
  offDiagBasis = basisData->ksiBasis;

  TangentBasisType hBasis;

  for(int i = 0; i < diagBasis.size(); i++)
    hBasis.push_back(diagBasis[i]);
  for(int i = 0; i < offDiagBasis.size(); i++)
    hBasis.push_back(offDiagBasis[i]);

  for(int i = 0; i < hBasis.size(); i++)
  {
    yb = innerProduct(p, y, hBasis[i]);
    xb = innerProduct(p, x, hBasis[i]);

    lieXB = LieBracketVertical(p, x, hBasis[i], basisData);
    lieYB = LieBracketVertical(p, y, hBasis[i], basisData);
    lieZB = LieBracketVertical(p, z, hBasis[i], basisData);

    innerProd = 0.5 * innerProduct(p, lieXY, lieZB);
    innerProd -= 0.25 * innerProduct(p, lieYZ, lieXB);
    innerProd -= 0.25 * innerProduct(p, lieZX, lieYB);

    curv += hBasis[i] * innerProd;
  }

  TangentType spCurv = y * zx - x * zy;
  return (TangentType)(spCurv + curv);
}

ScalarType KendallShapeSpaceGeometry::
sectionalCurvature(const PointType & p, const TangentType & v,
                   const TangentType & w)
{
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  TangentType curv = arma::zeros<TangentType>(dim, num);
  curv = curvatureTensor(p, v, w, v);
  double sec = innerProduct(p, curv, w);
  double vv = this->normSquared(p, v);
  double ww = this->normSquared(p, w);
  double vw = innerProduct(p, v, w);

  return (sec / ((vv * ww) - pow(vw, 2)));
}

void KendallShapeSpaceGeometry::
projectTangent(TangentType & hTangent, const PointType & p,
               const TangentType & tangent)
{
  unsigned int i, j;
  ScalarType innerProd;

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  TangentBasisType vertBasis;
  IndexListType vertIndex(2);
  TangentType basisVec = arma::zeros<TangentType>(dim, num);

  // Whats the innerProd
  innerProd = innerProduct(p, p, tangent);

  // Removing translation
  TangentType translateTangent = tangent;
  VectorType meanTgt = mean(tangent, 1);

  for(i = 0; i < num; i++)
  {
    VectorType tgtCol = tangent.col(i);
    translateTangent.col(i) = tgtCol - meanTgt;
  }

  // Inner Product
  innerProd = innerProduct(p, p, translateTangent);
  TangentType projectedTangent = tangent;
  projectedTangent = translateTangent - innerProd * p;

  hTangent = projectedTangent; // Initialization

  // removing the vertical components
  verticalBasis(p, vertBasis, vertIndex);

  for(int i = 0; i < vertBasis.size(); i++)
  {
    basisVec = vertBasis[i] / sqrt(this->normSquared(p, vertBasis[i]));
    innerProd = innerProduct(p, hTangent, basisVec);
    hTangent -= innerProd * basisVec;
  }

}

void KendallShapeSpaceGeometry::
tangentToList(const TangentType & v, ScalarListType & list)
{
  unsigned int i, j;
  unsigned int count = 0;

  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  list.set_size(dim * num);

  for(j = 0; j < num; j++)
    {
      for(i = 0; i < dim; i++)
	{
	  list[count] = v(i, j);
	  count += 1;
	}
    }

  return;
}

void KendallShapeSpaceGeometry::
listToTangent(const ScalarListType & list, TangentType & v)
{
  unsigned int i, j;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  v.set_size(dim, num);
  for(j = 0; j < num; j++)
    for(i = 0; i < dim; i++)
      v(i, j) = list[j * dim + i];

  return;
}

void KendallShapeSpaceGeometry::
pointToList(const PointType & p, ScalarListType & list)
{
  unsigned int i, j;
  unsigned int count = 0;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];

  list.set_size(dim * num);

  for(j = 0; j < num; j++)
    {
      for(i = 0; i < dim; i++)
	{
	  list[count] = p(i, j);
	  count += 1;
	}
    }

  return;
}

void KendallShapeSpaceGeometry::
listToPoint(const ScalarListType & list, PointType & p)
{
  unsigned int i, j;
  unsigned int dim = dimParameters[0];
  unsigned int num = dimParameters[1];
  p.set_size(dim, num);
  for(j = 0; j < num; j++)
    for(i = 0; i < dim; i++)
      p(i, j) = list[j * dim + i];

  return;
}

KendallShapeSpaceGeometry::PointType
KendallShapeSpaceGeometry::
preshape(PointType & p)
{
  unsigned int i;
  unsigned int num = dimParameters[1];

  // Removing translation and scale
  MatrixType translateP = p;
  VectorType meanP = mean(p, 1);

  for(i = 0; i < num; i++)
  {
    VectorType pCol = p.col(i);
    translateP.col(i) = pCol - meanP;
  }

  ScalarType scaleFactor = sqrt(this->normSquared(translateP, translateP));

  if(scaleFactor < 1.0e-12)
    return SuperClass::zeroVector;

  return (PointType)(translateP / scaleFactor);
}
