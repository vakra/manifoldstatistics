#include "MatrixExponential.hxx"

MatrixExponential::PointType
MatrixExponential::matrixExp2(const TangentType & v)
{
  double vTr = 0.5 * trace(v);

  // x is trace-free version of v
  PointType x = v;
  x(0, 0) = x(0, 0) - vTr;
  x(1, 1) = x(1, 1) - vTr;

  double xDet = det(x);

  double d;

  PointType output(2, 2);
  output.eye();

  if(xDet < -ZERO_THRESHOLD)
  {
    d = sqrt(-xDet);
    output = cosh(d) * output + (sinh(d) / d) * x;
  }
  else if(xDet > ZERO_THRESHOLD)
  {
    d = sqrt(xDet);
    output = cos(d) * output + (sin(d) / d) * x;
  }
  else
    output = output + x;

  // Exponentiate the trace and put it back in result
  return exp(vTr) * output;
}

MatrixExponential::PointType
MatrixExponential::matrixExp(const TangentType & v)
{
  int dimension = v.n_rows;

  if(dimension == 2)
    return matrixExp2(v);

  int ideg = 6;

  PointType output(dimension, dimension);

  int lwork_out = 4 * dimension * dimension + ideg + 1;
  double * work_out = new double[lwork_out];

  int * work_in = new int[dimension];
  int iexph;
  int ns;
  int iflag;

  double t = 1.0;

  const double * input = v.memptr();

  dgpadm_(&ideg, &dimension, &t, input, &dimension, work_out, &lwork_out,
          work_in, &iexph, &ns, &iflag);

  // Put result back into row-major
  int index = 0, i, j;
  for(j = 0; j < dimension; j++)
  {
    for(i = 0; i < dimension; i++)
    {
      output(i, j) = work_out[iexph + index - 1];
      index++;
    }
  }

  delete [] work_out;
  delete [] work_in;

  return output;
}

MatrixExponential::PointType
MatrixExponential::skewMatrixExp2(const TangentType & v)
{
  PointType g(2, 2);

  ScalarType theta = v(1, 0);

  g(0, 0) = cos(theta);
  g(1, 0) = sin(theta);
  g(0, 1) = -sin(theta);
  g(1, 1) = cos(theta);

  return g;
}

MatrixExponential::PointType
MatrixExponential::skewMatrixExp3(const TangentType & v)
{
  PointType g(3, 3);

  ScalarType theta = sqrt(v(1,0)*v(1,0) + v(2,0)*v(2,0) + v(2,1)*v(2,1));

  // Rodrigues' formula
  if(theta < 1e-12)
    g = eye<mat>(3, 3);
  else
    g = eye<mat>(3, 3) + (sin(theta) / theta) * v +
      ((1 - cos(theta)) / (theta * theta)) * (v * v);

  return g;
}

MatrixExponential::PointType
MatrixExponential::skewMatrixExp(const TangentType & v)
{
  int dimension = v.n_rows;

  if(dimension == 2)
    return skewMatrixExp2(v);
  else if(dimension == 3)
    return skewMatrixExp3(v);

  // General algorithm for n x n skew symmetric matrices
  cx_vec eVals;
  cx_mat U;

  // Should be a more efficient eigendecomposition for skew-symmetric matrices,
  // but maybe not in Armadillo
  eig_gen(eVals, U, v);

  // For some reason U is not coming out to be orthonormal columns, grr
  unsigned int i;
  for(i = 0; i < dimension; i++)
  {
    cx_vec col = U.col(i);
    ScalarType colNorm = arma::norm(col, "fro");
    U.col(i) = col / colNorm;
  }

  return real(U * diagmat(exp(eVals)) * htrans(U));
}

MatrixExponential::TangentType
MatrixExponential::rotationMatrixLog2(const PointType & r)
{
  TangentType v(2, 2);

  ScalarType theta = atan2(r(1, 0), r(0, 0));

  v(0, 0) = 0.0;
  v(1, 0) = theta;
  v(0, 1) = -theta;
  v(1, 1) = 0.0;

  return v;
}

MatrixExponential::TangentType
MatrixExponential::rotationMatrixLog3(const PointType & r)
{
  TangentType v(2, 2);

  ScalarType theta = acos(0.5 * (trace(r) - 1));

  if(fabs(theta) < 1e-12)
    v.fill(0.0);
  else
    v = (0.5 * theta / sin(theta)) * (r - trans(r));

  return v;
}

MatrixExponential::TangentType
MatrixExponential::rotationMatrixLog(const PointType & r)
{
  int dimension = r.n_rows;

  if(dimension == 2)
    return rotationMatrixLog2(r);
  else if(dimension == 3)
    return rotationMatrixLog3(r);

  cx_vec eVals;
  cx_mat U;

  // Again, should be a more efficient algorithm for eigen analysis here
  eig_gen(eVals, U, r);

  // For some reason U is not coming out to be orthonormal columns, grr
  unsigned int i;
  for(i = 0; i < dimension; i++)
  {
    cx_vec col = U.col(i);
    ScalarType colNorm = arma::norm(col, "fro");
    U.col(i) = col / colNorm;
  }

  //std::cout << "U = " << std::endl << U << std::endl;
  //std::cout << "eVals = " << eVals << std::endl;

  return real(U * diagmat(log(eVals)) * htrans(U));
}
