/*==============================================================================
  File: SphereGeometry.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#include "SphereGeometry.hxx"

ScalarType
SphereGeometry::innerProduct(const PointType & base, const TangentType & v,
                             const TangentType & w)
{
  ScalarType result = 0.0;
  unsigned int i;
  for(i = 0; i < dimension + 1; i++)
    result += v[i] * w[i];
  return result;
}

SphereGeometry::PointType
SphereGeometry::expMap(const PointType & base, const TangentType & t)
{
  ScalarType theta = sqrt(normSquared(base, t));
  if(theta < 1.0e-12)
    return base;

  PointType p = cos(theta) * base + (sin(theta) / theta) * t;
  ScalarType pNorm = sqrt(normSquared(p, p));
  return (PointType)(p / pNorm);
}

SphereGeometry::TangentType
SphereGeometry::logMap(const PointType & base, const PointType & p)
{
  ScalarType cosTheta = innerProduct(base, base, p);
  TangentType t = p - base * cosTheta;
  ScalarType length = sqrt(normSquared(base, t));

  // This also happens when theta = pi (log map is undefined)
  if(length < 1.0e-12 || cosTheta >= 1.0 || cosTheta <= -1.0)
    return SuperClass::zeroVector;

  return (TangentType)(t * (acos(cosTheta) / length));
}

// Parallel translate the tangent vector w at point p, along the geodesic with
// initial direction v
SphereGeometry::TangentType
SphereGeometry::parallelTranslate(const PointType & base, const TangentType & v,
                                  const TangentType & w)
{
  ScalarType vNorm = sqrt(normSquared(base, v));

  if(vNorm < 1.0e-12)
    return w;

  ScalarType innerProd = innerProduct(base, v, w);
  ScalarType scaleFactor =  innerProd / (vNorm * vNorm);

  // Component of w orthogonal to v
  TangentType orth = w - v * scaleFactor;

  // Compute parallel translated v
  TangentType vParallel = base * (-sin(vNorm) * vNorm) + v * cos(vNorm);

  // Add parallel translated v to orth, and get parallel translated w
  TangentType wParallelTranslated = vParallel * scaleFactor + orth;

  return wParallelTranslated;
}

SphereGeometry::TangentType
SphereGeometry::parallelTranslateAtoB(const PointType & a, const PointType & b,
                                      const TangentType & w)
{
  TangentType v = logMap(a, b);
  return parallelTranslate(a, v, w);
}

SphereGeometry::TangentType
SphereGeometry::curvatureTensor(const PointType & base, const TangentType & u,
                                const TangentType & v, const TangentType & w)
{
  ScalarType wDotU = innerProduct(base, w, u);
  ScalarType wDotV = innerProduct(base, w, v);

  TangentType sphereCurv = v * wDotU - u * wDotV;
  return sphereCurv;
}

ScalarType
SphereGeometry::sectionalCurvature(const PointType & base,
                                   const TangentType & v,
                                   const TangentType & w)
{
  return 1;
}

void
SphereGeometry::jacobiField(const PointType & base, const TangentType & v,
                            TangentType & j, TangentType & dj)
{
  ScalarType vNorm = sqrt(normSquared(base, v));
  if(vNorm < 1.0e-12)
  {
    j = j + dj;
    // dj = dj
  }
  else
  {
    ScalarType innerProdVJ = innerProduct(base, v, j);
    ScalarType innerProdVJPrime = innerProduct(base, v, dj);

    ScalarType scaleFactorJ = innerProdVJ / (vNorm * vNorm);
    ScalarType scaleFactorJPrime = innerProdVJPrime / (vNorm * vNorm);

    TangentType jTang = v * scaleFactorJ;
    TangentType djTang = v * scaleFactorJPrime;

    TangentType jOrth = j - jTang;
    TangentType djOrth = dj - djTang;

    j = jTang + djTang + cos(vNorm) * jOrth + (sin(vNorm) / vNorm) * djOrth;
    j = parallelTranslate(base, v, j);

    dj = djTang - (vNorm * sin(vNorm)) * jOrth + cos(vNorm) * djOrth;
    dj = parallelTranslate(base, v, dj);
  }
}

void
SphereGeometry::adjointJacobiField(const PointType & base,
                                   const TangentType & v,
                                   TangentType & j, TangentType & dj)
{
  ScalarType vNorm = sqrt(normSquared(base, v));
  if(vNorm < 1.0e-12)
  {
    // j = j
    dj = j + dj;
  }
  else
  {
    ScalarType innerProdVJ = innerProduct(base, v, j);
    ScalarType innerProdVJPrime = innerProduct(base, v, dj);

    ScalarType scaleFactorJ = innerProdVJ / (vNorm * vNorm);
    ScalarType scaleFactorJPrime = innerProdVJPrime / (vNorm * vNorm);

    TangentType jTang = v * scaleFactorJ;
    TangentType djTang = v * scaleFactorJPrime;

    TangentType jOrth = j - jTang;
    TangentType djOrth = dj - djTang;

    j = jTang + cos(vNorm) * jOrth - (vNorm * sin(vNorm)) * djOrth;
    j = parallelTranslate(base, v, j);

    dj = jTang + djTang + (sin(vNorm) / vNorm) * jOrth + cos(vNorm) * djOrth;
    dj = parallelTranslate(base, v, dj);
  }
}

void
SphereGeometry::projectTangent(TangentType & vProjected, const PointType & base,
                               const TangentType & v)
{
  ScalarType innerProd = innerProduct(base, base, v);
  vProjected = v - innerProd * base;
  return;
}

void
SphereGeometry::tangentToList(const TangentType & v, ScalarListType & list)
{
  unsigned int i, dim = v.size();
  list.set_size(dim);
  for(i = 0; i < dim; i++)
    list[i] = v[i];

  return;
}

void
SphereGeometry::listToTangent(const ScalarListType & list, TangentType & v)
{
  unsigned int i, dim = list.size();
  v.set_size(dim);

  for(i = 0; i < dim; i++)
    v[i] = list[i];

  return;
}

void
SphereGeometry::pointToList(const PointType & p, ScalarListType & list)
{
  unsigned int i, dim = p.size();
  list.set_size(dim);
  for(i = 0; i < dim; i++)
    list[i] = p[i];

  return;
}

void
SphereGeometry::listToPoint(const ScalarListType & list, PointType & p)
{
  unsigned int i, dim = list.size();
  p.set_size(dim);

  for(i = 0; i < dim; i++)
    p[i] = list[i];

  return;
}
