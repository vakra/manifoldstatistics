/*==============================================================================
  File: SpecialLinearGroupGeometry.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

// Implements an efficient formula for the Riemmanian exponential map, using the
// matrix exponential, as found in the book: Helgason, Diferential Geometry, Lie
// Groups and Symmetric Spaces, 1978.

#include "SpecialLinearGroupGeometry.hxx"

SpecialLinearGroupGeometry::PointType
SpecialLinearGroupGeometry::
expMapAtId(const TangentType & v)
{
  if(leftInvariant)
    return (mExp->matrixExp(trans(v))) * (mExp->skewMatrixExp(v - trans(v)));
  else
    return (mExp->skewMatrixExp(v - trans(v))) * (mExp->matrixExp(trans(v)));
}

SpecialLinearGroupGeometry::TangentType
SpecialLinearGroupGeometry::
logMapAtId(const PointType & q)
{
  return zeroVector;
}

SpecialLinearGroupGeometry::TangentType
SpecialLinearGroupGeometry::
parallelTranslateTangentAtId(const TangentType & v, ScalarType s)
{
  TangentType t = 0.5 * (v - trans(v));
  TangentType x = 0.5 * (v + trans(v));

  PointType g;

  if(leftInvariant)
    g = mExp->skewMatrixExp(-2 * s * t);
  else
    g = mExp->skewMatrixExp(2 * s * t);

  return t + adjointAction(g, x);
}

// Implements an RK4 integrator for parallel translated w,
// using exact parallel translated v
SpecialLinearGroupGeometry::TangentType
SpecialLinearGroupGeometry::
parallelTranslateAtId(const TangentType & v, const TangentType & w)
{
  TangentType vCurr, vHalf, vNext;
  TangentType wCurr;
  TangentType k1, k2, k3, k4;
  ScalarType sCurr, sHalf, sNext;

  // Arrange the time discretization
  ScalarType length = norm(id, v);
  unsigned int numSteps = ceil(length / stepSize);
  ScalarType delta = 1.0 / static_cast<ScalarType>(numSteps);
  unsigned int i;

  wCurr = w;
  vCurr = v;
  for(i = 0; i < numSteps; i++)
  {
    sCurr = (static_cast<ScalarType>(i)) * delta;
    sHalf = (static_cast<ScalarType>(i) + 0.5) * delta;
    sNext = (static_cast<ScalarType>(i) + 1.0) * delta;

    vCurr = parallelTranslateTangentAtId(v, sCurr);
    vHalf = parallelTranslateTangentAtId(v, sHalf);
    vNext = parallelTranslateTangentAtId(v, sNext);

    k1 = -connectionAtId(vCurr, wCurr);
    k2 = -connectionAtId(vHalf, wCurr + 0.5 * delta * k1);
    k3 = -connectionAtId(vHalf, wCurr + 0.5 * delta * k2);
    k4 = -connectionAtId(vNext, wCurr + delta * k3);

    wCurr = wCurr + (delta / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
  }

  return wCurr;
}

// Implements RK4 integration of system of first-order equations to solve Jacobi
// field equation, following (Bullo, 1995) and also (Hinkle et al., 2013), using
// exact parallel translated v
void SpecialLinearGroupGeometry::
jacobiFieldAtId(const TangentType & v, TangentType & j, TangentType & dj)
{
  TangentType vCurr, vHalf, vNext;
  TangentType jCurr;
  TangentType zCurr;
  TangentType k1, k2, k3, k4;
  TangentType h1, h2, h3, h4;
  ScalarType sCurr, sHalf, sNext;

  // Arrange the time discretization
  ScalarType length = norm(id, v);
  unsigned int numSteps = ceil(length / stepSize);
  ScalarType delta = 1.0 / static_cast<ScalarType>(numSteps);
  unsigned int i;

  jCurr = j;

  // Convert to reduced representation
  // Here z = \delta v (constrained variation of v)
  zCurr = dj + 0.5 * (ad(v, j) - sym(v, j));

  for(i = 0; i < numSteps; i++)
  {
    sCurr = (static_cast<ScalarType>(i)) * delta;
    sHalf = (static_cast<ScalarType>(i) + 0.5) * delta;
    sNext = (static_cast<ScalarType>(i) + 1.0) * delta;

    vCurr = parallelTranslateTangentAtId(v, sCurr);
    vHalf = parallelTranslateTangentAtId(v, sHalf);
    vNext = parallelTranslateTangentAtId(v, sNext);

    k1 = ad(jCurr, vCurr) + zCurr;
    h1 = -sym(vCurr, zCurr);

    k2 = ad(jCurr + 0.5 * delta * k1, vHalf) + zCurr + 0.5 * delta * h1;
    h2 = -sym(vHalf, zCurr + 0.5 * delta * h1);

    k3 = ad(jCurr + 0.5 * delta * k2, vHalf) + zCurr + 0.5 * delta * h2;
    h3 = -sym(vHalf, zCurr + 0.5 * delta * h2);

    k4 = ad(jCurr + delta * k3, vNext) + zCurr + delta * h3;
    h4 = -sym(vNext, zCurr + delta * h3);

    jCurr = jCurr + (delta / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
    zCurr = zCurr + (delta / 6) * (h1 + 2 * h2 + 2 * h3 + h4);
  }

  // Convert back to unreduced representation
  j = jCurr;
  dj = zCurr - 0.5 * (ad(v, j) - sym(v, j));
}

// Implements RK4 integration of system of first-order equations to solve
// adjoint Jacobi field equation, following (Bullo, 1995) and also
// (Hinkle et al., 2013), using exact parallel translated v
void SpecialLinearGroupGeometry::
adjointJacobiFieldAtId(const TangentType & v, TangentType & j, TangentType & dj)
{
  TangentType vCurr, vHalf, vNext;
  TangentType jCurr;
  TangentType zCurr;
  TangentType k1, k2, k3, k4;
  TangentType h1, h2, h3, h4;
  ScalarType sCurr, sHalf, sNext;

  // Arrange the time discretization
  ScalarType length = norm(id, v);
  unsigned int numSteps = ceil(length / stepSize);
  ScalarType delta = 1.0 / static_cast<ScalarType>(numSteps);
  unsigned int i;

  // Convert to reduced representation
  // Here z = \delta v (constrained variation of v)
  zCurr = dj;
  jCurr = j + 0.5 * (adTranspose(v, zCurr) + ad(v, zCurr) -
                     adTranspose(zCurr, v));

  for(i = 0; i < numSteps; i++)
  {
    sCurr = (static_cast<ScalarType>(i)) * delta;
    sHalf = (static_cast<ScalarType>(i) + 0.5) * delta;
    sNext = (static_cast<ScalarType>(i) + 1.0) * delta;

    vCurr = parallelTranslateTangentAtId(v, sCurr);
    vHalf = parallelTranslateTangentAtId(v, sHalf);
    vNext = parallelTranslateTangentAtId(v, sNext);

    k1 = adTranspose(vCurr, jCurr);
    h1 = jCurr + symTranspose(vCurr, zCurr);

    k2 = adTranspose(vHalf, jCurr + 0.5 * delta * k1);
    h2 = jCurr + 0.5 * delta * k1 + symTranspose(vHalf, zCurr + 0.5 * delta * h1);

    k3 = adTranspose(vHalf, jCurr + 0.5 * delta * k2);
    h3 = jCurr + 0.5 * delta * k2 + symTranspose(vHalf, zCurr + 0.5 * delta * h2);

    k4 = adTranspose(vNext, jCurr + delta * k3);
    h4 = jCurr + delta * k3 + symTranspose(vNext, zCurr + delta * h3);

    jCurr = jCurr + (delta / 6) * (k1 + 2 * k2 + 2 * k3 + k4);
    zCurr = zCurr + (delta / 6) * (h1 + 2 * h2 + 2 * h3 + h4);
  }

  // Convert back to unreduced representation
  j = jCurr - 0.5 * (adTranspose(vNext, zCurr) + ad(vNext, zCurr) -
                     adTranspose(zCurr, vNext));
  dj = zCurr;
}

void SpecialLinearGroupGeometry::
projectTangent(TangentType & vProjected, const PointType & p,
               const TangentType & v)
{
  vProjected = inv(p) * v;

  // Compute mean of diagonal
  arma::vec d = vProjected.diag();
  ScalarType mean = 0.0;
  unsigned int i;
  for(i = 0; i < d.size(); i++)
    mean += d[i];
  mean /= static_cast<ScalarType>(d.size());

  // Subtract mean from diagonal
  for(i = 0; i < d.size(); i++)
    vProjected(i, i) -= mean;

  vProjected =  p * vProjected;
}
