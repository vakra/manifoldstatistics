/*==============================================================================
  File: ManifoldFileWriter.txx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

template<class PointType, class TangentType>
bool ManifoldFileWriter::writePointList(const std::vector<PointType> & points)
{
  unsigned int i;
  unsigned int extrinsicDimension;

  if(!container)
  {
    cerr << "Error writing point list: container not initialized"
         << endl;
    return false;
  }

  RiemannianGeometryFactory<PointType, TangentType> factory;
  std::shared_ptr<RiemannianGeometry<PointType, TangentType> > geom;
  geom = factory.create(container);

  if(!geom)
  {
    cerr << "Error: Could not create Riemannian geometry from container"
         << endl;
    return false;
  }

  ofstream fout(fileName.c_str());

  if(fout.fail())
  {
    cerr << "Error: Could not open file for saving: " << fileName << endl;
    return false;
  }

  // Output magic number and file format version (currently 1)
  fout << "GFBR1" << endl;
  fout << "manifold = " << container->getManifoldType() << endl;
  fout << "primitive = point" << endl;
  fout << "dimParameters =" << flush;

  for(i = 0; i < container->getDimParameters().size(); i++)
    fout << " " << container->getDimParameters()[i];
  fout << endl;

  fout << "numElements = " << points.size() << endl;

  if(container->getIsBinary())
    fout << "format = binary" << endl;
  else
    fout << "format = text" << endl;
  fout << "data:" << endl;

  if(fout.fail())
  {
    cerr << "Error: Could not write header to file: " << fileName << endl;
    fout.close();
    return false;
  }

  extrinsicDimension = geom->getExtrinsicDimension();
  ScalarListType pointAsList(extrinsicDimension);
  PointType point;

  if(container->getIsBinary())
  {
    try
    {
      BOOST_FOREACH(point, points)
      {
        geom->pointToList(point, pointAsList);
        for(i = 0; i < extrinsicDimension; i++)
          fout.write(reinterpret_cast<char *>(&(pointAsList[i])), sizeof(ScalarType));
      }
    }
    catch(...)
    {
      cerr << "Error: Failed in writing binary point list to file: " << fileName
           << endl;
      fout.close();
      return false;
    }
  }
  else
  {
    // NOTE: numeric_limits provides the maximum precision that can be
    // safely expressed in base-10 given IEEE754 rounding conventions;
    // this is not the same as the maximum internal IEEE754 precision.
    fout.precision(std::numeric_limits<ScalarType>::digits10);

    try
    {
      BOOST_FOREACH(point, points)
      {
        geom->pointToList(point, pointAsList);
        for(i = 0; i < extrinsicDimension - 1; i++)
          fout << pointAsList[i] << " ";
        fout << pointAsList[extrinsicDimension - 1] << endl;
      }
    }
    catch(...)
    {
      cerr << "Error: Failed in writing text point list to file: " << fileName
           << endl;
      fout.close();
      return false;
    }
  }

  fout.close();

  return true;
}

template<class PointType, class TangentType>
bool ManifoldFileWriter::writeTangentBundleList(
  const std::vector<TangentBundlePointType<PointType, TangentType> > & tbPoints)
{
  unsigned int i;
  unsigned int extrinsicDimension;

  if(!container)
  {
    cerr << "Error writing tangent bundle list: container not initialized"
         << endl;
    return false;
  }

  RiemannianGeometryFactory<PointType, TangentType> factory;
  std::shared_ptr<RiemannianGeometry<PointType, TangentType> > geom;
  geom = factory.create(container);

  if(!geom)
  {
    cerr << "Error: Could not create Riemannian geometry from container"
         << endl;
    return false;
  }

  ofstream fout(fileName.c_str());

  if(fout.fail())
  {
    cerr << "Error: Could not open file for saving: " << fileName << endl;
    return false;
  }

  // Output magic number and file format version (currently 1)
  fout << "GFBR1" << endl;
  fout << "manifold = " << container->getManifoldType() << endl;
  fout << "primitive = TangentBundle" << endl;
  fout << "dimParameters =" << flush;

  for(i = 0; i < container->getDimParameters().size(); i++)
    fout << " " << container->getDimParameters()[i];
  fout << endl;

  fout << "numElements = " << tbPoints.size() << endl;

  if(container->getIsBinary())
    fout << "format = binary" << endl;
  else
    fout << "format = text" << endl;
  fout << "data:" << endl;

  if(fout.fail())
  {
    cerr << "Error: Could not write header to file: " << fileName << endl;
    fout.close();
    return false;
  }

  extrinsicDimension = geom->getExtrinsicDimension();
  ScalarListType pointAsList(extrinsicDimension);
  TangentBundlePointType<PointType, TangentType> tbPoint;

  if(container->getIsBinary())
  {
    try
    {
      BOOST_FOREACH(tbPoint, tbPoints)
      {
        // write base point
        geom->pointToList(tbPoint.p, pointAsList);
        for(i = 0; i < extrinsicDimension; i++)
          fout.write(reinterpret_cast<char *>(&(pointAsList[i])), sizeof(ScalarType));

        // write tangent
        geom->tangentToList(tbPoint.u, pointAsList);
        for(i = 0; i < extrinsicDimension; i++)
          fout.write(reinterpret_cast<char *>(&(pointAsList[i])), sizeof(ScalarType));
      }
    }
    catch(...)
    {
      cerr << "Error: Failed in writing binary point list to file: " << fileName
           << endl;
      fout.close();
      return false;
    }
  }
  else
  {
    // NOTE: numeric_limits provides the maximum precision that can be
    // safely expressed in base-10 given IEEE754 rounding conventions;
    // this is not the same as the maximum internal IEEE754 precision.
    fout.precision(std::numeric_limits<ScalarType>::digits10);

    try
    {
      BOOST_FOREACH(tbPoint, tbPoints)
      {
        // write base point
        geom->pointToList(tbPoint.p, pointAsList);
        for(i = 0; i < extrinsicDimension - 1; i++)
          fout << pointAsList[i] << " ";
        fout << pointAsList[extrinsicDimension - 1] << endl;

        // write tangent
        geom->tangentToList(tbPoint.u, pointAsList);
        for(i = 0; i < extrinsicDimension - 1; i++)
          fout << pointAsList[i] << " ";
        fout << pointAsList[extrinsicDimension - 1] << endl;
      }
    }
    catch(...)
    {
      cerr << "Error: Failed in writing text point list to file: " << fileName
           << endl;
      fout.close();
      return false;
    }
  }

  fout.close();

  return true;
}
