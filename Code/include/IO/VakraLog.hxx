/*==============================================================================
  File: VakraLog.hxx

  Implements a general-purpose logging facility for use in the manifold
  statitics research project.  Tries to provide reasonable default behaviour
  to minimise code footprint in client modules.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __VakraLog_hxx
#define __VakraLog_hxx

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/**
  @class VakraLog

  This is a general-purpose logging module that writes output to any text stream.
  By default it writes to std::clog but it can be directed to a file.  It is intended
  to provide a unified, "use anywhere" message-storing solution to simplify other code.

  @todo   Test under different environmental conditions (particularly redirecting stdio, stderr, etc).
 */
class VakraLog
{
  public:
    static const char * DEFAULT_TITLE;

    /** Create a logger that writes to std::clog with a default title.
     */
    explicit VakraLog(bool autoflush = true);

    /** Create a logger that writes to the given stream.
      @param title An optional identifier preprended to messages.
      @param destination Stream to write to.
     */
    explicit VakraLog(ostream & destination, const string title = DEFAULT_TITLE, bool autoflush = true);

    /** Create a logger that writes to the named file.
      @param title An optional identifier preprended to messages.
      @param destination File to open.
     */
    explicit VakraLog(const string destination, const string title = DEFAULT_TITLE, bool autoflush = true);
    ~VakraLog(void);

    void log(char * message);       /** Send a low-priority note.  @param message Text to log. */
    void log(const char * message); /** Send a low-priority note.  @param message Text to log. */
    void log(string message);       /** Send a low-priority note.  @param message Text to log. */
    void warn(char * message);      /** Send a low-priority warning.  @param message Text to log. */
    void warn(const char * message);/** Send a low-priority warning.  @param message Text to log. */
    void warn(string message);      /** Send a low-priority warning.  @param message Text to log. */
    void err(char * message);       /** Send a high-priority error message.  @param message Text to log. */
    void err(const char * message); /** Send a high-priority error message.  @param message Text to log. */
    void err(string message);       /** Send a high-priority error message.  @param message Text to log. */

    void flush(void);              /** Ensure all messages have been written. */
    bool bufferIsEmpty(void);      /** Check status.  @returns true if messages await writing. */

  private:
    static const char * DEFAULT_FILE;

    bool mBufferIsEmpty;
    bool mFileNeedsClosing;
    const string mTitle;
    ofstream mFile;
    ostream& mDestination;
    bool mAutoflush;
};

#endif  // Included VakraLog

