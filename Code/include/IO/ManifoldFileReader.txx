/*==============================================================================
  File: ManifoldFileReader.txx

  Defines templated member functions of ManifoldReader.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

template<class PointType, class TangentType>
bool ManifoldFileReader::readPointList(std::vector<PointType> & points)
{
  if(!container)
  {
    cerr << "Error reading point list: Container not initialized" << endl;
    points.clear();
    return false;
  }

  if(container->getPrimitiveType() != POINT)
  {
    cerr << "Error: Trying to read point list from non-point list file" << endl;
    points.clear();
    return false;
  }

  RiemannianGeometryFactory<PointType, TangentType> factory;
  std::shared_ptr<RiemannianGeometry<PointType, TangentType> > geom;
  geom = factory.create(container);

  unsigned int numPoints = container->getNumElements();
  unsigned int extrinsicDimension = geom->getExtrinsicDimension();
  unsigned int i, j, k;
  ScalarListType pList(extrinsicDimension);

  points.resize(numPoints);
  if(container->getIsBinary())
  {
    inputFile.open(fileName.c_str(), std::ifstream::binary);
    inputFile.seekg(filePos);

    unsigned int size = extrinsicDimension * numPoints * sizeof(ScalarType);

    char * data = new char[size];

    inputFile.read(data, size);

    k = 0;
    for(i = 0; i < numPoints; i++)
    {
      for(j = 0; j < extrinsicDimension; j++, k++)
        pList[j] = (reinterpret_cast<ScalarType *>(data))[k];

      geom->listToPoint(pList, points[i]);
    }

    delete [] data;

    inputFile.close();
  }
  else
  {
    std::string buffer;
    std::string lineBuffer;

    inputFile.open(fileName.c_str(), std::ifstream::in);
    inputFile.seekg(filePos);

    while(inputFile.good() && (filePos = inputFile.tellg()) > -1 &&
          getline(inputFile, lineBuffer))
    {
      buffer.append(lineBuffer);
      buffer.append(" ");
    }

    boost::char_separator<char> separator(" \t\r\n");
    boost::tokenizer<boost::char_separator<char> > tokens(buffer, separator);
    boost::tokenizer<boost::char_separator<char> >::iterator it;

    it = tokens.begin();
    for(i = 0; i < numPoints; i++)
    {
      for(j = 0; j < extrinsicDimension; j++, ++it)
      {
        if(it == tokens.end())
        {
          cerr << "Error: Unexpected end of file." << endl;
          points.clear();
          inputFile.close();
          return false;
        }

        try { pList[j] = boost::lexical_cast<ScalarType>(*it); }
        catch(boost::bad_lexical_cast &)
        {
          cerr << "Error converting to Scalar Type: " << *it << endl;
          points.clear();
          inputFile.close();
          return false;
        }
      }

      geom->listToPoint(pList, points[i]);
    }

    inputFile.close();
  }

  return true;
}
