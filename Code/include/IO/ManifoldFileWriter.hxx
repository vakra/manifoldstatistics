/*==============================================================================
  File: ManifoldFileWriter.hxx



  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __ManifoldFileWriter_hxx
#define __ManifoldFileWriter_hxx

#include "MFDContainer.hxx"
#include "VakraLog.hxx"

#include <boost/foreach.hpp>

class ManifoldFileWriter
{
public:
  ManifoldFileWriter() {}

  ManifoldFileWriter(std::string _fileName, std::string manifoldType,
                     DimensionList dimParameters, bool isBinary = true)
  {
    fileName = _fileName;

    container.reset(new MFDContainer());
    container->setManifoldType(manifoldType);
    container->setDimParameters(dimParameters);
    container->setIsBinary(isBinary);
  }

  void setManifold(std::string manifoldType, DimensionList dimParameters)
  {
    if(!container)
      container.reset(new MFDContainer());

    container->setManifoldType(manifoldType);
    container->setDimParameters(dimParameters);
  }

  void setManifold(std::string manifoldType, unsigned int dimension)
  {
    DimensionList dimParameters(1);
    dimParameters[0] = dimension;

    setManifold(manifoldType, dimParameters);
  }

  void setIsBinary(bool isBinary)
  {
    if(!container)
      container.reset(new MFDContainer());

    container->setIsBinary(isBinary);
  }

  void setFileName(std::string _fileName) { fileName = _fileName; }

  // Writes a list of points to the file.
  // Container and fileName should already be set.
  // Returns true if successful, false otherwise.
  template<class PointType, class TangentType>
  bool writePointList(const std::vector<PointType> & points);

  // Writes a list of tangent bundle elements
  template<class PointType, class TangentType>
  bool writeTangentBundleList(
    const std::vector<TangentBundlePointType<PointType, TangentType> > & points);

private:
  std::shared_ptr<MFDContainer> container;

  std::string fileName;
};

#include "ManifoldFileWriter.txx"

#endif
