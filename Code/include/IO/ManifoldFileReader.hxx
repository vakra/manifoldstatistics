/*==============================================================================
  File: ManifoldFileReader.hxx

  Defines a standard interface for reading manifold data, including points,
  tangents, etc., regardless of manifold type and dimension.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __ManifoldFileReader_hxx
#define __ManifoldFileReader_hxx

#include <boost/tokenizer.hpp>

#include "MFDContainer.hxx"
#include "VakraLog.hxx"
#include "RiemannianGeometryFactory.hxx"

#include <boost/lexical_cast.hpp>

class ManifoldFileReader
{
public:
  typedef boost::tokenizer<boost::char_separator<char> > inTokenizer;
  typedef inTokenizer::iterator inIterator;

  ManifoldFileReader() {}
  ManifoldFileReader(const std::string & _fileName)
  {
    load(_fileName);
  }

  ~ManifoldFileReader()
  {
    if(inputFile)
      inputFile.close();
  }

  // Opens a file and loads the header data
  void load(const std::string & _fileName);

  // Read the data block from a file and returns a point list
  // Either constructor with fileName or load should be called first
  // Returns true if successful, false otherwise
  template<class PointType, class TangentType>
  bool readPointList(std::vector<PointType> & points);

  std::shared_ptr<MFDContainer> getContainer() { return container; }

  template<class PointType, class TangentType>
  shared_ptr<RiemannianGeometry<PointType, TangentType> >
  getGeometry()
  {
    shared_ptr<RiemannianGeometry<PointType, TangentType> > geom;
    RiemannianGeometryFactory<PointType, TangentType> factory;
    if(container)
      geom = factory.create(container);

    return geom;
  }

private:
  std::shared_ptr<MFDContainer> container;

  std::string fileName;
  ifstream inputFile;
  std::streampos filePos;
};

#include "ManifoldFileReader.txx"

#endif
