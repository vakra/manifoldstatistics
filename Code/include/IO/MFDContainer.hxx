/*==============================================================================
  File: MFDContainer.hxx

  Provides a container for the header information found in MFD files.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __MFDContainer_hxx
#define __MFDContainer_hxx

#include <string>
#include "ManifoldPrimitives.hxx"

enum PrimitiveType
{
  UNKNOWN_PRIMITIVE,
  POINT,
  TANGENT,
  TANGENT_BUNDLE,
  FRAME,
  FRAME_BUNDLE,
  TENSOR,
  PGA
};

enum RepresentationType
{
  UNKNOWN_REPRESENTATION,
  SCALAR,
  VECTOR,
  MATRIX,
  VECTOR_TANGENT_BUNDLE,
  MATRIX_TANGENT_BUNDLE
};

class MFDContainer
{
public:
  MFDContainer()
  {
    manifoldType = "";
    primitiveType = UNKNOWN_PRIMITIVE;
    numElements = 0;
    isBinary = true;
  }

  ~MFDContainer() {}

  std::string getManifoldType() const { return manifoldType; }

  DimensionList getDimParameters() const { return dimParameters; }
  PrimitiveType getPrimitiveType() const { return primitiveType; }

  bool getIsBinary() const { return isBinary; }

  unsigned int getNumElements() const { return numElements; }

  void setManifoldType(const std::string & _manifoldType)
  {
    manifoldType = _manifoldType;
  }

  void setDimParameters(const DimensionList & _dimParameters)
  {
    dimParameters = _dimParameters;
  }

  void setPrimitiveType(const PrimitiveType & _primitiveType)
  {
    primitiveType = _primitiveType;
  }

  void setIsBinary(bool _isBinary) { isBinary = _isBinary; }

  void setNumElements(unsigned int _numElements)
  {
    numElements = _numElements;
  }

  RepresentationType getRepresentationType() const
  {
    if(manifoldType.compare("Euclidean") == 0 && dimParameters[0] == 1)
      return SCALAR;
    else if(manifoldType.compare("Euclidean") == 0 ||
            manifoldType.compare("Sphere") == 0)
      return VECTOR;
    else if(manifoldType.compare("SpecialLinearGroup") == 0 ||
            manifoldType.compare("SpecialOrthogonalGroup") == 0 ||
            manifoldType.compare("Grassmanian") == 0 ||
            manifoldType.compare("KendallShapeSpace") == 0)
      return MATRIX;
    else
      return UNKNOWN_REPRESENTATION;
  }

  friend std::ostream & operator<<(std::ostream & out,
                                   const MFDContainer & container)
  {
    unsigned int i;

    out << "Manifold type = " << container.manifoldType << endl;
    out << "Primitive type = " << container.primitiveType << endl;
    out << "Representation type = " << container.getRepresentationType()
        << endl;
    out << "Dimension parameters =" << flush;
    for(i = 0; i < container.dimParameters.size(); i++)
      out << " " << container.dimParameters[i];
    out << endl;
    out << "Number of elements = " << container.numElements << endl;

    return out;
  }

private:
  std::string manifoldType;
  DimensionList dimParameters;
  PrimitiveType primitiveType;
  bool isBinary;

  unsigned int numElements;
};

#endif
