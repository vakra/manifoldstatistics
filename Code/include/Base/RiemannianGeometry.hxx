/*==============================================================================
  File: RiemannianGeometry.hxx

  RiemannianGeometry is an abstract class for defining the geometry on a
  Riemannian manifold. The geometry is defined via a Riemannian metric, and this
  leads to geodesics, distances, and curvature.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __RiemannianGeometry_hxx
#define __RiemannianGeometry_hxx

#include "MetricSpace.hxx"

template<class PointType, class TangentType>
class RiemannianGeometry : public MetricSpace<PointType>
{
public:
  typedef std::vector<PointType> PointListType;
  typedef std::vector<TangentType> TangentListType;

  RiemannianGeometry() {}
  virtual ~RiemannianGeometry() {}

  // Defines the Riemannian metric at a base point for two tangent vectors
  virtual ScalarType innerProduct(const PointType & p, const TangentType & v,
                                  const TangentType & w) = 0;
  virtual ScalarType norm(const PointType & p, const TangentType & v)
  {
    return sqrt(normSquared(p, v));
  }
  virtual ScalarType normSquared(const PointType & p, const TangentType & v)
  {
    return innerProduct(p, v, v);
  }

  // expMap gives the Riemannian exponential map (i.e., geodesic segment
  // starting at base with initial velocity v).
  virtual PointType expMap(const PointType & p, const TangentType & v) = 0;

  // logMap is the inverse of the exponential map. It returns the initial
  // veloctiy vector for the geodesic segment between base and p.
  virtual TangentType logMap(const PointType & p, const PointType & q) = 0;

  // Geodesic distance between points a and b
  virtual ScalarType distance(const PointType & p, const PointType & q)
  {
    TangentType v = logMap(p, q);
    return norm(p, v);
  }

  virtual ScalarType distanceSquared(const PointType & p, const PointType & q)
  {
    TangentType v = logMap(p, q);
    return normSquared(p, v);
  }

  // Parallel translation of a tangent vector
  virtual TangentType parallelTranslate(const PointType & p,
                                        const TangentType & v,
                                        const TangentType & w) = 0;

  virtual TangentType parallelTranslateAtoB(const PointType & p,
                                            const PointType & q,
                                            const TangentType & w) = 0;

  // Computes the Riemannian curvature tensor R(u, v)w
  virtual TangentType curvatureTensor(const PointType & p,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w) = 0;

  // Gives the sectional curvature at a point
  virtual ScalarType sectionalCurvature(const PointType & p,
                                        const TangentType & v,
                                        const TangentType & w) = 0;

  // Gives the Jacobi field at time 1 along a geodesic
  // p, v are initial position and velocity of the geodesic
  // j, dj are the intial Jacobi field and its derivative at t=0
  // j, dj are overwritten with the return values at t=1
  virtual void jacobiField(const PointType & p, const TangentType & v,
                           TangentType & j, TangentType & dj) = 0;

  // Gives the adjoint Jacobi field at time 0 along a geodesic
  // p, v are initial position and (backward) velocity of the geodesic at t=1
  // j, dj are the intial Jacobi field and its derivative at t=1
  // j, dj are overwritten with the return values at t=0
  virtual void adjointJacobiField(const PointType & p, const TangentType & v,
                                  TangentType & j, TangentType & dj) = 0;

  virtual void setDimParameters(const DimensionList & _dimParameters) = 0;
  virtual DimensionList & getDimParameters() { return dimParameters; }
  virtual unsigned int getDimParameter(unsigned int n) { return dimParameters[n]; }
  virtual unsigned int getNumDimParameters() { return dimParameters.size(); }

  virtual unsigned int getDimension() { return dimension; }
  virtual unsigned int getExtrinsicDimension() { return extrinsicDimension; }

  virtual TangentType & getZeroVector() { return zeroVector; }

  // Takes a tangent v, and projects to the horizontal tangent vector at p,
  // returning vProjected
  virtual void projectTangent(TangentType & vProjected,
                              const PointType & p,
                              const TangentType & v) = 0;

  virtual void tangentToList(const TangentType & v,
                             ScalarListType & list) = 0;

  virtual void listToTangent(const ScalarListType & list,
                             TangentType & v) = 0;

  virtual void pointToList(const PointType & p,
			   ScalarListType & list) = 0;

  virtual void listToPoint(const ScalarListType & list,
			   PointType & p) = 0;

protected:
  // "dimension" is the manifold dimension in the usual sense.
  // "extrinsicDimension" is the dimension of the vector representation,
  // i.e., the Euclidean embedding space.
  // Derived classes will need to set these dimensions appropriately.
  unsigned int dimension;
  unsigned int extrinsicDimension;

  // Vector tracks the parameters that determine the manifold's dimension.
  // Derived classes must determine how these are used.
  DimensionList dimParameters;

  // This should be set in the constructor of a derived class.
  TangentType zeroVector;
};

#endif
