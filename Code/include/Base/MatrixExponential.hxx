/*==============================================================================
  File: MatrixExponential.hxx

  This is a wrapper for the expokit implementation of the matrix exponential
  function.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __MatrixExponential_hxx
#define __MatrixExponential_hxx

#include "ManifoldPrimitives.hxx"

static const double ZERO_THRESHOLD = 1.0e-8;

#ifdef __cplusplus
extern "C" {
#endif

void dgpadm_(int* ideg, int* m, double* t, const double* H, int* ldh, double* wsp,
             int* lwsp, int* ipiv, int* iexph, int* ns, int* iflag);

#ifdef __cplusplus
}

#endif

class MatrixExponential
{

public:
  typedef MatrixType PointType;
  typedef MatrixType TangentType;
  
  // Matrix exponential for a general 2x2 matrix
  PointType matrixExp2(const TangentType & v);

  // Matrix exponential for a general nxn matrix
  PointType matrixExp(const TangentType & v);

  // Matrix exponential for a 2x2 skew-symmetric matrix
  PointType skewMatrixExp2(const TangentType & v);

  // Matrix exponential for a 3x3 skew-symmetric matrix
  PointType skewMatrixExp3(const TangentType & v);

  // Matrix exponential for a nxn skew-symmetric matrix
  PointType skewMatrixExp(const TangentType & v);

  // Matrix log for a 2x2 rotation matrix (returns skew-symmetric matrix)
  TangentType rotationMatrixLog2(const PointType & r);

  // Matrix log for a 3x3 rotation matrix (returns skew-symmetric matrix)
  TangentType rotationMatrixLog3(const PointType & r);

  // Matrix log for a nxn rotation matrix (returns skew-symmetric matrix)
  TangentType rotationMatrixLog(const PointType & r);

  // TODO: add symmetric matrix exponential
  // Matrix exponential for a symmetric matrix
  // PointType symMatrixExp(const TangentType & v);
};

#endif
