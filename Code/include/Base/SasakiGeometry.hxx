/*=============================================================================
  File: SasakiGeometry.hxx

  Description: SasakiGeometry is an abstract class to define the Riemannain
  geometry on the tangent bundle of a manifold by way of defining the Sasaki
  metric on the Tangent bundle.
============================================================================= */

#ifndef __SasakiGeometry_hxx
#define __SasakiGeometry_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
struct TangentBundlePointType
{
public:
  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;

  PointType p;
  TangentType u;

  inline TangentBundlePointType() : p(), u() {}

  inline TangentBundlePointType(const TangentBundlePointType & z)
  {
    p = z.p;
    u = z.u;
  }

  inline TangentBundlePointType(const PointType &q, const TangentType &t)
  {
    p = q;
    u = t;
  }

  inline TangentBundlePointType operator=(const TangentBundlePointType & z)
  {
    p = z.p;
    u = z.u;

    return *this;
  }

  friend std::ostream & operator<<(std::ostream & out,
                                   const TangentBundlePointType & z)
  {
    out << z.p << " " << z.u;
    return out;
  }
};

template<class TangentType>
struct TangentBundleTangentType
{
public:
  TangentType x;
  TangentType y;

  inline TangentBundleTangentType() : x(), y() {}

  inline TangentBundleTangentType(const TangentBundleTangentType & z)
  {
    x = z.x;
    y = z.y;
  }

  inline TangentBundleTangentType(const TangentType &t1, const TangentType &t2)
  {
    x = t1;
    y = t2;
  }

  inline TangentBundleTangentType operator=(const TangentBundleTangentType & z)
  {
    x = z.x;
    y = z.y;

    return *this;
  }

  inline TangentBundleTangentType & operator+=(const TangentBundleTangentType & u)
  {
    x += u.x;
    y += u.y;

    return *this;
  }

  inline TangentBundleTangentType & operator*=(const ScalarType s)
  {
    x *= s;
    y *= s;

    return *this;
  }

  inline TangentBundleTangentType operator+(TangentBundleTangentType u) const
  {
    TangentBundleTangentType result(x + u.x, y + u.y);
    return result;
  }

  inline TangentBundleTangentType operator-(TangentBundleTangentType u) const
  {
    TangentBundleTangentType result(x - u.x, y - u.y);
    return result;
  }

  inline TangentBundleTangentType operator-() const
  {
    TangentBundleTangentType result(-(x), -(y));
    return result;
  }

  const TangentBundleTangentType operator*(ScalarType s) const
  {
    TangentBundleTangentType result(x * s, y * s);
    return result;
  }

  inline TangentBundleTangentType operator/(ScalarType s) const
  {
    TangentBundleTangentType result(x / s, y / s);
    return result;
  }
};

typedef TangentBundlePointType<VectorType, VectorType> VectorTBPointType;
typedef TangentBundleTangentType<VectorType> VectorTBTangentType;
typedef TangentBundlePointType<MatrixType, MatrixType> MatrixTBPointType;
typedef TangentBundleTangentType<MatrixType> MatrixTBTangentType;

template<class ManifoldPointType, class ManifoldTangentType>
class SasakiGeometry :
  public RiemannianGeometry<TangentBundlePointType<ManifoldPointType, ManifoldTangentType>,
                            TangentBundleTangentType<ManifoldTangentType> >
{
public:
  typedef TangentBundlePointType<ManifoldPointType, ManifoldTangentType> TMPointType;
  typedef TangentBundleTangentType<ManifoldTangentType> TMTangentType;
  typedef RiemannianGeometry<ManifoldPointType, ManifoldTangentType> ManifoldGeometryType;
  typedef RiemannianGeometry<TMPointType, TMTangentType> SuperClass;
  typedef std::shared_ptr<ManifoldGeometryType> ManifoldGeometryPtr;

  typedef std::vector<TMPointType> TMPointListType;

  SasakiGeometry(ManifoldGeometryPtr _geom) : SuperClass()
  {
    geom = _geom;
    if(geom)
    {
      SuperClass::zeroVector.x = geom->getZeroVector();
      SuperClass::zeroVector.y = geom->getZeroVector();
    }
  }

  ManifoldGeometryPtr getManifoldGeometry() {return geom; }

  ScalarType innerProduct(const TMPointType & base,
			  const TMTangentType & v,
			  const TMTangentType & w)
  {
    if(geom)
      return(geom->innerProduct(base.p, v.x, w.x) +
             geom->innerProduct(base.p, v.y, w.y));

    return 0.0;
  }

  virtual TMPointType expMap(const TMPointType & base,
			     const TMTangentType & v);

  virtual TMTangentType logMap(const TMPointType & base,
			       const TMPointType & target);

  virtual unsigned int getDimension()
  {
    if(geom)
      return 2 * geom->getDimension();

    return 0;
  }

  virtual TMTangentType parallelTranslate(const TMPointType & base,
					  const TMTangentType & v,
					  const TMTangentType & w);

  virtual TMTangentType parallelTranslateAtoB(const TMPointType & a,
					      const TMPointType & b,
					      const TMTangentType & w)
  {
    TMTangentType v = this->logMap(a, b);
    return this->parallelTranslate(a, v, w);
  }

  unsigned int getExtrinsicDimension()
  {
    if(geom)
      return 2 * geom->getExtrinsicDimension();

    return 0;
  }

  virtual void projectTangent(TMTangentType & vProjected,
			      const TMPointType & base,
			      const TMTangentType & v)
  {
    geom->projectTangent(vProjected.x, base.p, v.x);
    geom->projectTangent(vProjected.y, base.p, v.y);
    return;
  }

  virtual TMTangentType curvatureTensor(const TMPointType & p,
					const TMTangentType & u,
					const TMTangentType & v,
					const TMTangentType & w)
  {
    return SuperClass::zeroVector;
  }

  // Gives the sectional curvature at a point
  virtual ScalarType sectionalCurvature(const TMPointType & p,
                                        const TMTangentType & v,
                                        const TMTangentType & w)
  {
    return 0;
  }

  // Gives the Jacobi field at time 1 along a geodesic
  // p, v are initial position and velocity of the geodesic
  // j, dj are the intial Jacobi field and its derivative at t=0
  // j, dj are overwritten with the return values at t=1
  virtual void jacobiField(const TMPointType & p, const TMTangentType & v,
                           TMTangentType & j, TMTangentType & dj)
  {
    return;
  }

  // Gives the adjoint Jacobi field at time 0 along a geodesic
  // p, v are initial position and (backward) velocity of the geodesic at t=1
  // j, dj are the intial Jacobi field and its derivative at t=1
  // j, dj are overwritten with the return values at t=0
  virtual void adjointJacobiField(const TMPointType & p, const TMTangentType & v,
                                  TMTangentType & j, TMTangentType & dj)
  {
    return;
  }

  // NOTE: dimensions are determined by the "geom" dimensions, so this function
  // does nothing
  virtual void setDimParameters(const DimensionList & dims)  {}

  virtual void tangentToList(const TMTangentType & v, ScalarListType & list)
  {
    ScalarListType listX, listY;
    geom->tangentToList(v.x, listX);
    geom->tangentToList(v.y, listY);

    unsigned int i, x = listX.size(), y = listY.size();
    list.resize(x + y);
    for(i = 0; i < x; i++)
      list[i] = listX[i];
    for(i = 0; i < listY.size(); i++)
      list[i + x] = listY[i];
    return;
  }

  virtual void listToTangent(const ScalarListType & list, TMTangentType & v)
  {
    unsigned int i, n = list.size() / 2;
    ScalarListType listX, listY;
    listX.resize(n);
    listY.resize(n);
    for(i = 0; i < n; i++)
    {
      listX[i] = list[i];
      listY[i] = list[i+n];
    }
    geom->listToTangent(listX, v.x);
    geom->listToTangent(listY, v.y);
    return;
  }

  virtual void pointToList(const TMPointType & a, ScalarListType & list)
  {
    ScalarListType listP, listU;
    geom->pointToList(a.p, listP);
    geom->tangentToList(a.u, listU);

    unsigned int i, p = listP.size(), u = listU.size();
    list.resize(p + u);
    for(i = 0; i < p; i++)
      list[i] = listP[i];
    for(i = 0; i < listU.size(); i++)
      list[i + p] = listU[i];

    return;
  }

  virtual void listToPoint(const ScalarListType & list, TMPointType & a)
  {
    unsigned int i, n = list.size() / 2;
    ScalarListType listP, listU;
    listP.resize(n);
    listU.resize(n);
    for(i = 0; i < n; i++)
    {
      listP[i] = list[i];
      listU[i] = list[i+n];
    }
    geom->listToPoint(listP, a.p);
    geom->listToTangent(listU, a.u);
    return;
  }

protected:
  ManifoldGeometryPtr geom;
};

#include "SasakiGeometry.txx"

#endif
