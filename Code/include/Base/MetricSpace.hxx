/*==============================================================================
  File: MetricSpace.hxx

  MetricSpace is an abstract class for defining distances between objects. It's
  only member function is distance().

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __MetricSpace_hxx
#define __MetricSpace_hxx

#include "ManifoldPrimitives.hxx"

template<class PointType>
class MetricSpace
{
public:
  MetricSpace() {}
  virtual ~MetricSpace() {}

  virtual ScalarType distance(const PointType & p1, const PointType & p2) = 0;
};

#endif
