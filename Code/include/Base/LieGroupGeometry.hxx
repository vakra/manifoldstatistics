/*==============================================================================
  File: LieGroupGeometry.hxx

  This is an abstract class to define the differential geometry of a Lie group.
  It is derived from RiemannianGeometry. Note that TangentType vectors are often
  assumed to be in the Lie algebra and represent invariant vector fields. They
  can be either left- or right-invariant.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __LieGroupGeometry_hxx
#define __LieGroupGeometry_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
class LieGroupGeometry : public RiemannianGeometry<PointType, TangentType>
{
public:
  typedef RiemannianGeometry<PointType, TangentType> SuperClass;
  
  LieGroupGeometry(bool _leftInvariant = true) : SuperClass()
  {
    leftInvariant = _leftInvariant;
  }
  virtual ~LieGroupGeometry() {}

  virtual PointType getId() { return id; }

  virtual PointType inverse(const PointType & p) = 0;

  virtual ScalarType innerProductAtId(const TangentType & v,
                                      const TangentType & w) = 0;

  virtual ScalarType innerProduct(const PointType & p, const TangentType & v,
                                  const TangentType & w)
  {
    return innerProductAtId(translate(inverse(p), v), translate(inverse(p), w));
  }

  virtual ScalarType normSquaredAtId(const TangentType & v)
  {
    return innerProductAtId(v, v);
  }

  virtual ScalarType normAtId(const TangentType & v)
  {
    return sqrt(innerProductAtId(v, v));
  }

  // Left multiplication of a point q by the point p
  virtual PointType leftMultiply(const PointType & p, const PointType & q) = 0;

  // Right multiplication of a point q by the point p
  virtual PointType rightMultiply(const PointType & p, const PointType & q) = 0;

  // Applies left or right multiplication of q by p, depending on invariance
  virtual PointType multiply(const PointType & p, const PointType & q)
  {
    if(leftInvariant)
      return leftMultiply(p, q);
    else
      return rightMultiply(p, q);
  }

  // Left translation by p of a tangent vector v in Lie algebra
  // Returned tangent vector is in tangent space of p
  virtual TangentType leftTranslate(const PointType & p, 
				    const TangentType & v) = 0;

  // Right translation by p of a tangent vector v in Lie algebra
  // Returned tangent vector is in tangent space of p
  virtual TangentType rightTranslate(const PointType & p, 
				     const TangentType & v) = 0;

  // Applies either left or right translation, depending on metric invariance
  virtual TangentType translate(const PointType & p, const TangentType & v)
  {
    if(leftInvariant)
      return leftTranslate(p, v);
    else
      return rightTranslate(p, v);
  }

  // Computes the exponential map starting at the identity
  // The initial velocity v is an element of the Lie algebra
  virtual PointType expMapAtId(const TangentType & v) = 0;

  // expMap gives the Riemannian exponential map (i.e., geodesic segment
  // starting at p with initial velocity v).
  virtual PointType expMap(const PointType & p, const TangentType & v)
  {
    // Translate to identity, expMapAtId, then multiply back
    PointType q = expMapAtId(translate(inverse(p), v));
    return multiply(p, q);
  }

  // Computes log map from id to q. Returns the initial velocity of the geodesic
  // segment, which is an element of the Lie algebra.
  virtual TangentType logMapAtId(const PointType & q) = 0;

  // logMap is the inverse of the exponential map. It returns the initial
  // veloctiy vector for the geodesic segment p and q.
  virtual TangentType logMap(const PointType & p, const PointType & q)
  {
    TangentType v = logMapAtId(translate(inv(p), q));

    return translate(p, v);
  }

  // Adjoint action, aka, "Big" Ad operator (v is in Lie algebra)
  virtual TangentType adjointAction(const PointType & p, const TangentType & v)
  {
    return rightTranslate(inverse(p), leftTranslate(p, v));
  }

  // Transpose of Ad (v is in Lie algebra)
  virtual TangentType adjointActionTranspose(const PointType & p,
                                             const TangentType & v) = 0;

  // Little ad operator, a.k.a. Lie bracket (v, w are in Lie algebra)
  virtual TangentType ad(const TangentType & v, const TangentType & w) = 0;

  // Transpose of ad operator (v, w are in Lie algebra)
  virtual TangentType adTranspose(const TangentType & v, 
				  const TangentType & w) = 0;

  // The symmetric product of two vectors: sym_v w = -ad_v^T w - ad_w^T v
  virtual TangentType sym(const TangentType & v, const TangentType & w)
  {
    return -adTranspose(v, w) - adTranspose(w, v);
  }

  // Transpose of the symmetric product: sym_v^T w = -ad_v w + ad_w^T v
  virtual TangentType symTranspose(const TangentType & v, 
				   const TangentType & w)
  {
    return -ad(v, w) + adTranspose(w, v);
  }

  // Affine connection for two invariant vector fields (v, w are in Lie algebra)
  virtual TangentType connectionAtId(const TangentType & v,
                                     const TangentType & w)
  {
    return 0.5 * (ad(v, w) - adTranspose(v, w) - adTranspose(w, v));
  }

  // Parallel translation of w along geodesic starting at the identity with
  // initial velocity v, defined in Lie algebra.
  virtual TangentType parallelTranslateAtId(const TangentType & v,
                                            const TangentType & w) = 0;

  virtual TangentType parallelTranslate(const PointType & p,
                                        const TangentType & v,
                                        const TangentType & w)
  {
    TangentType vAtId, wAtId;
    vAtId = translate(inverse(p), v);
    wAtId = translate(inverse(p), w);

    return translate(expMap(p, v), parallelTranslateAtId(vAtId, wAtId));
  }

  virtual TangentType parallelTranslateAtoB(const PointType & p,
                                            const PointType & q,
                                            const TangentType & w)
  {
    TangentType v = logMap(p, q);
    TangentType vAtId = translate(inverse(p), v);
    TangentType wAtId = translate(inverse(p), w);

    return translate(q, parallelTranslateAtId(vAtId, wAtId));
  }

  // Riemannian curvature tensor at identity (u, v, w are in Lie algebra)
  virtual TangentType curvatureTensorAtId(const TangentType & u,
                                          const TangentType & v,
                                          const TangentType & w)
  {
    return connectionAtId(u, connectionAtId(v, w)) -
      connectionAtId(v, connectionAtId(u, w)) - connectionAtId(ad(u, v), w);
  }

  // Riemannian curvature tensor, R(u, v)w,  at a point p (u, v, w are at p)
  virtual TangentType curvatureTensor(const PointType & p,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w)
  {
    TangentType uAtId, vAtId, wAtId;
    uAtId = translate(inverse(p), u);
    vAtId = translate(inverse(p), v);
    wAtId = translate(inverse(p), w);

    return translate(p, curvatureTensorAtId(uAtId, vAtId, wAtId));
  }

  // Sectional curvature in the Lie algebra (see Cheeger and Ebin, 1975)
  // This should be redefined in derived classes if curvature simplifies
  virtual ScalarType sectionalCurvatureAtId(const TangentType & v,
                                            const TangentType & w)
  {
    return normSquaredAtId(sym(v, w)) -
      innerProductAtId(adTranspose(v, v), adTranspose(w, w)) -
      0.75 * normSquaredAtId(ad(v, w)) -
      0.5 * innerProductAtId(ad(ad(v, w), w), v) -
      0.5 * innerProductAtId(ad(ad(w, v), v), w);
  }

  // Gives the sectional curvature at a point
  virtual ScalarType sectionalCurvature(const PointType & p,
                                        const TangentType & v,
                                        const TangentType & w)
  {
    PointType pInv = inverse(p);
    return sectionalCurvatureAtId(translate(pInv, v), translate(pInv, w));
  }

  // Gives the Jacobi field at time 1 along a geodesic
  // id, v is the intial position and velocity of the geodesic at t=0
  // j, dj are the initial Jacobi field and its derivative at t=0
  // j, dj are overwritten with the return values at t=1
  // All vectors are in the Lie algebra
  virtual void jacobiFieldAtId(const TangentType & v, TangentType & j,
                               TangentType & dj) = 0;

  // Gives the Jacobi field tangent vector at time 1 along a geodesic
  // p, v are initial position and velocity of the geodesic at t=0
  // j, dj are the intial Jacobi field and its derivative at t=0
  // j, dj are overwritten with the return values at t=1
  virtual void jacobiField(const PointType & p, const TangentType & v,
                           TangentType & j, TangentType & dj)
  {
    TangentType vAtId;
    vAtId = translate(inverse(p), v);
    j = translate(inverse(p), j);
    dj = translate(inverse(p), dj);

    jacobiFieldAtId(vAtId, j, dj);

    PointType q = expMap(p, v);

    j = translate(q, j);
    dj = translate(q, dj);
  }

  // Gives the adjoint Jacobi field at time 0 along a geodesic
  // id, v are initial position and (backward) velocity of the geodesic at t=1
  // j, dj are the intial Jacobi field and its derivative at t=1
  // j, dj are overwritten with the return values at t=0
  // All vectors are in the Lie algebra
  virtual void adjointJacobiFieldAtId(const TangentType & v,
                                      TangentType & j, TangentType & dj) = 0;

  // Gives the adjoint Jacobi field at time 0 along a geodesic
  // p, v are initial position and (backward) velocity of the geodesic at t=1
  // j, dj are the intial Jacobi field and its derivative at t=1
  // j, dj are overwritten with the return values at t=0
  virtual void adjointJacobiField(const PointType & p, const TangentType & v,
                                  TangentType & j, TangentType & dj)
  {
    TangentType vAtId;
    vAtId = translate(inverse(p), v);
    j = translate(inverse(p), j);
    dj = translate(inverse(p), dj);

    adjointJacobiFieldAtId(vAtId, j, dj);

    PointType q = expMap(p, v);

    j = translate(q, j);
    dj = translate(q, dj);
  }

protected:
  // Is the metric left or right invariant?
  bool leftInvariant;

  // Identity element
  PointType id;
};

#endif
