/*==============================================================================
  File: RiemannianGeometryFactory.txx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

template<>
typename
RiemannianGeometryFactory<ScalarType, ScalarType>::SharedGeometryPtr
RiemannianGeometryFactory<ScalarType, ScalarType>::
create(string type, DimensionList parameters)
{
  SharedGeometryPtr geom;

  if(type.compare("Euclidean") == 0)
  {
    if(parameters.size() != 1)  // TODO: more robust error handling
      cerr << "Error: EuclideanGeometry can only have one dimension." << endl;
    else if(parameters[0] != 1)
      cerr << "Error: Creating ScalarType manifold, but dimension != 1."
           << endl;
    else
      geom.reset(new EuclideanGeometry<ScalarType, ScalarType>(parameters));
  }
  else  // TODO: more robust error handling
    cerr << "Error: Cannot create manifold type " << type << " using ScalarType." << endl;

  return geom;
}

template<>
typename
RiemannianGeometryFactory<VectorType, VectorType>::SharedGeometryPtr
RiemannianGeometryFactory<VectorType, VectorType>::
create(string type, DimensionList parameters)
{
  SharedGeometryPtr geom;

  if(type.compare("Euclidean") == 0)
  {
    if(parameters.size() != 1)  // TODO: more robust error handling
      cerr << "Error: EuclideanGeometry can only have one dimension." << endl;
    else
      geom.reset(new EuclideanGeometry<VectorType, VectorType>(parameters));
  }
  else if(type.compare("Sphere") == 0)
  {
    if(parameters.size() != 1)  // TODO: more robust error handling
      cerr << "Error: SphereGeometry can only have one dimension." << endl;
    else
      geom.reset(new SphereGeometry(parameters));
  }
  else  // TODO: more robust error handling
    cerr << "Error: Cannot create manifold type " << type << " using VectorType." << endl;

  return geom;
}

template<>
typename
RiemannianGeometryFactory<MatrixType, MatrixType>::SharedGeometryPtr
RiemannianGeometryFactory<MatrixType, MatrixType>::
create(string type, DimensionList parameters)
{
  SharedGeometryPtr geom;

  if(type.compare("KendallShapeSpace") == 0)
  {
    if(parameters.size() != 2)
      cerr << "Error: KendallShapeSpaceGeometry needs two dimension arguments." << endl;
    else
    {
      if(parameters[0] == 2)
	geom.reset(new KendallShapeSpace2DGeometry(parameters));
      else
	geom.reset(new KendallShapeSpaceGeometry(parameters));
    }
  }
  else if(type.compare("SpecialLinearGroup") == 0)
  {
    if(parameters.size() != 1)
      cerr << "Error: SpecialLinearGroup can only have one dimension parameter."
           << endl;
    else
      geom.reset(new SpecialLinearGroupGeometry(parameters));
  }
  else if(type.compare("Grassmannian") == 0)
  {
    if(parameters.size() != 2)
      cerr << "Error: Grassmannian should have two dimension parameters."
           << endl;
    else
      geom.reset(new GrassmannianGeometry(parameters));
  }
  else if(type.compare("SpecialOrthogonalGroup") == 0)
  {
    if(parameters.size() != 1)
      cerr << "Error: SpecialOrthogonalGroup can only have one dimension parameter."
           << endl;
    else
      geom.reset(new SpecialOrthogonalGroupGeometry(parameters[0]));
  }
  else  // TODO: more robust error handling
    cerr << "Error: Cannot create manifold type " << type << " using MatrixType." << endl;

  return geom;
}
