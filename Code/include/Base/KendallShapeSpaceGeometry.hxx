/*==============================================================================
  File: KendallShapeSpaceGeometry.hxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __KendallShapeSpaceGeometry_hxx
#define __KendallShapeSpaceGeometry_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
struct BasisDataType
{
  PointType p;
  typedef std::vector<TangentType> TangentBasisType;

  TangentBasisType uBasis, ksiBasis, vBasis;
  IndexListType uIndex, ksiIndex, vIndex;

  BasisDataType() : p(), uBasis(), ksiBasis(), vBasis(),
		   uIndex(), ksiIndex(), vIndex() {}

  BasisDataType(const BasisDataType & z)
  {
    p = z.p;
    uBasis = z.uBasis;
    ksiBasis = z.ksiBasis;
    vBasis = z.vBasis;
    uIndex = z.uIndex;
    ksiIndex = z.ksiIndex;
    vIndex = z.vIndex;
  }
};

class KendallShapeSpaceGeometry :
  public RiemannianGeometry<MatrixType, MatrixType>
{
public:

  typedef MatrixType PointType;
  typedef MatrixType TangentType;
  typedef RiemannianGeometry<PointType, TangentType> SuperClass;
  typedef BasisDataType<PointType, TangentType> BasisType;

  typedef std::vector<TangentType> TangentBasisType;
  typedef std::vector<VectorType> VectorListType;

  KendallShapeSpaceGeometry(const DimensionList & _dimParameters) : SuperClass()
  {
    setDimParameters(_dimParameters);
  }

  virtual ~KendallShapeSpaceGeometry() {}

  virtual ScalarType innerProduct(const PointType & base,
                                  const TangentType & v,
                                  const TangentType & w);

  virtual PointType expMap(const PointType & base, const TangentType & t);
  virtual TangentType logMap(const PointType & base, const PointType & p);

  virtual TangentType parallelTranslate(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual TangentType parallelTranslateAtoB(const PointType & a,
                                            const PointType & b,
                                            const TangentType & w);

  virtual void verticalBasis(const PointType & p, TangentBasisType & vertBasis,
                             IndexListType & vertIndex);

  virtual void horizontalBasis(const PointType & p,
                               TangentBasisType & diagBasis,
                               IndexListType & diagIndex,
                               TangentBasisType & horBasis,
                               IndexListType & horIndex);

  virtual void setBasis(PointType p, BasisType * basisDataNew);

  virtual TangentType BasisLieBracket(const PointType & p, VectorType s,
                                      IndexType & index1, IndexType & index2,
				      BasisType * basis);

  virtual TangentType LieBracketVertical(const PointType & p,
					 const TangentType & v,
					 const TangentType & w,
					 BasisType * basis);

  virtual TangentType curvatureTensor(const PointType & base,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w);

  virtual ScalarType sectionalCurvature(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual void jacobiField(const PointType & base, const TangentType & v,
			   TangentType & j, TangentType & jPrime);

  virtual void adjointJacobiField(const PointType & base, const TangentType & v,
				  TangentType & j, TangentType & jPrime);

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 2)
      return;

    dimParameters = _dimParameters;

    dimension = dimParameters[0] * (dimParameters[1] - 1) - 1 -
      (dimParameters[0] * (dimParameters[1] - 1)) / 2;
    extrinsicDimension = dimParameters[0] * dimParameters[1];
    zeroVector.set_size(dimParameters[0], dimParameters[1]);
    zeroVector.fill(0.0);
  }

  virtual void projectTangent(TangentType & vProjected,
                              const PointType & base,
                              const TangentType & v);

  virtual void tangentToList(const TangentType & v, ScalarListType & list);

  virtual void listToTangent(const ScalarListType & list, TangentType & v);

  virtual void pointToList(const PointType & p, ScalarListType & list);

  virtual void listToPoint(const ScalarListType & list, PointType & p);

  // the preshape function
  virtual PointType preshape(PointType & p);
};

#endif
