/*==============================================================================
  File: EuclideanGeometry.hxx

  EuclideanGeometry defines the standard Euclidean metric on R^n.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __EuclideanGeometry_hxx
#define __EuclideanGeometry_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
class EuclideanGeometry : public RiemannianGeometry<PointType, TangentType>
{

};

// Template specialization for VectorType Euclidean (n > 1)
template<>
class EuclideanGeometry<VectorType, VectorType> :
  public RiemannianGeometry<VectorType, VectorType>
{
public:
  typedef VectorType PointType;
  typedef VectorType TangentType;
  typedef RiemannianGeometry<PointType, TangentType> SuperClass;

  EuclideanGeometry(unsigned int _dimension) : SuperClass()
  {
    setDimension(_dimension);
  }

  EuclideanGeometry(const DimensionList & _dimParameters) : SuperClass()
  {
    setDimParameters(_dimParameters);
  }

  virtual ScalarType innerProduct(const PointType & base, const PointType & v,
                                  const PointType & w)
  {
    unsigned int i;
    ScalarType result = 0.0;
    for(i = 0; i < dimension; i++)
      result += v[i] * w[i];
    return result;
  }

  virtual PointType expMap(const PointType & base, const TangentType & v)
  {
    return base + v;
  }

  virtual TangentType logMap(const PointType & base, const PointType & p)
  {
    return p - base;
  }

  virtual TangentType curvatureTensor(const PointType & base,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w)
  {
    return SuperClass::zeroVector;
  }

  virtual ScalarType sectionalCurvature(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w)
  {
    return 0;
  }

  virtual void jacobiField(const PointType & p, const TangentType & v,
                           TangentType & j, TangentType & dj)
  {
    j = j + dj;
    // dj stays constant
  }

  virtual void adjointJacobiField(const PointType & base, const TangentType & v,
                                  TangentType & j, TangentType & dj)
  {
    // j stays constant
    dj = j + dj;
  }

  virtual TangentType parallelTranslate(const PointType & a,
                                        const TangentType & v,
                                        const TangentType & w)
  {
    return w;
  }

  virtual TangentType parallelTranslateAtoB(const PointType & a,
                                            const PointType & b,
                                            const TangentType & w)
  {
    return w;
  }

  virtual void projectTangent(TangentType & vProjected, const PointType & base,
                              const TangentType & v)
  {
    vProjected.set_size(dimension);
    vProjected = v;
  }

  virtual void tangentToList(const TangentType & v, ScalarListType & list)
  {
    unsigned int i;
    list.set_size(dimension);
    for(i = 0; i < dimension; i++)
      list[i] = v[i];
  }

  virtual void listToTangent(const ScalarListType & list, TangentType & v)
  {
    unsigned int i;

    if(list.size() != dimension)
      return;

    v.set_size(dimension);
    for(i = 0; i < dimension; i++)
      v[i] = list[i];
  }

  virtual void setDimension(unsigned int _dimension)
  {
    dimParameters.resize(1);
    dimParameters[0] = _dimension;

    dimension = dimParameters[0];
    extrinsicDimension = dimension;
    zeroVector.set_size(extrinsicDimension);
    zeroVector.fill(0.0);
  }

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 1)
      return;

    setDimension(_dimParameters[0]);
  }

  virtual void pointToList(const PointType & p, ScalarListType & list)
  {
    unsigned int i;
    list.set_size(extrinsicDimension);
    for(i = 0; i < extrinsicDimension; i++)
      list[i] = p[i];
  }

  virtual void listToPoint(const ScalarListType & list, PointType & p)
  {
    unsigned int i;

    if(list.size() != extrinsicDimension)
      return;

    p.set_size(extrinsicDimension);
    for(i = 0; i < extrinsicDimension; i++)
      p[i] = list[i];
  }
};

// Template specialization for ScalarType Euclidean (n = 1)
template<>
class EuclideanGeometry<ScalarType, ScalarType> :
  public RiemannianGeometry<ScalarType, ScalarType>
{
public:
  typedef ScalarType PointType;
  typedef ScalarType TangentType;
  typedef RiemannianGeometry<PointType, TangentType> SuperClass;

  EuclideanGeometry() : SuperClass()
  {
    setDimension(1);
  }

  EuclideanGeometry(const DimensionList & _dimParameters) : SuperClass()
  {
    if(_dimParameters.size() == 1 && _dimParameters[0] == 1)
      setDimension(1);
  }

  virtual ScalarType innerProduct(const PointType & base, const PointType & v,
                                  const PointType & w)
  {
    return v * w;
  }

  virtual PointType expMap(const PointType & base, const TangentType & v)
  {
    return base + v;
  }

  virtual TangentType logMap(const PointType & base, const PointType & p)
  {
    return p - base;
  }

  virtual TangentType curvatureTensor(const PointType & base,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w)
  {
    return SuperClass::zeroVector;
  }

  virtual ScalarType sectionalCurvature(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w)
  {
    return 0;
  }

  virtual void jacobiField(const PointType & p, const TangentType & v,
                           TangentType & j, TangentType & dj)
  {
    j = j + dj;
    // dj stays constant
  }

  virtual void adjointJacobiField(const PointType & base, const TangentType & v,
                                  TangentType & j, TangentType & dj)
  {
    // j stays constant
    dj = j + dj;
  }

  virtual TangentType parallelTranslate(const PointType & a,
                                        const TangentType & v,
                                        const TangentType & w)
  {
    return w;
  }

  virtual TangentType parallelTranslateAtoB(const PointType & a,
                                            const PointType & b,
                                            const TangentType & w)
  {
    return w;
  }

  virtual void projectTangent(TangentType & vProjected, const PointType & base,
                              const TangentType & v)
  {
    vProjected = v;
  }

  virtual void tangentToList(const TangentType & v, ScalarListType & list)
  {
    list.set_size(1);
    list[0] = v;
  }

  virtual void listToTangent(const ScalarListType & list, TangentType & v)
  {
    if(list.size() != 1)
      return;

    v = list[0];
  }

  virtual void setDimension(unsigned int _dimension)
  {
    dimParameters.resize(1);
    dimParameters[0] = _dimension;

    dimension = dimParameters[0];
    extrinsicDimension = dimension;
    zeroVector = 0;
  }

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 1)
      return;

    setDimension(_dimParameters[0]);
  }

  virtual void pointToList(const PointType & p, ScalarListType & list)
  {
    list.set_size(1);
    list[0] = p;
  }

  virtual void listToPoint(const ScalarListType & list, PointType & p)
  {
    if(list.size() != 1)
      return;

    p = list[0];
  }
};

#endif
