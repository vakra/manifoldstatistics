template<class PointType, class TangentType>
typename SasakiGeometry<PointType, TangentType>::
TMPointType SasakiGeometry<PointType, TangentType>::
expMap(const TMPointType & base,
       const TMTangentType & v)
{
  unsigned int i, numberOfSteps;
  double vNorm = sqrt(this->normSquared(base, v));

  if(vNorm < 1.0e-12)
    return base;
  else if(vNorm < 0.01 && vNorm >= 1.0e-12)
    numberOfSteps = 10;
  else if(vNorm < 0.05 && vNorm >= 0.01)
    numberOfSteps = 30;
  else if(vNorm < 0.1 && vNorm >= 0.05)
    numberOfSteps = 60;
  else if(vNorm < 0.5 && vNorm >= 0.1)
    numberOfSteps = 100;
  else
    numberOfSteps = 500;

  double epsilon = 1 / static_cast<ScalarType>(numberOfSteps);

  unsigned int exDim = geom->getExtrinsicDimension();

  // Initializations
  PointType oldPoint = base.p, newPoint = base.p;
  TangentType oldU = base.u, newU = base.u;

  TangentType oldX = v.x, newX = v.x;
  TangentType oldY = v.y, newY = v.y;

  // Derivatives needed for the Geodesic equation: DX / dt
  TangentType changeX = v.x;

  for(i = 0; i < numberOfSteps; i++)
  {
    // Updating the M-point
    newPoint = geom->expMap(oldPoint, oldX * epsilon);

    changeX = -geom->curvatureTensor(oldPoint, oldU, oldY, oldX);
    newX = oldX + changeX * epsilon;

    // Updating X: parallel translating X to the next point
    newX = geom->parallelTranslateAtoB(oldPoint, newPoint, newX);

    // Updating U
    newU = geom->parallelTranslateAtoB(oldPoint, newPoint,
                                       oldU + oldY * epsilon);

    // Updating Y
    newY = geom->parallelTranslateAtoB(oldPoint, newPoint, oldY);

    // Re-initializing
    oldPoint = newPoint;
    oldU = newU;
    oldX = newX;
    oldY = newY;
  }

  TMPointType target;
  target.p = newPoint;
  target.u = newU;

  return target;
}

template<class PointType, class TangentType>
typename SasakiGeometry<PointType, TangentType>::
TMTangentType SasakiGeometry<PointType, TangentType>::
logMap(const TMPointType & base,
       const TMPointType & target)
{
  unsigned int numberOfSteps = 10;
  double epsilon = 1 / static_cast<ScalarType>(numberOfSteps);

  unsigned int exDim = geom->getExtrinsicDimension();

  // Initializations
  PointType startPoint, finalPoint;
  TangentType startU, finalU;
  startPoint = base.p; startU = base.u;
  finalPoint = target.p; finalU = target.u;

  TangentType v = geom->logMap(startPoint, finalPoint); // Initial Tangent
  TangentType oldX = v, newX = v;
  TangentType oldY = v, newY = v;

  std::vector<PointType> pointList(numberOfSteps + 1);
  std::vector<TangentType> uList(numberOfSteps + 1);

  unsigned int currentStep = 0; ScalarType timePoint = 0.0;
  PointType currentPoint;
  TangentType currentU;
  TangentType currentX, currentY;

  TangentType leftFwd, leftBack;
  TangentType rightFwd, rightBack;
  TangentType leftParallel;
  TangentType rightParallel;

  for(currentStep = 0; currentStep < numberOfSteps + 1; currentStep++)
  {
    timePoint =  currentStep * epsilon;
    currentPoint = geom->expMap(startPoint, v * timePoint);

    leftParallel = geom->parallelTranslateAtoB(startPoint,
                                               currentPoint, startU);
    rightParallel = geom->parallelTranslateAtoB(finalPoint,
                                                currentPoint, finalU);

    currentU = leftParallel * (1 - timePoint) + rightParallel * timePoint;

    pointList[currentStep] = currentPoint;
    uList[currentStep] = currentU;
  }

  // ******************** GRADIENT DESCENT ***************************
  // Calculating gradients with respect to the point and the tangent and
  // changing the lists accordingly to have minimum energy. The ultimate XList
  // will be representative of our logMap on the Tangent Bundle.  The sum of
  // lengths of tangents in XList will be our path length which we hope to
  // minimize and make geodesic under the Sasaki metric.

  // New Lists for gradients
  std::vector<TangentType> gradPList(numberOfSteps - 1);
  std::vector<TangentType> gradUList(numberOfSteps - 1);

  // Gradient computation necessities
  TangentType changeInX, changeInY;
  TangentType gradP, gradU;
  TangentType  curvatureTangent;

  TangentType leftU, rightU;
  TangentType leftX, rightX;
  TangentType leftY, rightY;
  PointType leftPoint, rightPoint;
  TangentType leftMidPoint;
  TangentType rightMidPoint;
  PointType newCurrentPoint;
  TangentType newCurrentU;
  TangentType temp;
  ScalarType changeInXNorm, changeInYNorm;

  // Initializations

  ScalarType diffEnergy = 1.0, energy = 0.0, diffSum = 1.0;

  // Log Files
  //ofstream en("BasicResults/en.txt");
  //ofstream grad("BasicResults/grad.txt");

  // Gradient descent:
  unsigned int loopCount = 0;
  while(diffSum > 1.0e-6 && loopCount < 50)
  {
    gradP.fill(0.0); gradU.fill(0.0); diffSum = 0.0;
    double oldEnergy = energy;
    energy = 0.0;
    loopCount++;

    for(currentStep = 1 ; currentStep < numberOfSteps; currentStep++)
    {
      leftPoint = pointList[currentStep - 1];
      currentPoint = pointList[currentStep];
      rightPoint = pointList[currentStep + 1];

      leftU = uList[currentStep - 1];
      currentU = uList[currentStep];
      rightU = uList[currentStep + 1];

      // Finding X and its derivative

      leftFwd = geom->logMap(leftPoint, currentPoint);
      leftBack = geom->logMap(currentPoint, leftPoint);
      rightFwd = geom->logMap(rightPoint, currentPoint);
      rightBack = geom->logMap(currentPoint, rightPoint);

      leftX = leftBack / epsilon;
      rightX = rightBack / epsilon;

      currentX = (rightX - leftX) * 0.5;

      // DX / dt
      changeInX = (rightX + leftX) / epsilon;

      changeInXNorm = geom->norm(currentPoint, changeInX);

      // Finding Y
      leftParallel = geom->parallelTranslateAtoB(leftPoint,
                                                 currentPoint,
                                                 leftU);
      rightParallel = geom->parallelTranslateAtoB(rightPoint,
                                                  currentPoint,
                                                  rightU);
      currentY = (rightParallel - leftParallel) / (2.0 * epsilon);

      // Finding derivative of Y: DY / dt
      leftMidPoint = geom->expMap(leftPoint, leftFwd * 0.5);
      rightMidPoint = geom->expMap(currentPoint, rightBack * 0.5);

      leftY = (geom->parallelTranslateAtoB(currentPoint,
                                           leftMidPoint,
                                           currentU) -
               geom->parallelTranslateAtoB(leftPoint,
                                           leftMidPoint,
                                           leftU)) / epsilon;

      rightY = (geom->parallelTranslateAtoB(rightPoint,
                                            rightMidPoint,
                                            rightU) -
                geom->parallelTranslateAtoB(currentPoint,
                                            rightMidPoint,
                                            currentU)) / epsilon;

      leftY = geom->parallelTranslateAtoB(leftMidPoint, currentPoint, leftY);
      rightY = geom->parallelTranslateAtoB(rightMidPoint, currentPoint, rightY);

      changeInY = (rightY - leftY) / epsilon;

      changeInYNorm = geom->norm(currentPoint, changeInY);

      // Curvature
      curvatureTangent = geom->curvatureTensor(currentPoint, currentU,
                                               currentY, currentX);

      // Computing negative gradients
      gradP = changeInX + curvatureTangent;
      gradU = changeInY;

      gradPList[currentStep-1] = gradP;
      gradUList[currentStep-1] = gradU;

      // Energy and gradient norms
      energy +=  ((geom->normSquared(currentPoint, currentX)) +
                  (geom->normSquared(currentPoint, currentY))) * epsilon;
      diffSum += ((geom->normSquared(currentPoint, gradP)) +
                  (geom->normSquared(currentPoint, gradU)));
    }

    // writing it to file
    // en << energy << endl;
    // grad << diffSum << endl;

    // Is the gradient norm decreasing?
    diffEnergy = energy - oldEnergy;

    // Updating the point and tangent lists

    ScalarType delta = 3e-3;

    for(currentStep = 1; currentStep < numberOfSteps; currentStep++)
    {
      currentPoint = pointList[currentStep];
      currentU = uList[currentStep];

      gradP = gradPList[currentStep - 1];
      gradU = gradUList[currentStep - 1];

      newCurrentPoint = geom->expMap(currentPoint, gradP * delta);
      pointList[currentStep] = newCurrentPoint;

      newCurrentU = geom->parallelTranslateAtoB(currentPoint, newCurrentPoint,
                                                currentU + gradU * delta);
      uList[currentStep] = newCurrentU;
    }
  }

  // en.close();
  // grad.close();

  TangentType startX = (geom->logMap(startPoint, pointList[1])) / epsilon;

  TangentType uParallel = geom->parallelTranslateAtoB(pointList[1],
                                                      startPoint, uList[1]);
  TangentType startY = (uParallel - startU) / epsilon;
  TMTangentType tangent, startTangent;
  tangent.x = startX;
  tangent.y = startY;

  projectTangent(startTangent, base, tangent);
  return startTangent;
}

template<class PointType, class TangentType>
typename SasakiGeometry<PointType, TangentType>::
TMTangentType SasakiGeometry<PointType, TangentType>::
parallelTranslate(const TMPointType & a,
		  const TMTangentType & v,
                  const TMTangentType & w)
{
  TMTangentType wTrans, wTransProjected;
  unsigned int i, numberOfSteps;
  double vNorm = sqrt(this->normSquared(a, v));

  if(vNorm < 1.0e-12)
    return w;
  else if(vNorm < 0.01 && vNorm >= 1.0e-12)
    numberOfSteps = 10;
  else if(vNorm < 0.05 && vNorm >= 0.01)
    numberOfSteps = 30;
  else if(vNorm < 0.1 && vNorm >= 0.05)
    numberOfSteps = 60;
  else if(vNorm < 0.5 && vNorm >= 0.1)
    numberOfSteps = 100;
  else
    numberOfSteps = 500;

  double epsilon = 1 / static_cast<ScalarType>(numberOfSteps);

  unsigned int exDim = geom->getExtrinsicDimension();

  PointType oldPoint, newPoint;
  TangentType oldU, newU, oldX, newX;
  TangentType oldY, newY;
  TMPointType tempSasakiPoint;

  // Initializing
  oldPoint = a.p;
  oldU = a.u;
  oldX = v.x;
  oldY = v.y;
  wTrans = w;

  for(i = 0; i < numberOfSteps; i++)
  {
    // Updating the M-point
    newPoint = geom->expMap(oldPoint, oldX * epsilon);

    // Updating wTrans via the parallel translation relations on TM
    wTrans.x += (geom->curvatureTensor(oldPoint, oldU, wTrans.y, oldX) +
                 geom->curvatureTensor(oldPoint, oldU,
                                       oldY, wTrans.x)) * (-0.5) * epsilon;
    wTrans.x = geom->parallelTranslate(oldPoint, oldX * epsilon, wTrans.x);

    wTrans.y += geom->curvatureTensor(oldPoint, oldX,
                                      wTrans.x, oldU) * 0.5 * epsilon;
    wTrans.y = geom->parallelTranslate(oldPoint, oldX * epsilon, wTrans.y);

    // Updating X & parallel translating X to the next point
    newX = oldX - geom->curvatureTensor(oldPoint, oldU, oldY, oldX) * epsilon;
    newX = geom->parallelTranslate(oldPoint, oldX * epsilon, newX);

    // Updating U
    newU = geom->parallelTranslate(oldPoint, oldX * epsilon,
                                   oldU + oldY * epsilon);
    // Updating Y
    newY = geom->parallelTranslate(oldPoint, oldX * epsilon, oldY);

    // Projecting wTrans w.r.t (newPoint, newU)
    tempSasakiPoint.p = newPoint;
    tempSasakiPoint.u = newU;

    (this->projectTangent(wTransProjected, tempSasakiPoint, wTrans));

    // Re-initializing
    oldPoint = newPoint;
    oldU = newU;
    oldX = newX;
    oldY = newY;
    wTrans = wTransProjected;
  }
  return wTransProjected;
}
