/*==============================================================================
  File: SpecialLinearGroupGeometry.hxx

  This class defines SL(n, R), the special linear group of n x n real matrices
  with determinant one. Uses the standard left- or right-invariant metric
  defined via the Killing form.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __SpecialLinearGroupGeometry_hxx
#define __SpecialLinearGroupGeometry_hxx

#include "LieGroupGeometry.hxx"
#include "MatrixExponential.hxx"

class SpecialLinearGroupGeometry : public LieGroupGeometry<MatrixType, MatrixType>
{
public:
  typedef MatrixType PointType;
  typedef MatrixType TangentType;
  typedef LieGroupGeometry<PointType, TangentType> SuperClass;

  SpecialLinearGroupGeometry(unsigned int n,
			     MatrixExponential * _mExp = NULL,
			     bool _leftInvariant = true,
                             ScalarType _stepSize = 0.01) :
    SuperClass(_leftInvariant)
  {
    DimensionList dList;
    dList.resize(1);
    dList[0] = n;
    setDimParameters(dList);
    stepSize = _stepSize;
    mExp = _mExp;
  }

  SpecialLinearGroupGeometry(DimensionList dims,
			     MatrixExponential * _mExp = NULL,
			     bool _leftInvariant = true,
                             ScalarType _stepSize = 0.01) :
    SuperClass(_leftInvariant)
  {
    setDimParameters(dims);
    stepSize = _stepSize;
    mExp = _mExp;
  }

  ScalarType getStepSize() { return stepSize; }
  void setStepSize(ScalarType _stepSize) { stepSize = _stepSize; }

  // Inner product in the Lie algebra
  virtual ScalarType innerProductAtId(const TangentType & v,
                                      const TangentType & w)
  {
    return trace(trans(v) * w);
  }

  virtual PointType inverse(const PointType & p) { return inv(p); }

  // Left multiplication of a point q by the point p
  virtual PointType leftMultiply(const PointType & p, const PointType & q)
  {
    return p * q;
  }

  // Right multiplication of a point q by the point p
  virtual PointType rightMultiply(const PointType & p, const PointType & q)
  {
    return q * p;
  }

  // Left translation by p of a tangent vector v in Lie algebra
  // Returned tangent vector is in tangent space of p
  virtual TangentType leftTranslate(const PointType & p, const TangentType & v)
  {
    return p * v;
  }

  // Right translation by p of a tangent vector v in Lie algebra
  // Returned tangent vector is in tangent space of p
  virtual TangentType rightTranslate(const PointType & p, const TangentType & v)
  {
    return v * p;
  }

  // Computes the exponential map starting at the identity
  // The initial velocity v is an element of the Lie algebra
  virtual PointType expMapAtId(const TangentType & v);

  // Computes log map from id to q. Returns the initial velocity of the geodesic
  // segment, which is an element of the Lie algebra.
  virtual TangentType logMapAtId(const PointType & q);

  // Parallel translation of a velocity v along the geodesic starting at identity
  // with initial velocity v, defined in the Lie algebra
  virtual TangentType parallelTranslateTangentAtId(const TangentType & v,
                                                   ScalarType s = 1.0);

  // Parallel translation of w along geodesic starting at the identity with
  // initial velocity v, defined in the Lie algebra.
  virtual TangentType parallelTranslateAtId(const TangentType & v,
                                            const TangentType & w);

  // Gives the Jacobi field
  virtual void jacobiFieldAtId(const TangentType & v, TangentType & j,
                               TangentType & dj);

  // Gives the adjoint Jacobi field
  virtual void adjointJacobiFieldAtId(const TangentType & v, TangentType & j,
                                      TangentType & dj);

  // Takes a tangent v, and projects to the horizontal tangent vector at base,
  // returning vProjected
  virtual void projectTangent(TangentType & vProjected, const PointType & p,
                              const TangentType & v);

  virtual void tangentToList(const TangentType & v, ScalarListType & list)
  {
    list.resize(extrinsicDimension);
    unsigned int i, j, k = 0;
    for(j = 0; j < dimParameters[0]; j++)
    {
      for(i = 0; i < dimParameters[0]; i++)
      {
        list(k) = v(i, j);
        k++;
      }
    }
  }

  virtual void listToTangent(const ScalarListType & list, TangentType & v)
  {
    v = list;
    v.reshape(dimParameters[0], dimParameters[0]);
  }

  virtual void pointToList(const PointType & p, ScalarListType & list)
  {
    list.resize(extrinsicDimension);
    unsigned int i, j, k = 0;
    for(j = 0; j < dimParameters[0]; j++)
    {
      for(i = 0; i < dimParameters[0]; i++)
      {
        list(k) = p(i, j);
        k++;
      }
    }
  }

  virtual void listToPoint(const ScalarListType & list, PointType & p)
  {
    p = list;
    p.reshape(dimParameters[0], dimParameters[0]);
  }

  virtual TangentType adjointActionTranspose(const PointType & p,
                                             const TangentType & v)
  {
    return adjointAction(trans(p), v);
  }

  virtual TangentType ad(const TangentType & v, const TangentType & w)
  {
    if(leftInvariant)
      return v * w - w * v;
    else
      return w * v - v * w;
  }

  virtual TangentType adTranspose(const TangentType & v, const TangentType & w)
  {
    if(leftInvariant)
      return trans(v) * w - w * trans(v);
    else
      return w * trans(v) - trans(v) * w;
  }

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 1)
      return;

    dimParameters = _dimParameters;
    unsigned int n = dimParameters[0];

    extrinsicDimension = n * n;
    dimension = extrinsicDimension - 1;
    zeroVector = arma::zeros<arma::mat>(n, n);

    id.eye(n, n);
  }

protected:
  ScalarType stepSize;
  MatrixExponential * mExp;
};

#endif
