/*==============================================================================
  File: KendallShapeSpace2DGeometry.hxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __KendallShapeSpace2DGeometry_hxx
#define __KendallShapeSpace2DGeometry_hxx

#include "KendallShapeSpaceGeometry.hxx"

class KendallShapeSpace2DGeometry : public KendallShapeSpaceGeometry
{
public:
  typedef KendallShapeSpaceGeometry SuperClass;
  typedef SuperClass::PointType PointType;
  typedef SuperClass::TangentType TangentType;

  KendallShapeSpace2DGeometry(const DimensionList & _dimParameters) :
    SuperClass(_dimParameters)
  {
    setDimParameters(_dimParameters);
  }

  virtual ~KendallShapeSpace2DGeometry() {}

  virtual ScalarType innerProduct(const PointType & base,
                                  const TangentType & v,
                                  const TangentType & w);

  virtual PointType expMap(const PointType & base, const TangentType & t);
  virtual TangentType logMap(const PointType & base, const PointType & p);

  virtual TangentType parallelTranslate(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual TangentType parallelTranslateAtoB(const PointType & a,
                                            const PointType & b,
                                            const TangentType & w);

  virtual void jacobiField(const PointType & base, const TangentType & v,
			   TangentType & j, TangentType & jPrime);

  virtual void adjointJacobiField(const PointType & base, const TangentType & v,
				  TangentType & j, TangentType & jPrime);

  virtual TangentType curvatureTensor(const PointType & base,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w);

  virtual ScalarType sectionalCurvature(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 2 || _dimParameters[0] != 2)
      return;

    dimParameters[0] = 2;
    dimParameters[1] = _dimParameters[1];

    dimension = 2 * dimParameters[1] - 4;
    extrinsicDimension = 2 * dimParameters[1];
    zeroVector.set_size(2, dimParameters[1]);
    zeroVector.fill(0.0);
  }

  virtual void projectTangent(TangentType & vProjected,
                              const PointType & base,
                              const TangentType & v);

  virtual void tangentToList(const TangentType & v,
                             ScalarListType & list);

  virtual void listToTangent(const ScalarListType & list,
                             TangentType & v);

  virtual void pointToList(const PointType & p,
			   ScalarListType & list);

  virtual void listToPoint(const ScalarListType & list,
			   PointType & p);

  // the preshape function
  virtual PointType preshape(const PointType & p);
};

#endif
