/*==============================================================================
  File: ManifoldPrimitives.hxx

  Typedefs for point and tangent primitives on a manifold, and for structures
  that contain them.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __ManifoldPrimitives_hxx
#define __ManifoldPrimitives_hxx

#include <vector>
#include <armadillo>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <memory>

using namespace arma;
using namespace std;

// basic primitives
typedef double ScalarType;

typedef Col<ScalarType> ScalarListType;
typedef vector<unsigned int> DimensionList;
typedef Mat<ScalarType> MatrixType;
typedef Col<ScalarType> VectorType;

typedef Col<unsigned int> IndexType;
typedef vector<IndexType> IndexListType;

#endif
