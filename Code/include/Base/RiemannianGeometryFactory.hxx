/*==============================================================================
  File: RiemannianGeometryFactory.hxx

  RiemannianGeometryFactory is a factory method for creating RiemannianGeometry
  objects. It requires the type and the dimension of the concrete manifold to
  build. The dimension may be a list of integers. For example, in the case of
  the Stiefel manifold, V_k(R^n) = O(n) / O(n - k), there are two integers
  specified.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __RiemannianGeometryFactory_hxx
#define __RiemannianGeometryFactory_hxx

#include "MFDContainer.hxx"
#include "EuclideanGeometry.hxx"
#include "SphereGeometry.hxx"
#include "KendallShapeSpaceGeometry.hxx"
#include "KendallShapeSpace2DGeometry.hxx"
#include "SpecialLinearGroupGeometry.hxx"
#include "SpecialOrthogonalGroupGeometry.hxx"
#include "GrassmannianGeometry.hxx"
#include "SasakiGeometry.hxx"

template<class PointType, class TangentType>
class RiemannianGeometryFactory
{
public:

  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef std::shared_ptr<GeometryType> SharedGeometryPtr;

  RiemannianGeometryFactory() {}
  virtual ~RiemannianGeometryFactory() {}

  SharedGeometryPtr create(std::string type, DimensionList dimensions);

  SharedGeometryPtr create(std::string type, unsigned int dimension)
  {
    DimensionList dList(1);
    dList[0] = dimension;

    return create(type, dList);
  }

  SharedGeometryPtr create(std::shared_ptr<MFDContainer> mfd)
  {
    return create(mfd->getManifoldType(), mfd->getDimParameters());
  }
};

#include "RiemannianGeometryFactory.txx"

#endif
