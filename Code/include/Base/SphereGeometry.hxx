/*==============================================================================
  File: SphereGeometry.hxx

  SphereGeometry defines the standard metric on the unit n-sphere. Note that the
  PointType is a vector in R^(n+1) -- these vectors should be unit length, and
  this is not explicitly checked in this class. Also, tangent vectors should be
  perpendicular to the base point. These constraints should be enforced by code
  that uses this class, although the expMap function will renormalize the result
  to prevent numerical problems.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __SphereGeometry_hxx
#define __SphereGeometry_hxx

#include "RiemannianGeometry.hxx"

class SphereGeometry : public RiemannianGeometry<VectorType, VectorType>
{
public:
  typedef VectorType PointType;
  typedef VectorType TangentType;
  typedef RiemannianGeometry<PointType, TangentType> SuperClass;

  SphereGeometry(unsigned int _dimension) : SuperClass()
  {
    setDimension(_dimension);
  }

  SphereGeometry(const DimensionList _dimParameters) : SuperClass()
  {
    setDimParameters(_dimParameters);
  }

  virtual ScalarType innerProduct(const PointType & base,
                                  const TangentType & v,
                                  const TangentType & w);

  virtual PointType expMap(const PointType & base, const TangentType & t);
  virtual TangentType logMap(const PointType & base, const PointType & p);

  virtual TangentType parallelTranslate(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual TangentType parallelTranslateAtoB(const PointType & a,
                                            const PointType & b,
                                            const TangentType & w);

  virtual TangentType curvatureTensor(const PointType & base,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w);

  virtual ScalarType sectionalCurvature(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual void jacobiField(const PointType & base, const TangentType & v,
                           TangentType & j, TangentType & dj);

  virtual void adjointJacobiField(const PointType & base, const TangentType & v,
                                  TangentType & j, TangentType & dj);

  virtual void projectTangent(TangentType & vProjected,
                              const PointType & base,
                              const TangentType & v);

  virtual void tangentToList(const TangentType & v, ScalarListType & list);

  virtual void listToTangent(const ScalarListType & list, TangentType & v);

  virtual void pointToList(const PointType & p, ScalarListType & list);

  virtual void listToPoint(const ScalarListType & list, PointType & p);

  virtual void setDimension(unsigned int _dimension)
  {
    dimParameters.resize(1);
    dimParameters[0] = _dimension;

    dimension = dimParameters[0];
    extrinsicDimension = dimension + 1;
    zeroVector.set_size(extrinsicDimension);
    zeroVector.fill(0.0);
  }

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 1)
      return;

    setDimension(_dimParameters[0]);
  }
};

#endif
