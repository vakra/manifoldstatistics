/*==============================================================================
  File: SpecialOrthogonalGroupGeometry.hxx

  This class implements the special orthogonal group, SO(n), i.e., the space of
  nxn rotation matrices. The Lie algebra is the space of skew-symmetric
  matrices.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/
#ifndef __SpecialOrthogonalGroupGeometry_hxx
#define __SpecialOrthogonalGroupGeometry_hxx

#include "LieGroupGeometry.hxx"
#include "MatrixExponential.hxx"

class SpecialOrthogonalGroupGeometry : public LieGroupGeometry<MatrixType, MatrixType>
{
public:
  typedef MatrixType PointType;
  typedef MatrixType TangentType;
  typedef LieGroupGeometry<PointType, TangentType> SuperClass;

  SpecialOrthogonalGroupGeometry(unsigned int n,
                                 MatrixExponential * _mExp = NULL) // : LieGroupGeometry()
  {
    DimensionList dList;
    dList.resize(1);
    dList[0] = n;
    setDimParameters(dList);
    mExp = _mExp;
  }
  virtual ~SpecialOrthogonalGroupGeometry() {}

  virtual PointType inverse(const PointType & p)
  {
    return trans(p);
  }

  // Factor of 1/2 makes norm = rotation angle in SO(2) and SO(3)
  virtual ScalarType innerProductAtId(const TangentType & v,
                                      const TangentType & w)
  {
    return 0.5 * trace(v * trans(w));
  }

  // Left multiplication of a point q by the point p
  virtual PointType leftMultiply(const PointType & p, const PointType & q)
  {
    return p * q;
  }

  // Right multiplication of a point q by the point p
  virtual PointType rightMultiply(const PointType & p, const PointType & q)
  {
    return q * p;
  }

  // Left translation by p of a tangent vector v in Lie algebra
  // Returned tangent vector is in tangent space of p
  virtual TangentType
    leftTranslate(const PointType & p, const TangentType & v)
  {
    return p * v;
  }

  // Right translation by p of a tangent vector v in Lie algebra
  // Returned tangent vector is in tangent space of p
  virtual TangentType
    rightTranslate(const PointType & p, const TangentType & v)
  {
    return v * p;
  }

  // Computes the exponential map starting at the identity
  // The initial velocity v is an element of the Lie algebra
  virtual PointType expMapAtId(const TangentType & v)
  {
    return mExp->skewMatrixExp(v);
  }

  // Computes log map from id to q. Returns the initial velocity of the geodesic
  // segment, which is an element of the Lie algebra.
  virtual TangentType logMapAtId(const PointType & q)
  {
    return mExp->rotationMatrixLog(q);
  }

  // Adjoint action, aka, "Big" Ad operator (v is in Lie algebra)
  virtual TangentType adjointAction(const PointType & p, const TangentType & v)
  {
    return p * v * trans(p);
  }

  // Transpose of Ad (v is in Lie algebra)
  virtual TangentType adjointActionTranspose(const PointType & p,
                                             const TangentType & v)
  {
    return trans(p) * v * p;
  }

  // Little ad operator, a.k.a. Lie bracket (v, w are in Lie algebra)
  virtual TangentType ad(const TangentType & v, const TangentType & w)
  {
    return v * w - w * v;
  }

  // Transpose of ad operator (v, w are in Lie algebra)
  virtual TangentType
    adTranspose(const TangentType & v, const TangentType & w)
  {
    return -v * w + w * v;
  }

  // The symmetric product of two vectors: sym_v w = -ad_v^T w - ad_w^T v
  virtual TangentType sym(const TangentType & v, const TangentType & w)
  {
    return zeroVector;
  }

  // Transpose of the symmetric product: sym_v^T w = -ad_v w + ad_w^T v
  virtual TangentType symTranspose(const TangentType & v, const TangentType & w)
  {
    return zeroVector;
  }

  // Affine connection for two invariant vector fields (v, w are in Lie algebra)
  virtual TangentType connectionAtId(const TangentType & v,
                                     const TangentType & w)
  {
    return 0.5 * ad(v, w);
  }

  // Parallel translation of w along geodesic starting at the identity with
  // initial velocity v, defined in Lie algebra.
  virtual TangentType parallelTranslateAtId(const TangentType & v,
                                            const TangentType & w)
  {
    PointType g = expMapAtId(0.5 * v);

    return trans(g) * w * g;
  }

  // Riemannian curvature tensor at identity (u, v, w are in Lie algebra)
  virtual TangentType curvatureTensorAtId(const TangentType & u,
                                          const TangentType & v,
                                          const TangentType & w)
  {
    return 0.25 * (adTranspose(u, ad(v, w)) - adTranspose(v, ad(u, w)));
  }

  // Gives the sectional curvature at a point
  virtual ScalarType sectionalCurvatureAtId(const TangentType & v,
                                            const TangentType & w)
  {
    ScalarType vw = innerProductAtId(v, w);
    ScalarType denom = normSquaredAtId(v) * normSquaredAtId(w) - vw * vw;
    if(denom > 1e-12)
      return 0.25 * normSquaredAtId(ad(v, w)) / denom;
    else
      return 0.0;
  }

  // Gives the Jacobi field at time 1 along a geodesic
  // id, v is the intial position and velocity of the geodesic at t=0
  // j, dj are the initial Jacobi field and its derivative at t=0
  // j, dj are overwritten with the return values at t=1
  // All vectors are in the Lie algebra
  virtual void jacobiFieldAtId(const TangentType & v, TangentType & j,
                               TangentType & dj);

  // Gives the adjoint Jacobi field at time 0 along a geodesic
  // id, v are initial position and (backward) velocity of the geodesic at t=1
  // j, dj are the intial Jacobi field and its derivative at t=1
  // j, dj are overwritten with the return values at t=0
  // All vectors are in the Lie algebra
  virtual void adjointJacobiFieldAtId(const TangentType & v,
                                      TangentType & j, TangentType & dj);

  // Takes a tangent v, and projects to the horizontal tangent vector at p,
  // returning vProjected
  virtual void projectTangent(TangentType & vProjected,
                              const PointType & p,
                              const TangentType & v)
  {
    vProjected = translate(inverse(p), v);
    vProjected = 0.5 * (vProjected - trans(vProjected));
    vProjected = translate(p, vProjected);
  }

  virtual void tangentToList(const TangentType & v, ScalarListType & list)
  {
    list.resize(extrinsicDimension);
    unsigned int i, j, k = 0;
    for(j = 0; j < dimParameters[0]; j++)
    {
      for(i = 0; i < dimParameters[0]; i++)
      {
        list(k) = v(i, j);
        k++;
      }
    }
  }

  virtual void listToTangent(const ScalarListType & list, TangentType & v)
  {
    v = list;
    v.reshape(dimParameters[0], dimParameters[0]);
  }

  virtual void pointToList(const PointType & p, ScalarListType & list)
  {
    list.resize(extrinsicDimension);
    unsigned int i, j, k = 0;
    for(j = 0; j < dimParameters[0]; j++)
    {
      for(i = 0; i < dimParameters[0]; i++)
      {
        list(k) = p(i, j);
        k++;
      }
    }
  }

  virtual void listToPoint(const ScalarListType & list, PointType & p)
  {
    p = list;
    p.reshape(dimParameters[0], dimParameters[0]);
  }

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 1)
      return;

    dimParameters = _dimParameters;
    unsigned int n = dimParameters[0];

    dimension = (n * (n - 1)) / 2;
    extrinsicDimension = n * n;

    id = arma::eye<arma::mat>(n, n);
    zeroVector.resize(n, n);
    zeroVector.fill(0.0);
  }

protected:
  MatrixExponential * mExp;

  // Helper functions for Jacobi fields
  ScalarType scalarJacobiFunction(ScalarType t, ScalarType a, ScalarType b)
  {
    if(t > 1.0e-10)
      return cos(t) * a + (1.0 / t) * sin(t) * b;
    else
      return a + b;
  }

  ScalarType dScalarJacobiFunction(ScalarType t, ScalarType a, ScalarType b)
  {
    return -t * sin(t) * a + cos(t) * b;
  }

  ScalarType scalarAdjointJacobiFunction(ScalarType t, ScalarType a,
                                         ScalarType b)
  {
    return cos(t) * a - t * sin(t) * b;
  }

  ScalarType dScalarAdjointJacobiFunction(ScalarType t, ScalarType a,
                                          ScalarType b)
  {
    if(t > 1.0e-10)
      return (1.0 / t) * sin(t) * a + cos(t) * b;
    else
      return a + b;
  }
};

#endif
