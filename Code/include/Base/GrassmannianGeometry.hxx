/*=============================================================================
  File: GrassmannianGeometry.hxx
  =========================================================================== */

#ifndef __GrassmannianGeometry_hxx
#define __GrassmannianGeometry_hxx

#include "RiemannianGeometry.hxx"

class GrassmannianGeometry :
  public RiemannianGeometry<MatrixType, MatrixType>
{
public:
  typedef MatrixType PointType;
  typedef MatrixType TangentType;
  typedef RiemannianGeometry<PointType, TangentType> SuperClass;

  GrassmannianGeometry(const DimensionList & _dimParameters) : SuperClass()
  {
    setDimParameters(_dimParameters);
  }

  virtual ~GrassmannianGeometry() {}

  virtual ScalarType innerProduct(const PointType & base,
                                  const TangentType & v,
                                  const TangentType & w);

  virtual PointType expMap(const PointType & base, const TangentType & t);
  virtual TangentType logMap(const PointType & base, const PointType & p);

  virtual TangentType parallelTranslate(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual TangentType parallelTranslateAtoB(const PointType & a,
                                            const PointType & b,
                                            const TangentType & w);

  virtual TangentType curvatureTensor(const PointType & base,
                                      const TangentType & u,
                                      const TangentType & v,
                                      const TangentType & w);

  virtual ScalarType sectionalCurvature(const PointType & base,
                                        const TangentType & v,
                                        const TangentType & w);

  virtual void jacobiField(const PointType & base,
			   const TangentType & v,
			   TangentType & j,
			   TangentType & jPrime);

  virtual void adjointJacobiField(const PointType & base,
				  const TangentType & v,
				  TangentType & j,
				  TangentType & jPrime);

  virtual void setDimParameters(const DimensionList & _dimParameters)
  {
    if(_dimParameters.size() != 2)
      return;

    dimParameters = _dimParameters;

    dimension = dimParameters[1] * (dimParameters[0] - dimParameters[1]);
    extrinsicDimension = dimParameters[0] * dimParameters[1];
    zeroVector.set_size(dimParameters[0], dimParameters[1]);
    zeroVector.fill(0.0);
  }

  virtual void projectTangent(TangentType & vProjected,
                              const PointType & base,
                              const TangentType & v);

  virtual void tangentToList(const TangentType & v,
                             ScalarListType & list);

  virtual void listToTangent(const ScalarListType & list,
                             TangentType & v);

  virtual void pointToList(const PointType & p,
			   ScalarListType & list);

  virtual void listToPoint(const ScalarListType & list,
			   PointType & p);

  // the preshape function
  virtual PointType preshape(PointType & p);
};

#endif
