// A is an input matrix with each col normalized
// Q is an output matrix after GramSchmidt process

template<class PointType, class TangentType>
void ProbabilisticPGA<PointType, TangentType>::
GramSchmidt(MatrixType A, MatrixType & Q)
{
  unsigned int m = A.n_rows, n = A.n_cols;
  MatrixType v, R(m, n);
  R.fill(0.0);

  for (int j = 0; j < n; j++)
    {
      v = A.col(j);
      for (int i = 0; i <= j-1; i++)
	{
	  R(i, j) = dot(Q.col(i), A.col(j));
	  v = v - R(i, j) * Q.col(i);
	}
      R(j, j) = norm(v, "fro");
      Q.col(j) = v / R(j, j);
    }
}

//==========================================================================
// Generate random mutually orthogonal matrix
// A is a square matrix

template<class PointType, class TangentType>
void ProbabilisticPGA<PointType, TangentType>::
randOrthoMat(MatrixType & A)
{
  MatrixType ret;
  while(det(A) == 0.0)
    {A.randn();}
  ret = A;
  GramSchmidt(ret, A);
}

//==========================================================================
// Function for calculating unit length sphere area

template<class PointType, class TangentType>
double ProbabilisticPGA<PointType, TangentType>::
sphereArea(int dim)
{
  const double pi = 3.1415926535897931;
  double s0 = 2.0, s1 = 2*pi, area;
  if (dim == 0)
    return(s0);
  else if (dim == 1)
    return (s1);
  else
  {
    area = 2 * pi / (dim - 1) * sphereArea(dim - 2);
    return(area);
  }
}

//===========================================================================

template<class PointType, class TangentType>
VectorType ProbabilisticPGA<PointType, TangentType>::
integration(double tau)
{
  unsigned int steps = 10000;
  unsigned int dim = geom->getDimension();
  std::string type = typeid(*geom).name();
  type = type.substr(2, type.length());

  VectorType ret(2);
  double r, constNormal;
  double sumconstNormal = 0.0, sumconstDev = 0.0;
  const double pi = 3.1415926535897931;

  // using midpoint rule
  for (unsigned int i = 1; i <= steps; i++)
  {
    if(type.compare("SphereGeometry") == 0)
    {
      r = pi * (i - 0.5) / steps;
      constNormal = exp(-tau * r * r) * pow(sin(r), dim-1);
    }
    else if(type.compare("KendallShapeSpace2DGeometry") == 0)
    {
      r = 0.5 * pi * (i - 0.5) / steps;
      constNormal = exp(-tau * r * r) * pow(sin(r), dim-2) * 0.5 * sin(2 * r);
    }
    sumconstNormal += constNormal;
    sumconstDev += -r * r * constNormal;
  }

  ret(0) = sumconstDev / sumconstNormal;

  if(type.compare("SphereGeometry") == 0)
    ret(1) = pi / steps * sumconstNormal * sphereArea(dim-1);
  else if(type.compare("KendallShapeSpace2DGeometry") == 0)
    ret(1) = 0.5 * pi / steps * sumconstNormal * sphereArea(dim-1);

  return(ret);
}

//============================================================================
// exp map on Stiefel manifold

template<class PointType, class TangentType>
MatrixType ProbabilisticPGA<PointType, TangentType>::
expMapStiefel(const MatrixType & oribase,
	      const MatrixType & v,
	      double t)
{
  unsigned int p = oribase.n_cols;
  if(norm(v, "fro") < 1.0e-12)
    return oribase;

  MatrixExponential matrixExp;

  // Compact QR decomposition
  MatrixType base = oribase;
  GramSchmidt(oribase, base); // make sure base is on stiefel manifold
  MatrixType A = strans(base) * v; // should be skew-symmetric matrix
  A = 0.5 * (A - strans(A)); // enforce skew-symmetric

  MatrixType K = v - base * A;
  MatrixType Q, R;
  qr_econ(Q, R, K); // make sure this works fine in armadillo

  MatrixType X = zeros(2*p, 2*p);
  X.submat(0, 0, p-1, p-1) = A;
  X.submat(p, 0, 2*p-1, p-1) = R;
  X.submat(0, p, p-1, 2*p-1) = -strans(R);

  MatrixType MN =  matrixExp.skewMatrixExp(t*X);
  MN.shed_cols(p, 2*p-1); // remove the last p cols

  MatrixType M = MN.rows(0, p-1);
  MatrixType N = MN.rows(p, 2*p-1);

  MatrixType target = base * M + Q * N;

  return target;
}

//========================================================================
// calculate function energy for each data point

template<class PointType, class TangentType>
ScalarType ProbabilisticPGA<PointType, TangentType>::
UEnergy(const PointType & dataPoint,
	const ScalarListType & xPoint, // new x_i
	const PointType & base,
	const MatrixType & wLambda) // tangent
{
  ScalarType pe = 0.0;
  TangentType tangent;
  ScalarListType matToCol = wLambda * xPoint;
  geom->listToTangent(matToCol, tangent);
  pe += geom->distanceSquared(geom->expMap(base, tangent), dataPoint);
  pe *= 0.5 / (sigma * sigma);
  pe += 0.5 * dot(xPoint, xPoint);

  return(pe);
}

//========================================================================
// sum total energy over whole data points by using openmp

template<class PointType, class TangentType>
ScalarType ProbabilisticPGA<PointType, TangentType>::
UEnergyParallel(const PointListType & data,
		const MatrixType & x,
		const PointType & base,
		const MatrixType & wLambda) // w*Lambda
{
  unsigned int numPts = data.size();
  ScalarListType ret(numPts);

  //  #pragma omp parallel for
  for (unsigned i = 0; i < numPts; i++)
    ret[i] = UEnergy(data[i], x.col(i), base, wLambda);

  return(sum(ret));
}

//========================================================================
// gradient with respect to each data point

template<class PointType, class TangentType>
ScalarListType ProbabilisticPGA<PointType, TangentType>::
gradientU(const PointType & dataPoint,
	  const ScalarListType & xPoint,
	  const PointType & base,
	  const MatrixType & wLambda)
{
  PointType estimate;
  TangentType jOutput, et, eb, tangent;
  TangentType jOutputDash = geom->getZeroVector();
  ScalarListType matToCol, ret;
  matToCol = wLambda * xPoint;
  geom->listToTangent(matToCol, tangent);

  estimate = geom->expMap(base, tangent);
  et = geom->logMap(estimate, dataPoint); // The error on one data point
  eb = geom->logMap(estimate, base); // shooting in the opposite direction

  jOutput = et;
  geom->adjointJacobiField(estimate, eb, jOutput, jOutputDash);
  geom->tangentToList(jOutputDash, matToCol);
  ret = 1.0 / (sigma * sigma) * (strans(wLambda) * matToCol);

  return(xPoint - ret);
}

//========================================================================
// sum total gradient over whole data points by using openmp

template<class PointType, class TangentType>
void ProbabilisticPGA<PointType, TangentType>::
gradientUParallel(const PointListType & data,
		  const MatrixType & x,
		  const PointType & base,
		  const MatrixType & wLambda, // w * Lambda
		  MatrixType & gradX)
{
  //  #pragma omp parallel for
  for (unsigned i = 0; i < data.size(); i++)
    gradX.col(i) = gradientU(data[i], x.col(i), base, wLambda);
}

//========================================================================
// HMC sampling for x_i on manifolds

template<class PointType, class TangentType>
ScalarType ProbabilisticPGA<PointType, TangentType>::
HMC(const PointListType & data,
    const PointType & base,
    const MatrixType & wLambda,
    MatrixType & current_q,  // current x_i vector
    MatrixListType & current_qAll,
    const ScalarType stepSize, // steps for Hmc
    const int burnSamples,
    const int L) // all the samples
{
  unsigned int numPts = data.size();

  // p is momentum in kinectic energy
  MatrixType q, p(dimReduce, numPts), current_p;
  MatrixType gradX = zeros(dimReduce, numPts);
  MatrixType sum_samples = zeros(dimReduce, numPts);
  int acc = 0, acc_sample = 0;
  ScalarType acc_total = 0.0, energy=0.0, diff = 0.0;
  ScalarType current_U, proposed_U, current_K, proposed_K;

  srand (time(NULL));

  while (acc_sample < numSamples)
  {
    q = current_q;
    p.randn(); // generate normal random gaussian noise
    current_p = p;

    gradientUParallel(data, q, base, wLambda, gradX);
    p = p - 0.5 * stepSize * gradX; // half step for hamiltonian momentum

    for (int i = 0; i < L; i++) // full step for the position and momentum
    {
      q = q + stepSize * p;
      if (i != L-1)
      {
	gradientUParallel(data, q, base, wLambda, gradX);
	p = p - stepSize * gradX;
      }
    }

    gradientUParallel(data, q, base, wLambda, gradX);
    p = p - 0.5 * stepSize * gradX; // half step again
    p = -1.0 * p; // negate the monentum at end of trajectory

    current_U = UEnergyParallel(data, current_q, base, wLambda);
    current_K = 0.5 * pow(norm(current_p, "fro"), 2);
    proposed_U = UEnergyParallel(data, q, base, wLambda);
    proposed_K = 0.5 * pow(norm(p, "fro"), 2);

    diff = current_U + current_K - proposed_U - proposed_K;

    if (RandomType::realUniform(0.0, 1.0) < exp(diff))
    {
      current_q = q;
      acc += 1;
      if (acc > burnSamples) { //discard first number of samples
	sum_samples = sum_samples + q;
	energy += proposed_U;
	current_qAll.push_back(q);
	acc_sample += 1;
      }
    }

    acc_total += 1;
  } // hmc ends

  current_q = 1.0 / acc_sample * sum_samples;

  //std::cout << "acceptance rate is" << acc/acc_total << std::endl;

  return(energy / acc_sample);
}

// //========================================================================
// my geodesic regression on estimating base and tangent

template<class PointType, class TangentType>
void ProbabilisticPGA<PointType, TangentType>::
computePPGA(const PointListType & data,
	    const MatrixListType & current_qAll, // all the samples
	    PointType & base,
	    MatrixType & w,
	    MatrixType & Lambda,
	    const unsigned int maxIterations,
	    const ScalarType initStepSizeW,
	    const ScalarType initStepSizeSigma,
	    const ScalarType initStepSizeLambda,
	    const ScalarType initStepSizeBase)
{
  unsigned int numPts = data.size();
  unsigned int i, j, counter = 0;
  ScalarListType constRet, matToCol;
  ScalarType epsilon = 1.0e-4, prevEnergy = 1e10, dataError;
  ScalarType tau = 0.5 / (sigma * sigma);
  ScalarType energy, prevTau = tau, newTau = tau, gradientTau;
  MatrixType baseGradient; baseGradient.copy_size(base);
  MatrixType wGradient; wGradient.copy_size(w);
  MatrixType LambdaGradient(dimReduce, 1);
  MatrixType prevW = w, newW = w, updatedW;
  MatrixType prevLambda = Lambda, newLambda = Lambda;
  MatrixType xList, Atemp, ProjWGradient, rotationMat;
  PointType prevBase = base, newBase = base;
  PointType target, estimate;
  TangentType et, eb;
  TangentType jOutput, jOutputDash, tangent; jOutputDash.copy_size(base);

  ScalarType stepSizeW = initStepSizeW;
  ScalarType stepSizeSigma = initStepSizeSigma;
  ScalarType stepSizeLambda = initStepSizeLambda;
  ScalarType stepSizeBase = initStepSizeBase;

  while(counter < maxIterations)
  {
    // varying position and direction simultaneously
    baseGradient.fill(0.0);
    wGradient.fill(0.0);
    LambdaGradient.fill(0.0);
    dataError = 0.0;

    // energy and gradient for normalising constant
    constRet = integration(tau);

    for (j = 0; j < numSamples; j++) // go through each sample
    {
      xList = current_qAll[j];

      for(i = 0; i < numPts; i++)
      {
	target = data[i];
	matToCol = w * Lambda * xList.col(i);
	geom->listToTangent(matToCol, tangent);

	estimate = geom->expMap(base, tangent);
	et = geom->logMap(estimate, target); // The error on one data point
	eb = geom->logMap(estimate, base); // shooting in the opposite direction

	dataError += geom->distanceSquared(estimate, target);

	jOutput = et;
	jOutputDash.fill(0.0);
	geom->adjointJacobiField(estimate, eb, jOutput, jOutputDash);
	geom->tangentToList(jOutputDash, matToCol); // convert jOutputDash to be a list

	baseGradient += jOutput;
	wGradient += matToCol * strans(xList.col(i)) * Lambda;
	LambdaGradient += strans(w * diagmat(xList.col(i))) * matToCol;
      }
    }
    // calculate gradientsigma and energy for all samples
    gradientTau = numSamples*numPts * constRet(0) + dataError;
    if(constRet(1) <= 0.0) {constRet(1) += 1.0e-10;}
    energy = numSamples * numPts * log(constRet(1)) + tau * dataError;

    baseGradient = 1.0 / numSamples * baseGradient * 2.0 * tau;
    wGradient = 1.0 / numSamples * wGradient * 2.0 * tau;
    LambdaGradient = 1.0 / numSamples * LambdaGradient * 2.0 * tau;
    gradientTau /= numSamples;
    energy /= numSamples;

    // Updating sigma
    newTau = tau - stepSizeSigma * gradientTau;

    // Updating new Lambda
    newLambda = Lambda + diagmat(LambdaGradient * stepSizeLambda);

    // Updating new base point
    newBase = geom->expMap(base, baseGradient * stepSizeBase);

    // project gradient
    Atemp = strans(w) * wGradient; // should be skew-symmetric, but is not
    Atemp = 0.5 * (Atemp + strans(Atemp)); // find the symmetric part
    ProjWGradient = wGradient - w * Atemp; // project out the symmetric part

    // update new W
    updatedW = expMapStiefel(w, ProjWGradient, stepSizeW);

    // Parallel translate w
    for (i = 0; i < dimReduce; i++)
    {
      matToCol = updatedW.col(i);
      geom->listToTangent(matToCol, tangent);
      newW.col(i) = geom->parallelTranslateAtoB(base, newBase, tangent); // needs to be checked
    }

    // Adaptivity: Updating the new base point, sigma, w and Lambda
    if(energy > prevEnergy)
    {
      stepSizeW *= 0.8;
      stepSizeLambda *= 0.8;
      stepSizeBase *= 0.8;
      stepSizeSigma *= 0.8;
      base = prevBase;
      w = prevW;
      Lambda = prevLambda;
      tau = prevTau;
    }
    else
    {
      prevBase = base;
      prevW = w;
      prevLambda = Lambda;
      prevTau = tau;
      prevEnergy = energy;
      base = newBase;
      w = newW;
      Lambda = newLambda;
      tau = newTau;
    }
    counter++;

    if(geom->norm(base, baseGradient) < epsilon &&
       norm(wGradient, "fro") < epsilon &&
       geom->norm(base, LambdaGradient) < epsilon &&
       std::fabs(gradientTau) < epsilon)
      break;
  }

  sigma = sqrt(0.5 / tau);
}

//========================================================================
// Expectation maximization on estimating base and w
// needs to be checked

template<class PointType, class TangentType>
void ProbabilisticPGA<PointType, TangentType>::
EM(PointType & base,
   MatrixType & current_q,  //current x_i vectors
   MatrixType & w,
   MatrixType & Lambda,
   const PointListType & data,
   const unsigned int EMIter,
   const unsigned int maxIterations,
   const ScalarType initStepSizeW,
   const ScalarType initStepSizeSigma,
   const ScalarType initStepSizeLambda,
   const ScalarType initStepSizeBase)
{
  MatrixListType current_qAll;
  MatrixType wLambda;
  ScalarType energy;

  for (int i = 0; i < EMIter; i++)
  {
    wLambda =  w * Lambda;
    energy = HMC(data, base, wLambda, current_q, current_qAll);

    computePPGA(data, current_qAll, base, w, Lambda,
		maxIterations, initStepSizeW, initStepSizeSigma,
		initStepSizeLambda, initStepSizeBase);

    current_qAll.clear(); // clear the vector list

    std::cout << "Iter, " << i << ", estSigma, " << sigma << std::endl;
  }
}
