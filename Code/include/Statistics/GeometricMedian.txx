/*==============================================================================
  File: GeometricMedian.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

template<class PointType, class TangentType>
void GeometricMedian<PointType, TangentType>::
computeMedian(const PointListType & pointList, PointType & median) const
{
  TangentType u, v;
  ScalarType dist;
  ScalarType invDistSum, distSum, lastDistSum;
  ScalarType lastNormSquared;
  ScalarType currStepSize = stepSize;
  unsigned int iter, i;

  if(pointList.size() == 0)
    return;

  median = pointList[0];

  lastNormSquared = epsilon + 1.0;
  lastDistSum = 1.0e10;

  iter = 0;
  while(lastNormSquared >= epsilon && iter < maxIterations)
  {
    distSum = 0.0;
    invDistSum = 0.0;
    v = geom->getZeroVector();
    for(i = 0; i < pointList.size(); i++)
    {
      u = geom->logMap(median, pointList[i]);
      dist = geom->norm(median, u);

      if(dist > 1.0e-8)
      {
	v += u / dist;
        distSum += dist;
        invDistSum += 1.0 / dist;
      }
    }

    if(distSum >= lastDistSum)
    {
      currStepSize *= 0.5;
      median = pointList[0];
      lastNormSquared = epsilon + 1.0;
      lastDistSum = 1.0e10;
    }

    lastNormSquared = geom->normSquared(median, v);

    if(invDistSum > 1.0e-8)
    {
      v *= 1.0 / invDistSum;
      median = geom->expMap(median, v * currStepSize);
    }

    iter++;
  }
}
