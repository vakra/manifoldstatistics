/*==============================================================================
  File: GeodesicRegression.cxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

// Computing a least-squares geodesic given data
template<class PointType, class TangentType>
void GeodesicRegression<PointType, TangentType>::
computeGeodesicRegression(const PointListType & pointList,
                          const ScalarListType & scalarList,
                          PointType & base, TangentType & tangent)
{
  unsigned int i;
  unsigned int counter = 0;
  TangentType be, et, eb;
  TangentType zeroVector = geom->getZeroVector();

  // Checking if the data coming in is compatible
  if(pointList.size() == 0 || pointList.size() != scalarList.size())
    return; // Need to do better error handling
  // need to something similar for initialized base and tangent

  // initializing other variables
  ScalarType stepSize = initStepSize; // stepSize automatically changed
  ScalarType prevEnergy = 1e10;
  PointType prevBase = base;
  TangentType prevTangent = tangent;
  PointType target, estimate;
  TangentType pointGradient, vectorGradient;
  TangentType jOutput, jOutputDash, gradient;
  TangentType currentTangent, updatedTangent, newTangent;
  ScalarType energy;

  while(counter < maxIterations)
  {
    // varying position and direction simultaneously

    pointGradient = zeroVector;
    vectorGradient = zeroVector;

    energy = 0.0;
    for(i = 0; i < pointList.size(); i++)
    {
      target = pointList[i];
      currentTangent = tangent * scalarList[i];
      estimate = geom->expMap(base, currentTangent);

      be = geom->logMap(base, estimate); // tangent from base to estimate
      et = geom->logMap(estimate, target); // The error on one data point
      eb = geom->logMap(estimate, base); // shooting in the opposite direction

      energy += geom->normSquared(estimate, et);
      //cout << "energy = " << energy << endl;

      jOutput = et;
      jOutputDash = zeroVector;
      geom->adjointJacobiField(estimate, eb, jOutput, jOutputDash);
      pointGradient += jOutput;
      vectorGradient += jOutputDash * scalarList[i];
    }
    // Updating new base point
    PointType newBase = geom->expMap(base, pointGradient * stepSize);

    // Updating new tangent direction
    updatedTangent = tangent + vectorGradient * stepSize;
    newTangent = geom->parallelTranslateAtoB(base, newBase, updatedTangent);

    // Updating the new base point and tangent direction
    if(energy > prevEnergy)
    {
      stepSize = stepSize * 0.5;
      base = prevBase;
      tangent= prevTangent;
    }
    else
    {
      prevBase = base;
      prevTangent = tangent;
      base = newBase;
      tangent = newTangent;
      prevEnergy = energy;
    }
    counter++;

    //std::cout << geom->norm(base, pointGradient) + geom->norm(base, vectorGradient) << std::endl;
    if(geom->norm(base, pointGradient) < epsilon &&
       geom->norm(base, vectorGradient) < epsilon)
      break;
  }
}
