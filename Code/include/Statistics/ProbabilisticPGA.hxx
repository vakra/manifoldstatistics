/*==============================================================================
  File: ProbabilisticPGA.hxx

  ============================================================================= */
#ifndef __ProbabilisticPGA_hxx
#define __ProbabilisticPGA_hxx

#include "RandomGenerator.hxx"
#include "RiemannianGeometry.hxx"
#include "MatrixExponential.hxx"

template<class PointType, class TangentType>
class ProbabilisticPGA
{
public:
  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;
  typedef std::shared_ptr<GeometryType> GeometryPtr;
  typedef RandomGenerator<PointType, TangentType> RandomType;
  typedef vector<MatrixType> MatrixListType;

  ProbabilisticPGA(GeometryPtr _geom,
		   const unsigned int & _dimReduce,
		   const ScalarType & _sigma,
		   const int & _numSamples = 200)
  {
    geom = _geom;
    dimReduce = _dimReduce;
    sigma = _sigma;
    numSamples = _numSamples;
  }

  GeometryPtr getRiemannianGeometry()
  {
    return geom;
  }

  // Gram–Schmidt process: orthonormalising a set of vectors in an inner product space
  void GramSchmidt(MatrixType A, MatrixType & Q);

  // Generate random orthogonal matrix
  void randOrthoMat(MatrixType & A);

  // Function for calculating unit length sphere area
  double sphereArea(int dim);

  // Integration over [0, \pi] / [0, \pi/2]
  // return derivative w.r.t normalizing constant
  // tau = 0.5/(sigma*sigma)
  VectorType integration(double tau);

  void matAbs(MatrixType & mat);

  // exp map on Stiefel manifold
  MatrixType expMapStiefel(const MatrixType & base,
			   const MatrixType & v, double t);

  // calculate function energy for each data point
  ScalarType UEnergy(const PointType & dataPoint,
		     const ScalarListType & xPoint, // new x_i
		     const PointType & base,
		     const MatrixType & wLambda); // tangent

  // sum total energy over whole data points by using openmp
  ScalarType UEnergyParallel(const PointListType & data,
			     const MatrixType & x,
			     const PointType & base,
			     const MatrixType & wLambda); // w*Lambda

  // gradient with respect to each data point
  ScalarListType gradientU(const PointType & dataPoint,
			   const ScalarListType & xPoint,
			   const PointType & base,
			   const MatrixType & wLambda); // tangent

  // sum total gradient over whole data points by using openmp
  void gradientUParallel(const PointListType & data,
			 const MatrixType & x,
			 const PointType & base,
			 const MatrixType & wLambda, // w*Lambda
			 MatrixType & gradX);

  // hmc sampling for x_i on manifolds
  ScalarType HMC(const PointListType & data,
		 const PointType & base,  // mu
		 const MatrixType & wLambda,
		 MatrixType & current_q,  // current x_i vector
		 MatrixListType & current_qAll,
		 const ScalarType stepSize=2e-2, // steps for Hmc
		 const int burnSamples=100,
		 const int L=20); // all the samples

  // my geodesic regression on estimating base and tangent
  void computePPGA(const PointListType & data,
		   const MatrixListType & current_qAll, // all the samples
		   PointType & base,
		   MatrixType & w,
		   MatrixType & Lambda,
		   const unsigned int maxIterations,
		   const ScalarType initStepSizeW,
		   const ScalarType initStepSizeSigma,
		   const ScalarType initStepSizeLambda,
		   const ScalarType initStepSizeBase);

  // Expectation maximization for estimating base and w
  void EM(PointType & base,
	  MatrixType & current_q,  //current x_i vectors
	  MatrixType & w,
	  MatrixType & Lambda,
	  const PointListType & data,
	  const unsigned int EMIter,
	  const unsigned int maxIterations = 200,
	  const ScalarType initStepSizeW = 2e-3,
	  const ScalarType initStepSizeSigma = 4e-3,
	  const ScalarType initStepSizeLambda = 2e-4,
	  const ScalarType initStepSizeBase = 1e-4);

protected:
  GeometryPtr geom;
  unsigned int dimReduce;
  ScalarType sigma;
  int numSamples;
};

#include "ProbabilisticPGA.txx"

#endif
