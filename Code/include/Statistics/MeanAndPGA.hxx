/*==============================================================================
  File: MeanAndPGA.hxx

  MeanAndPGA computes statistics of data on a Riemannian manifold, including
  Frechet mean, covariance, and principal geodesic analysis (PGA). (PGA is the
  generalization of PCA to manifold geometries). The class takes a pointer to a
  RiemannianGeometry class, which defines how the statistics are computed. For
  details on the statistics computations, see the references:

  Fletcher, P.T., Joshi, S., Lu, C., Pizer, S.M. Principal Geodesic Analysis for
  the Study of Nonlinear Statistics of Shape, IEEE Transactions on Medical
  Imaging, vol. 23, no. 8, pp. 995-1005, IEEE, Aug. 2004.

  Fletcher, P.T., Joshi, S. Principal Geodesic Analysis on Symmetric Spaces:
  Statistics of Diffusion Tensors. CVAMIA Workshop part of ECCV 2004,
  LNCS 3117, Springer-Verlag, pp. 87-98, 2004.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __MeanAndPGA_hxx
#define __MeanAndPGA_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
class MeanAndPGA
{
public:
  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;
  typedef std::shared_ptr<GeometryType> SharedRiemannianGeometryPtr;

  MeanAndPGA(SharedRiemannianGeometryPtr _geom,
             const ScalarType & _stepSize = 1.0,
             const ScalarType & _epsilon = 1.0e-8)
  {
    geom = _geom;
    stepSize = _stepSize;
    epsilon = _epsilon;
  }

  SharedRiemannianGeometryPtr getRiemannianGeometry()
  {
    return geom;
  }

  void computeMean(const PointListType & pointList, PointType & mean) const;

  void computeWeightedAve(const ScalarListType & weightList,
                          const PointListType & pointList,
                          PointType & weightedAve) const;

  void computeMeanAndCovariance(const PointListType & pointList,
                                PointType & mean,
                                MatrixType & covariance) const;

  // Returns principal vectors/values, in ascending order
  void computeMeanAndPGA(const PointListType & pointList,
                         PointType & mean, MatrixType & pgaVariances,
                         MatrixType & pgaVectorsMatrix) const;

protected:
  SharedRiemannianGeometryPtr geom;
  ScalarType stepSize;

  // Epsilon for stopping criteria
  ScalarType epsilon;
};

#include "MeanAndPGA.txx"

#endif
