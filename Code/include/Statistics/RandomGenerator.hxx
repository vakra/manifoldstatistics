/*==============================================================================
  File: RandomGenerator.hxx

  RandomGenerator is an abstract class for random variables taking values on a
  Riemannian manifold.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __RandomGenerator_hxx
#define __RandomGenerator_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
class RandomGenerator
{
public:
  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef std::shared_ptr<GeometryType> GeometryPtr;
  typedef std::vector<PointType> PointListType;

  RandomGenerator() {}
  virtual ~RandomGenerator() {}

  static void seed(unsigned int seed) { srand(seed); }
  static void seedWithTime() { srand(time(NULL)); }

  static ScalarType realUniform(ScalarType min, ScalarType max)
  {
    return min + (max - min) * (static_cast<ScalarType>(rand()) /
                                (static_cast<ScalarType>(RAND_MAX) + 1.0));
  }

  // Function to generate a random point on the manifold
  static PointType randomGaussianPoint(GeometryPtr geom,
                                       const PointType & mean,
                                       ScalarType sigma)
  {
    ScalarType x, y, r2;
    unsigned int i;
    unsigned int exDim = geom->getExtrinsicDimension();

    ScalarListType randomList(exDim);
    TangentType randomTangent, temp;

    // Generate random gaussians with polar Box-Muller method
    for(i = 0; i < exDim; i++)
    {
      r2 = 0;
      while(r2 > 1.0 || r2 == 0)
      {
        x = (2.0 * static_cast<ScalarType>(rand()) /
             static_cast<ScalarType>(RAND_MAX + 1.0)) - 1.0;
        y = (2.0 * static_cast<ScalarType>(rand()) /
             static_cast<ScalarType>(RAND_MAX + 1.0)) - 1.0;
        r2 = x * x + y * y;
      }
      ScalarType randomNumber = sigma * y * sqrt(-2.0 * log(r2) / r2);
      randomList(i) = randomNumber;
    }
    geom->listToTangent(randomList, temp);
    geom->projectTangent(randomTangent, mean, temp);
    PointType out = geom->expMap(mean, randomTangent);

    return out;
  }
};

#endif
