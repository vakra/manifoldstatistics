/*==============================================================================
  File: GeometricMedian.hxx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __GeometricMedian_hxx
#define __GeometricMedian_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
class GeometricMedian
{
public:
  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;
  typedef std::shared_ptr<RiemannianGeometry<PointType, TangentType> > GeometryPtr;

  GeometricMedian(GeometryPtr _geom,
                  const ScalarType & _stepSize = 1.0,
                  const ScalarType & _epsilon = 1.0e-8,
                  const ScalarType & _maxIterations = 1000)
  {
    geom = _geom;
    stepSize = _stepSize;
    epsilon = _epsilon;
    maxIterations = _maxIterations;
  }

  GeometryPtr getRiemannianGeometry()
  {
    return geom;
  }

  void computeMedian(const PointListType & pointList, PointType & median) const;

private:
  GeometryPtr geom;
  ScalarType stepSize;

  // Epsilon for stopping criteria
  ScalarType epsilon;

  // Max iterations for alternate stopping criteria
  unsigned int maxIterations;
};

#include "GeometricMedian.txx"

#endif
