/*==============================================================================
  File: GeodesicRegression.hxx

  GeodesicRegression computes the least-squares geodesic to a set of given data
  on a Riemannian manifold with associated independent variables. The class
  takes a pointer to the RiemannianGeometry class. See the reference:

  Fletcher, P.T. Geodesic Regression on Riemannian Manifolds, MICCAI Workshop on
  Mathematical Foundations of Computational Anatomy (MFCA), 2011.

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

#ifndef __GeodesicRegression_hxx
#define __GeodesicRegression_hxx

#include "RiemannianGeometry.hxx"

template<class PointType, class TangentType>
class GeodesicRegression
{
public:
  typedef RiemannianGeometry<PointType, TangentType> GeometryType;
  typedef typename GeometryType::PointListType PointListType;
  typedef typename std::shared_ptr<RiemannianGeometry<PointType, TangentType> > GeometryPtr;

  GeodesicRegression(GeometryPtr _geom,
                     ScalarType _stepSize = 1.0,
                     unsigned int _maxIterations = 1000,
                     ScalarType _epsilon = 1e-5)
  {
    geom = _geom;
    initStepSize = _stepSize;
    maxIterations = _maxIterations;
    epsilon = _epsilon;
  }

  GeometryPtr getRiemannianGeometry()
  {
    return geom;
  }

  void setStepSize(ScalarType s) { initStepSize = s; }
  void setMaxIterations(unsigned int m) { maxIterations = m; }
  void setEpsilon(ScalarType e) { epsilon = e; }

  void computeGeodesicRegression(const PointListType & pointList,
                                 const ScalarListType & scalarList,
                                 PointType & base,
                                 TangentType & tangent);
private:
  GeometryPtr geom;

  // Epsilon for stopping criteria
  ScalarType epsilon;

  // Stepsize for moving in the gradient direction
  ScalarType initStepSize;

  // Maximum number of iterations to run gradient descent
  unsigned int maxIterations;
};

#include "GeodesicRegression.txx"

#endif
