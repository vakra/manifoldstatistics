/*==============================================================================
  File: MeanAndPGA.txx

  (C) Copyright 2013 The ManifoldStatistics Team
  Distributed under the Boost Software License, Version 1.0
  See the accompanying file LICENSE.txt for details.

==============================================================================*/

template<class PointType, class TangentType>
void MeanAndPGA<PointType, TangentType>::
computeMean(const PointListType & pointList, PointType & mean) const
{
  TangentType tangent, lastTangent, initialTangent;
  ScalarType lastNormSquared, normSquared, prevNormSquared, initialNormSquared;
  ScalarType currStepSize = stepSize;
  unsigned int i;

  if(pointList.size() == 0)
    return;

  // Need to make sure the mean vector has correct size
  mean = pointList[0];
  
  tangent = geom->getZeroVector();
  for(i = 0; i < pointList.size(); i++)
    tangent += geom->logMap(mean, pointList[i]);
  tangent = tangent * (1.0 / static_cast<ScalarType>(pointList.size()));
  initialTangent = tangent;

  initialNormSquared = geom->normSquared(mean, tangent);
  lastNormSquared = initialNormSquared;

  while(lastNormSquared >= epsilon)
  {
    mean = geom->expMap(mean, tangent * currStepSize);

    tangent = geom->getZeroVector();
    for(i = 0; i < pointList.size(); i++)
      tangent += geom->logMap(mean, pointList[i]);

    tangent = tangent * (1.0 / static_cast<ScalarType>(pointList.size()));

    normSquared = geom->normSquared(mean, tangent);
    if(normSquared >= lastNormSquared)
    {
      currStepSize *= 0.5;
      mean = pointList[0];
      lastNormSquared = initialNormSquared;
      tangent = initialTangent;
    }
    else
      lastNormSquared = normSquared;
  }
}

template<class PointType, class TangentType>
void MeanAndPGA<PointType, TangentType>::
computeWeightedAve(const ScalarListType & weightList,
                   const PointListType & pointList,
                   PointType & weightedAve) const
{
  TangentType tangent, initialTangent;
  ScalarType lastNormSquared, normSquared, initialNormSquared;
  ScalarType currStepSize = stepSize;
  unsigned int i;

  if(pointList.size() == 0 || pointList.size() != weightList.size())
    return;

  weightedAve = pointList[0];

  tangent = geom->getZeroVector();
  for(i = 0; i < pointList.size(); i++)
    tangent += geom->logMap(weightedAve, pointList[i]) * weightList[i];

  initialTangent = tangent;

  initialNormSquared = geom->normSquared(weightedAve, tangent);
  lastNormSquared = initialNormSquared;
  while(lastNormSquared >= epsilon)
  {
    weightedAve = geom->expMap(weightedAve, tangent * currStepSize);

    tangent = geom->getZeroVector();
    for(i = 0; i < pointList.size(); i++)
      tangent += geom->logMap(weightedAve, pointList[i]) * weightList[i];

    normSquared = geom->normSquared(weightedAve, tangent);
    if(normSquared >= lastNormSquared)
    {
      currStepSize *= 0.5;
      weightedAve = pointList[0];
      lastNormSquared = initialNormSquared;
      tangent = initialTangent;
    }
    else
      lastNormSquared = normSquared;
  }
}


template<class PointType, class TangentType>
void MeanAndPGA<PointType, TangentType>::
computeMeanAndCovariance(const PointListType & pointList,
                         PointType & mean,
                         MatrixType & covariance) const
{
  TangentType logPoint;
  unsigned int i, j, k;

  ScalarListType listLogPoint;

  // Computing mean
  computeMean(pointList, mean);

  // determining matrix dimension for covariance matrix
  logPoint = geom->getZeroVector();
  geom->tangentToList(logPoint, listLogPoint);

  unsigned int listSize = listLogPoint.size();
  listLogPoint.clear();

  covariance.zeros(listSize, listSize);

  // Finding covariance
  for(i = 0; i < pointList.size(); i++)
  {
    logPoint = geom->logMap(mean, pointList[i]);
    geom->tangentToList(logPoint, listLogPoint);

    for(k = 0; k < listSize; k++)
    {
      for(j = 0; j < listSize; j++)
        covariance(j, k) += listLogPoint[j] * listLogPoint[k];
    }
    listLogPoint.clear();
  }

  covariance *= (1.0 / static_cast<ScalarType>(pointList.size() - 1));
}

template<class PointType, class TangentType>
void MeanAndPGA<PointType, TangentType>::
computeMeanAndPGA(const PointListType & pointList, PointType & mean,
                  MatrixType & pgaVariances,
                  MatrixType & pgaVectorsMatrix) const
{
  MatrixType covariance;
  computeMeanAndCovariance(pointList, mean, covariance);

  ScalarListType eigVal;
  MatrixType eigVec;
  eig_sym(eigVal, eigVec, covariance);
  pgaVariances = diagmat(eigVal);
  pgaVectorsMatrix = eigVec;
}
