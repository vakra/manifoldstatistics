PROJECT(ManifoldStatistics)

CMAKE_MINIMUM_REQUIRED(VERSION 2.7)

# == Check gcc compiler: == 
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

# == Adds any local CMake models (Find*.cmake files): ==
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
    "${ManifoldStatistics_SOURCE_DIR}/CMake/Modules/")

# == Using Boost: ==
SET(Boost_USE_STATIC_LIBS OFF)
SET(Boost_USE_MULTITHREADED ON)
SET(Boost_USE_STATIC_RUNTIME OFF)
FIND_PACKAGE(Boost REQUIRED COMPONENTS program_options)
IF (Boost_FOUND)
    INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})
    ADD_DEFINITIONS( "-DHAS_BOOST" )
ENDIF()

# == Using Armadillo: ==
FIND_PACKAGE(Armadillo REQUIRED)
IF (ARMADILLO_FOUND)
    INCLUDE_DIRECTORIES(${ARMADILLO_INCLUDE_DIRS})
ENDIF()

# == Using expokit: ==
FIND_PACKAGE(Expokit REQUIRED)

# == Optional subdirectories to build ==
OPTION(BUILD_TESTING "Build tests?" OFF)
OPTION(BUILD_APPLICATIONS "Build applications?" ON)

# == Setup Include Directories ==
LIST(APPEND ManifoldStatistics_INCLUDE_DIRECTORIES
  ${ManifoldStatistics_SOURCE_DIR}/Code/include/Base/
  ${ManifoldStatistics_SOURCE_DIR}/Code/include/IO/
  ${ManifoldStatistics_SOURCE_DIR}/Code/include/Statistics/
)

INCLUDE_DIRECTORIES(${ManifoldStatistics_INCLUDE_DIRECTORIES})

# == Shared Library Option ==
MARK_AS_ADVANCED(BUILD_SHARED_LIBS)
OPTION(BUILD_SHARED_LIBS "Build shared libraries?" ON)
IF(BUILD_SHARED_LIBS)
  SET(LIBTYPE SHARED)
ELSE(BUILD_SHARED_LIBS)
  SET(LIBTYPE STATIC)
ENDIF(BUILD_SHARED_LIBS)

# == What to Build ==
ADD_SUBDIRECTORY(${ManifoldStatistics_SOURCE_DIR}/Code/src/)

IF(BUILD_TESTING)
  ADD_SUBDIRECTORY(${ManifoldStatistics_SOURCE_DIR}/Testing/)
ENDIF(BUILD_TESTING)

IF(BUILD_APPLICATIONS)
  ADD_SUBDIRECTORY(${ManifoldStatistics_SOURCE_DIR}/Applications/)
ENDIF(BUILD_APPLICATIONS)
